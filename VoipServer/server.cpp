#include "NetworkVoipServer.h"
#include <string>
#include <iostream>
#include <tuple>

using namespace std;
//
//template<typename T>
//void test_impl(const T& value) {
//	cout << value;
//}

void test_impl(const char *t) {
	cout << t;
}

void test_impl(const int &t) {
	cout << t*t;
}



template<typename ... Param>
void test(const Param& ... param) {
	(void)initializer_list<int>{ (test_impl(param), 0)... };
}

int main() {
	Voip::Network::CNetworkVoipServer server;
	
	server.InitIPv6("", 50024);
	server.Start();
	
	while (true) {
		Sleep(1);
	}
	return 0;
}