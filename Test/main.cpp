#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif


#include <windows.h>
#include <windowsx.h>
#include <WinUser.h>
#include <Mmsystem.h>
#include <iostream>

#include "MainWindow.h"
#include "Timer.h"

#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "Voip.lib")

using namespace Voip;
using namespace std;

Gui::CMainWindow *mainWindow = nullptr;
Voip::Helpers::CTimer timer;

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	//Voip::Network::CPortMapper* portMapper = new Voip::Network::CPortMapper();
	//portMapper->AddPortMapping(50024, 50024);

	mainWindow = new Gui::CMainWindow();
	mainWindow->Init(hInstance, NULL);
	mainWindow->Show();
	MSG msg;
	msg.message = WM_NULL;

	while (msg.message != WM_QUIT) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			if (mainWindow->IsVisible()) {
				timer.Tick();
				mainWindow->Tick(timer.GetTimeTotal(), timer.GetTimeDelta());
			}
			else {
				Sleep(1);
			}
		}
	}

	return 0;
}