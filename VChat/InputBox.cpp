#include "InputBox.h"
namespace Voip {
	namespace Gui {
		CInputBox::CInputBox()
		{
			m_inputField = NULL;
			m_btnCancel = NULL;
			m_btnConfirm = NULL;
			m_labelTitle = NULL;
		}

		CInputBox::~CInputBox()
		{
			Fini();
		}

		void CInputBox::Init(HWND hwnd, int id, bool bShowBackground, std::wstring cancelBtn, std::wstring confirmBtn)
		{
			CScenario::Init(hwnd, id, bShowBackground);
			CButtonRect* bar = new CButtonRect();
			bar->Init(hwnd, 0, L"", { 0,0 }, { Dimensions::INPUTBOX_WIDTH, Dimensions::INPUTBOX_HEIGHT });
			bar->SetBgColor(&Colors::WHITE);
			bar->SetInteractivity(false);
			bar->SetActiveState(true);

			m_inputField = new CInputfield();
			m_btnCancel = new CButtonRect();
			m_btnConfirm = new CButtonRect();
			m_labelTitle = new CLabel();

			m_inputField->Init(hwnd, IDS::INPUTBOX_INPUTFIELD);
			m_labelTitle->Init(IDS::INPUTBOX_TITLE);
			m_btnCancel->Init(hwnd, IDS::Element::BTN_CANCEL, cancelBtn);
			m_btnConfirm->Init(hwnd, IDS::Element::INPUTBOX_BTN_CONFIRM, confirmBtn);

			m_btnCancel->SetSize(Dimensions::BUTTON_WIDTH - 10, Dimensions::BAR_HEIGHT);
			m_btnConfirm->SetSize(Dimensions::BUTTON_WIDTH - 10, Dimensions::BAR_HEIGHT);

			m_labelTitle->SetBgColor(&Colors::GUIC_TEXT_BOX_BACKGROUND);
			m_inputField->SetCaretColor(&Colors::GUIC_TEXT_BOX_BACKGROUND);
			m_inputField->SetBgColor(&Colors::GUIC_TEXT_HIGHLIGHT);
			m_btnConfirm->SetBgColor(&Colors::GUIC_BUTTON_GREEN);
			m_btnConfirm->SetOpacityMinMax(1.0f, 0.9f);

			AddElement(bar);
			AddElement(m_labelTitle);
			AddElement(m_inputField);
			AddElement(m_btnCancel);
			AddElement(m_btnConfirm);


			m_inputField->AlignCenter(bar, true, true);
			m_btnCancel->SetPositionUnder(m_inputField);
			m_btnConfirm->SetPositionUnder(m_inputField)->AlignRight(m_inputField);
			m_labelTitle->SetPositionAbove(m_inputField);

			CenterWindow();
		}

		void CInputBox::SetInfo(std::wstring title, std::wstring hint)
		{
			m_inputField->SetHint(hint);
			m_labelTitle->SetText(title);
		}

		std::string CInputBox::GetText()
		{
			return m_inputField->GetText();
		}

		void CInputBox::Show()
		{
			CScenario::Show();
			// clear text
			m_inputField->SetText("");

			// set focus to inputfield
			m_inputField->Notify(m_gui, Notifications::GUIN_ENTER_INPUT_MODE);
			m_inputField->SetActiveState(true);
		}
	}
}