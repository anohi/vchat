#include "NetworkVoipServer.h"


namespace Voip {
	namespace Network {
		CNetworkVoipServer::CNetworkVoipServer()
		{
			ZeroMemory(m_ac, UDP_BUFFER_SIZE);
			ZeroMemory(m_acCollect, MAX_UDP_BUFFER_SIZE);
			m_iCollectionPosition = 0;
			m_iCollectionCount = 0;
			m_collection.clear();
			m_si_info_len = sizeof(m_si_info);
			m_bServerStarted = false;
		}

		CNetworkVoipServer::~CNetworkVoipServer()
		{

		}

		void CNetworkVoipServer::Start()
		{
			if (m_bServerStarted) {
				this->HandleError("NetowrkVoipServer::Start() -> already started");
			}
			else {
				std::thread server([=]() {
					m_bServerStarted = true;
					HandleRequest();
				});
				server.detach();

				std::thread statistics([=]() {
					while (m_bServerStarted) {
						Sleep(1500);

						system("cls");
						cout << "users (" << m_users.size() << ") {\n";
						for each(const auto& user in m_users) {
							cout << " " << *user.second << "\n";
						}
						cout << "}\n\n";

						cout << "channels (" << m_channels.size() << ") {\n";
						for each(const auto& channel in m_channels) {
							cout << " " << channel.second->GetChannelName() << "(" << channel.second->GetUsers()->size() << "/" << channel.second->GetMaxMembers() << ")\n";
						}
						cout << "}\n\n";

					}
				});
				statistics.detach();
			}
		}

		void CNetworkVoipServer::Stop()
		{
			// tell every client to stop

			// stop the server
			m_bServerStarted = false;

			// cleanup
			this->Cleanup();
		}

		void CNetworkVoipServer::HandleRequest()
		{
			int request;
			char caUsername[50], caUserID[10], caChannel[50], pass[50], ip[50];

			while (m_bServerStarted) {
				RecvCollection();
				if (!IsCollectionEmpty()) {
					ZeroMemory(caUsername, sizeof(caUsername));
					ZeroMemory(caUserID, sizeof(caUserID));
					ZeroMemory(caChannel, sizeof(caChannel));
					ZeroMemory(pass, sizeof(pass));

					request = ExtractI(); // what method does the client wants to perform?
					switch (request)
					{
					case START_VOIP:
						break;
					case END_VOIP:
						break;
					case VOIP_DATA:
					{
						//cout << "sound data received" << endl;

						ExtractS(caUsername);
						ExtractS(caUserID);
						ExtractS(caChannel);
						int iSize = NextCollectionItemSize();

						//CNetworkVoipChannel *channel = GetChannel(caChannel);
						auto user = GetUser(caUsername, caUserID);
						if (!user/* || !channel*/) {
							break;
						}

						char *soundData = new char[iSize];
						ExtractS(soundData, iSize);

						// do something with received data
						user->PushData(soundData, iSize);
						//channel->FillBuffer(user, soundData, iSize);

						break;
					}
					case TEXT_MESSAGE_USER:
						break;
					case TEXT_MESSAGE_CHANNEL:
						break;
					case TEXT_MESSAGE_SERVER:
						break;
					case JOIN_SERVER:
					{
						ExtractS(caUsername);
						ExtractS(pass);

						CNetworkVoipUser* user = nullptr;
						if (m_strServerPassword != "") {
							if (string(pass) == m_strServerPassword) {
								user = AcceptUser(caUsername, "");
							}
						}
						else {
							user = AcceptUser(caUsername, "");
						}

						if (caUserID) {
							int response = OK;
							ClearBuffers();
							Collect(response);
							Collect(user->GetID());
							Collect(user->GetIP());
							Collect(user->GetPort());
							SendCollection();
						}
						else {
							int response = FAIL;
							ClearBuffers();
							Collect(response);
							SendCollection();
						}
						break;
					}
					case LEAVE_SERVER:
					{
						ExtractS(caUsername);
						ExtractS(caUserID);
						RemoveUserFromServer(caUsername, caUserID);
						break;
					}
					case CREATE_CHANNEL:
					{
						ExtractS(caUsername);
						ExtractS(caUserID);
						ExtractS(caChannel);
						int maxMembers = ExtractI();

						bool created = CreateChannel(caChannel, caUsername, caUserID, maxMembers);
						SendB(created);
						break;
					}
					case JOIN_CHANNEL:
					{
						ExtractS(caUsername);
						ExtractS(caUserID);
						ExtractS(caChannel);
						int response = VoipError::OK;
						bool joined = false;
						CNetworkVoipChannel *thisChannel = GetChannel(caChannel);
						CNetworkVoipUser* user = GetUser(caUsername, caUserID);

						if (thisChannel) {
							if (user) {
								if (thisChannel->GetCurrentMembers() < thisChannel->GetMaxMembers()) {
									CNetworkVoipChannel *anyChannel = OnWhatChannelIsUser(user);

									if (!user || !thisChannel || (anyChannel == thisChannel)) {
										response = VoipError::FAIL;
										ClearBuffers();
										Collect(response);
										SendCollection();
										break;
									}
									// user on any other channel? leave old channel
									if (anyChannel) {
										anyChannel->RemoveUser(user);
									}

									joined = (thisChannel->AddUser(user) == VoipError::OK);
								}
								else {
									response = VoipError::CHANNEL_FULL;
								}
							}
							else {
								response = VoipError::USER_NOT_EXISTS;
							}
						}
						else {
							response = VoipError::CHANNEL_NOT_EXISTS;
						}


						ClearBuffers();
						Collect(response);
						if (joined) {
							int channelSize = thisChannel->GetCurrentMembers();
							Collect(channelSize);
							//for each (const auto &currentUser in *thisChannel->GetUsers()) {
							//	Collect(currentUser->GetUserID());
							//}
							
							thisChannel->Notify(JOIN_CHANNEL, user);

						}
						SendCollection();
						break;
					}
					case LEAVE_CHANNEL:
					{
						ExtractS(caUsername);
						ExtractS(caUserID);
						ExtractS(caChannel);

						RemoveUserFromChannel(caUsername, caUserID, caChannel);

						break;
					}
					case REMOVE_USER_FROM_CHANNEL:
					{
						ExtractS(caUsername);
						ExtractS(caUserID);
						ExtractS(caChannel);
						bool removed = RemoveUserFromChannel(caUsername, caUserID, caChannel);
						SendB(removed);
						break;
					}
					case GET_CHANNELS_LIST:
						RespondWithChannelList();
						break;
					case GET_CHANNEL_MEMBERS:
						break;
					case GET_USER:
						break;
					case GET_USERS_LIST:
					{
						ExtractS(caUsername);
						ExtractS(caUserID);
						ExtractS(caChannel);

						int response = VoipError::OK;
						ClearBuffers();
						CNetworkVoipChannel *channel = GetChannel(caChannel);
						CNetworkVoipUser* user = GetUser(caUsername, caUserID);

						if (!channel || !user) {
							response = VoipError::FAIL;
							Collect(response);
							SendCollection();
							break;
						}

						auto users = channel->GetUsers();
						int memberCount = users->size();
						Collect(response);
						Collect(memberCount);
						for (auto &user : *users) {
							Collect(user->GetUserID());
						}
						SendCollection();

						break;
					}
					default:
						break;
					}
				}
			}
		}


		CNetworkVoipUser* CNetworkVoipServer::AcceptUser(std::string strUsername, std::string strID)
		{
			if (!UserExists(strUsername, strID)) {
				srand(time(NULL));
				CNetworkVoipUser* user = new CNetworkVoipUser(strUsername, to_string(rand() % 888888 + 111111), m_clientHost,to_string(6000 + (rand() % 59536)));
				m_users[user->GetUserID()] = user;
				return user;
			}
			return FALSE;
		}

		bool CNetworkVoipServer::AddUserToChannel(std::string strUsername, std::string strID, std::string strChannelName)
		{
			CNetworkVoipUser* user = GetUser(strUsername, strID);
			CNetworkVoipChannel *thisChannel = GetChannel(strChannelName);
			CNetworkVoipChannel *anyChannel = OnWhatChannelIsUser(user);

			if (!user || !thisChannel) {
				return false;
			}

			// is already on this channel?
			if (anyChannel == thisChannel) {
				return false;
			}

			// user on any other channel? leave old channel
			if (anyChannel) {
				anyChannel->RemoveUser(user);
			}

			return (thisChannel->AddUser(user) == VoipError::OK);
		}

		bool CNetworkVoipServer::RemoveUserFromServer(std::string strUsername, std::string strID)
		{
			CNetworkVoipUser* user = GetUser(strUsername, strID);
			if (user && user->GetIP() == string(m_clientHost)) {
				if (RemoveUserFromChannel(strUsername, strID)) {
					// cleanup network resources
					user->GetUDPClient()->Cleanup();
				}
				// remove from server
				m_users.erase(user->GetUserID());
				return true;
			}
			return false;
		}

		bool CNetworkVoipServer::RemoveUserFromChannel(std::string strUsername, std::string strID, std::string strChannelName)
		{
			CNetworkVoipChannel *currentChannel;
			if (CNetworkVoipUser* user = GetUser(strUsername, strID)) {
				currentChannel = strChannelName.empty() ? OnWhatChannelIsUser(user) : GetChannel(strChannelName);
				if (currentChannel) {
					if (currentChannel->RemoveUser(user) == VoipError::OK) {
						currentChannel->Notify(LEAVE_CHANNEL, user);
						// remove channel if no one is on it anymore
						if (!currentChannel->GetCurrentMembers()) {
							RemoveChannel(currentChannel->GetChannelName());
						}
						return true;
					}
				}
			}
			return false;
		}

		bool CNetworkVoipServer::CreateChannel(std::string strChannelName, std::string strCreator, std::string strUserID, int iMaxMembers)
		{
			CNetworkVoipUser* user = GetUser(strCreator, strUserID);
			if (user && !ChannelExists(strChannelName)) {
				CNetworkVoipChannel *anyChannel = OnWhatChannelIsUser(user);
				if (anyChannel) {
					if (anyChannel->RemoveUser(user) != VoipError::OK) {
						return false;
					}
					if (!anyChannel->GetCurrentMembers()) {
						RemoveChannel(strChannelName);
					}
				}

				CNetworkVoipChannel *newChannel = new CNetworkVoipChannel();
				newChannel->SetChannelName(strChannelName);
				newChannel->SetMaxMembers(iMaxMembers);
				newChannel->AddUser(user);
				newChannel->SetOperator(user);
				m_channels[strChannelName] = newChannel;

				return true;
			}
			return false;
		}

		void CNetworkVoipServer::RemoveChannel(std::string strChannelName)
		{
			if (ChannelExists(strChannelName)) {
				m_channels.erase(strChannelName);
			}
		}

		void CNetworkVoipServer::ChangeUsername(std::string strOldUsername, std::string strNewUsername)
		{
		}

		void CNetworkVoipServer::RespondWithChannelList()
		{
			ClearBuffers();

			int channelSize = m_channels.size();
			Collect(channelSize);
			for each (auto const &channel in m_channels) {
				// channel name
				Collect(channel.first);
				// operator count
				int operatorCount = channel.second->GetOperators()->size();
				Collect(operatorCount);
				// operators
				//for each (CNetworkVoipUser &op in channel.second->GetOperators()) {
				//	Collect(op->GetUserID());
				//}
				// member count
				int memberCount = channel.second->GetUsers()->size();
				Collect(memberCount);
				// members
				//for each (auto &user in channel.second->GetUsers()) {
				//	Collect(user.first);
				//}
			}

			SendCollection();
		}

		CNetworkVoipUser * CNetworkVoipServer::GetUser(std::string strUsername, std::string strUserID)
		{
			if (UserExists(strUsername, strUserID)) {
				return m_users[strUsername + "#" + strUserID];
			}
			return nullptr;
		}

		CNetworkVoipChannel * CNetworkVoipServer::GetChannel(std::string strChannelName)
		{
			if (ChannelExists(strChannelName)) {
				return m_channels[strChannelName];
			}
			return nullptr;
		}

		CNetworkVoipChannel * CNetworkVoipServer::OnWhatChannelIsUser(CNetworkVoipUser* user)
		{
			for each (const auto &channel in m_channels) {
				if (channel.second->HasUser(user)) {
					return channel.second;
				}
			}
			return nullptr;
		}

		bool CNetworkVoipServer::UserExists(std::string strUserName, std::string strID)
		{
			return (m_users.find(strUserName + "#" + strID) != m_users.end());
		}

		bool CNetworkVoipServer::ChannelExists(std::string strChannelName)
		{
			return (m_channels.find(strChannelName) != m_channels.end());
		}

	}
}