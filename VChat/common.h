#pragma once
#ifndef DEBUG(msg, value)
#define DEBUG(msg, value) \
do { \
cout << (msg) << (value) << endl; \
} while (0)
#endif