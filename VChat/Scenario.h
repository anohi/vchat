#pragma once
#include <algorithm>
#include <vector>
#include "GuiHelpers.h"
#include "GuiElement.h"

namespace Voip {
	namespace Gui {
		class CScenario
		{
		protected:
			std::vector<CGuiElement*> m_elements;

			CScenario*			m_returnScenario;
			D2D1_POINT_2F		m_offset;
			D2D1_POINT_2F		m_center;
			D2D1_RECT_F			m_bounds;
			D2D1_MATRIX_3X2_F	m_transformation;

			HWND m_gui;
			bool m_bShouldRender;
			bool m_bIsActive;
			bool m_bZoomedIn;
			bool m_bIsTransitioning;
			bool m_bShowBackground;
			float m_zoomFactor;
			float m_opacity;
			int m_id;

			std::wstring m_title;
			IDWriteTextLayout* m_textLayoutTitle;

			void Notify(HWND hwnd, Notifications code);

		public:
			CScenario();
			~CScenario();

			virtual void Init(HWND hwnd, int id, bool bShowBackground = false);
			virtual void Fini();
			virtual void Show();
			virtual void Hide();

			virtual void Update(float &time, float &timeDelta);
			virtual void Render();
			virtual void SetOffset(float x, float y);
			virtual bool OnMouseMove(D2D1_POINT_2F &point);

			
			virtual CGuiElement* HitTest(D2D1_POINT_2F &point);
			virtual CGuiElement* GetElement(int id);
			CScenario* Return();
			D2D1_POINT_2F GetSize();

			int GetId();
			bool IsActive();
			bool ShouldRender();
			bool ShouldReturn();
			void ResetControlState();
			void SetTitle(std::wstring title);
			void SetActiveState(bool state);
			void SetReturnScenario(CScenario* scenario);
			void SetCenter(D2D1_POINT_2F point);
			void AddElement(CGuiElement *element);
			void AddElementLeftTo(CGuiElement *anchor, CGuiElement *element, float padding = Dimensions::PADDING);
			void AddElementRightTo(CGuiElement *anchor, CGuiElement *element, float padding = Dimensions::PADDING);
			void AddElementBelow(CGuiElement *anchor, CGuiElement *element, float padding = Dimensions::PADDING);
			void AddElementAbove(CGuiElement *anchor, CGuiElement *element, float padding = Dimensions::PADDING);
			void CenterWindow();
		};
	}
}