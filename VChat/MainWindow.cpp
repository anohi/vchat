#include "MainWindow.h"

namespace Voip {
	namespace Gui {

		CMainWindow::CMainWindow()
			:m_hInstance(NULL), m_parent(NULL), m_gui(NULL), m_bGuiVisible(false), m_bIsRunning(false), m_guiManager(nullptr), m_voipClient(nullptr),
			m_bLBDown(false), m_bRBDown(false)
		{
		}


		CMainWindow::~CMainWindow()
		{
			Fini();
		}

		bool CMainWindow::Init(HINSTANCE hInstance, HWND hWnd)
		{
			m_hInstance = hInstance;
			m_parent = hWnd;
			m_bGuiVisible = false;
			m_rect = { 0, 0, Dimensions::WINDOW_WIDTH, Dimensions::WINDOW_HEIGHT };
			//AdjustWindowRectEx(&m_rect, WS_OVERLAPPEDWINDOW, FALSE, WS_EX_OVERLAPPEDWINDOW);

			// Fenstertruktur:
			WNDCLASSEX wcex;
			wcex.hInstance = m_hInstance;
			wcex.lpszClassName = _T("MainWindow");;
			wcex.lpfnWndProc = this->helperWindowProc;
			wcex.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
			wcex.cbSize = sizeof(WNDCLASSEX);

			// Handles f�r das Vektoria-Icon und den Standardmauszeiger:
			wcex.hIcon = 0; // LoadIcon(m_hInstance, MAKEINTRESOURCE(1));
			wcex.hIconSm = 0;//LoadIcon(wcex.hInstance, MAKEINTRESOURCE(1));
			wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
			wcex.lpszMenuName = NULL;
			wcex.cbClsExtra = 0;
			wcex.cbWndExtra = 0;
			wcex.hbrBackground = CreateSolidBrush(COLORREF(RGB(30, 33, 36)));

			if (!RegisterClassEx(&wcex)) {
				MessageBox(NULL, _T("Call to RegisterClassEx failed!"), _T("MainWindow"), NULL);
				return false;
			}

			m_gui = CreateWindowEx(
				0,
				_T("MainWindow"),
				_T("Voip"),
				WS_VISIBLE,
				(GetSystemMetrics(SM_CXSCREEN) - m_rect.right - m_rect.left) / 2,	// center window horizontally
				(GetSystemMetrics(SM_CYSCREEN) - m_rect.bottom - m_rect.top) / 2,	// center window vertically
				m_rect.right,	// window width
				m_rect.bottom,	// window height
				HWND_DESKTOP,	// parent is desktop
				NULL,			// no menu
				m_hInstance,
				NULL
				);

			if (m_gui == NULL) {
				DWORD error = GetLastError();
				MessageBox(NULL, (LPCWSTR)std::to_string(error).c_str(), TEXT("Error"), MB_OK);
				return false;
			}

			// saves the pointer to this class for later usage
			SetWindowLongPtr(m_gui, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
			// remove window styles, we will create our own title bar
			SetWindowLong(m_gui, GWL_STYLE, WS_VISIBLE);
			// saves the window rect
			//GetWindowRect(m_gui, &m_rect);
			//AdjustWindowRectEx(&m_rect, WS_OVERLAPPEDWINDOW, FALSE, WS_EX_OVERLAPPEDWINDOW);
			

			// initialize Graphics (direct2d)
			m_guiManager = new CGuiManager();
			if (!m_guiManager) return false;
			if (!m_guiManager->Init(m_gui)) {
				delete m_guiManager;
				m_guiManager = nullptr;
				return false;
			}

			m_guiManager->Resize(D2D1::SizeU(m_rect.right, m_rect.bottom));

			// create and initialize scenarios
			CScenario* mainScenario = CreateMainScenario(IDS::Element::SCN_MAIN);
			CInputBox* newChannel = new CInputBox(); // CreateChannelScenario(IDS::Element::SCN_CREATE_CHANNEL);
			CScenario* exitScenario = CreateExitConfirmationScenario(IDS::Element::SCN_EXIT);
			CSettings* settings = new CSettings();
			CGroup* channel = new CGroup();
			CChannelList* channelList = new CChannelList();
			CInfoBox* infoBox = new CInfoBox();
			
			settings->Init(m_gui, IDS::Element::SCN_SETTTINGS);
			settings->SetTitle(L"Settings");

			channel->Init(m_gui, IDS::Element::SCN_CURRENT_CHANNEL);
			channelList->Init(m_gui, IDS::Element::SCN_CHANNEL_LIST);
			channelList->SetTitle(L"Channels");

			infoBox->Init(m_gui, IDS::Element::SCN_INFO_BOX, true);
			infoBox->SetTimeout(3);
			newChannel->Init(m_gui, IDS::Element::SCN_CREATE_CHANNEL, true);
			newChannel->SetInfo(L"CHANNEL NAME", L"Enter a channel name");

			m_guiManager->AddScenario(mainScenario);
			m_guiManager->AddScenario(newChannel);
			m_guiManager->AddScenario(channel);
			m_guiManager->AddScenario(channelList);
			m_guiManager->AddScenario(settings);
			m_guiManager->AddScenario(infoBox);
			m_guiManager->AddScenario(exitScenario);

			// link scenarios with each other
			settings->SetReturnScenario(mainScenario);
			newChannel->SetReturnScenario(mainScenario);
			exitScenario->SetReturnScenario(mainScenario);
			mainScenario->SetReturnScenario(exitScenario);
			channelList->SetReturnScenario(mainScenario);
			
			m_guiManager->SetActiveScenario(IDS::Element::SCN_MAIN);
			m_guiManager->ShowScenario(IDS::Element::SCN_MAIN);
			
			// initialize voip client
			m_voipClient = new Voip::Network::CNetworkVoipClient();
			m_voipClient->Init(m_gui);

			// load settings
			if (!settings->LoadSettings()) {
				return false;
			}

			// set the callback so the voip client can notify us about changes
			m_voipClient->SetCallBack([&](int code, void* data) {
				VoipCallback(code, data);
			});


			if (settings->IsAutoConnect()) {
				std::thread autoConnect([&]() {
					Connect();
				});

				autoConnect.detach();
			}



			
			// system tray icon
			//m_trayData.cbSize = sizeof(NOTIFYICONDATA);
			//m_trayData.hWnd = (HWND)m_gui;
			//m_trayData.uID = 1;
			//m_trayData.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
			//m_trayData.hIcon = LoadIcon(NULL, IDI_APPLICATION);;
			//m_trayData.uCallbackMessage = WM_USER_SHELLICON;
			//CopyMemory(m_trayData.szTip, L"Voip", sizeof(L"Voip"));
			////LoadString(hInstance, IDS_APPTOOLTIP, m_trayData.szTip, MAX_LOADSTRING);

			//BOOL bSuccess = Shell_NotifyIcon(NIM_ADD, &m_trayData);
			//if (!(bSuccess)) {
			//	MessageBox(m_gui, L"Unable to Set Tary Icon", L"Error", MB_OK);
			//	return false;
			//}



			return true;
		}

		void CMainWindow::Fini()
		{
			// save settings
			if (auto scn = m_guiManager->GetScenario(IDS::Element::SCN_SETTTINGS)) {
				dynamic_cast<CSettings*>(scn)->SaveSettings();
			}

			// clean up
			Hide();
			DestroyWindow(m_gui);
			m_bIsRunning = false;

			Helpers::SafeDelete(&m_guiManager);

			if (m_voipClient) {
				m_voipClient->LeaveServer();
				Helpers::SafeDelete(&m_voipClient);
			}
			//Shell_NotifyIcon(NIM_DELETE, &m_trayData);
			// quit
			PostQuitMessage(0);
		}

		void CMainWindow::Show()
		{
			m_bGuiVisible = true;
			m_bIsRunning = true;
			if (ShowWindow(m_gui, SW_SHOW)) {
				SetFocus(m_gui);
			}
		}

		void CMainWindow::Hide()
		{
			m_bGuiVisible = false;
			ShowWindow(m_gui, SW_HIDE);
		}

		void CMainWindow::Minimize()
		{
			m_bGuiVisible = false;
			PostMessage(m_gui, WM_SYSCOMMAND, SC_MINIMIZE, 0);
		}

		void Voip::Gui::CMainWindow::SysTray()
		{
			ShowWindow(m_gui, SW_HIDE);
		}

		bool CMainWindow::IsVisible()
		{
			DWORD style = GetWindowStyle(m_gui);
			if (style & WS_MINIMIZE || !m_bGuiVisible || !IsWindowVisible(m_gui)) return false;

			return true;
		}

		LRESULT CMainWindow::WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			switch (msg)
			{
			
			//case WM_ACTIVATE:		m_bGuiVisible = true; break;
			case WM_NCDESTROY:		Fini();		// frees all ressources
			case WM_ERASEBKGND:		return 1;	// we will erase the background in Tick()
			case WM_NOTIFY:			return OnNotify(wParam, lParam); // handle messages sent by child windows/controls
			case WM_KEYUP:			return OnKeyUp(wParam, lParam); // no time consuming actions as this will block Tick()
			case WM_LBUTTONDOWN:
				m_mousePos = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
				m_bLBDown = true;
				SetCapture(hWnd);
				break;
			case WM_LBUTTONUP: {
				if (!m_bRBDown) {
					m_guiManager->ExitInputMode();
					m_guiManager->HitTest(m_mousePos);
				}
				m_bLBDown = false;
				ReleaseCapture();
				break;
			}
			case WM_RBUTTONDOWN:
				SetCursor(Cursors::MOVING);
				m_mousePos = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
				m_bRBDown = true;
				SetCapture(hWnd);
				break;
			case WM_RBUTTONUP:
				m_bRBDown = false;
				ReleaseCapture();
				break;
			case WM_MOUSEMOVE: {
				// moving window when right button is held and mouse is moving 
				if (m_bRBDown) {
					SetWindowPos(m_gui, NULL, m_rect.left + (GET_X_LPARAM(lParam) - m_mousePos.x), m_rect.top + (GET_Y_LPARAM(lParam) - m_mousePos.y), 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_NOREDRAW);
					GetWindowRect(m_gui, &m_rect);
				}
				else {
					// tell elements to check if mouse is on it
					m_mousePos = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
					m_guiManager->OnMouseMove(m_mousePos);
				}
				break;
			}
			case WM_SIZE:
				if (m_guiManager) {
					GetClientRect(m_gui, &m_rect);
					m_guiManager->Resize(D2D1::SizeU(m_rect.right, m_rect.bottom));
				}
				break;
			case WM_USER_SHELLICON:
				switch (LOWORD(lParam)) {
				case WM_LBUTTONDBLCLK:
					ShowWindow(m_gui, SW_SHOW);
				case WM_RBUTTONDOWN:

					break;
				case WM_MOUSEMOVE:
					return 0;
				}
				break;
			case WM_CHAR:
				if (auto element = m_guiManager->GetActiveInputElement()) {
					dynamic_cast<CInputfield*>(element)->HandleInput(wParam);
				}
				break;
			default:
				break;
			}

			return DefWindowProc(hWnd, msg, wParam, lParam);
		}

		LRESULT CMainWindow::helperWindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			CMainWindow* self = reinterpret_cast<CMainWindow*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
			if (self == NULL) {
				return DefWindowProc(hWnd, msg, wParam, lParam);
			}
			return self->WindowProc(hWnd, msg, wParam, lParam);
		}

		LRESULT CMainWindow::OnNotify(WPARAM wParam, LPARAM lParam)
		{
			LPNMHDR param = LPNMHDR(lParam);
			switch (param->code)
			{
			case Notifications::GUIN_SETCURSOR_EDIT:
				SetCursor(Cursors::EDIT);
				return TRUE;
			case Notifications::GUIN_SETCURSOR_HAND:
				SetCursor(Cursors::HAND);
				return TRUE;
			case Notifications::GUIN_ENTER_INPUT_MODE: {
				m_guiManager->SetActiveInputElement(reinterpret_cast<CInputfield*>(param->idFrom));
				return TRUE;
			}
			case Notifications::GUIN_EXIT_INPUT_MODE:
				m_guiManager->SetActiveInputElement(NULL);
				return TRUE;
			case Notifications::GUIN_ENTER_SCENARIO:
				if (auto scenario = reinterpret_cast<CScenario*>(param->idFrom)) {
					auto id = scenario->GetId();
					
					switch (id)
					{
					case IDS::Element::SCN_EXIT:
						break;
					case IDS::Element::SCN_SETTTINGS:
						dynamic_cast<CSettings*>(scenario)->LoadSettings();
						break;
					case IDS::Element::SCN_INFO_BOX:
						m_guiManager->DeactivateAllScenarios();
						m_guiManager->SetActiveScenario(id);
						break;
					case IDS::Element::SCN_CHANNEL_LIST:
						
						break;
					default:
						break;
					}
					/*m_guiManager->SetActiveScenario(id);*/
				}
				return TRUE;
			case Notifications::GUIN_SCENARIO_RETURN: {
				if (auto scenario = reinterpret_cast<CScenario*>(param->idFrom)) {
					m_guiManager->ActivateAllScenarios(true); // reactivate all visible scenarios
					auto id = scenario->GetId();
					switch (id) {
					case IDS::Element::SCN_SETTTINGS:
						dynamic_cast<CSettings*>(scenario)->SaveSettings();
						break;
					case IDS::Element::SCN_CREATE_CHANNEL: // joining the channel
						
						break;
					}					
				}
				return TRUE;
			}
			case Notifications::GUIN_INPUT_TEXT_RETURN: {
				// check what inputfield has set the value
				if (auto inputField = reinterpret_cast<CInputfield*>(param->idFrom)) {
					switch (inputField->GetId())
					{
						// a new channel has been entered, create it
					case IDS::INPUTBOX_INPUTFIELD: {
						std::string channel = inputField->GetText();
						CreateChannel(channel);
						m_guiManager->Return();
						break;
					}
						
					case IDS::Element::IN_SEARCH_BOX:
						break;
					case IDS::Element::IN_SETTINGS_CHANNEL:
						break;
					case IDS::Element::IN_SETTINGS_USERNAME:
						break;
					case IDS::Element::IN_SETTINGS_SERVER_ADDR:
						break;
					default:
						break;
					}
				}
				return TRUE;
			}
			case Notifications::GUIN_CHANNEL_ITEM_CLICKED: {
				if (auto channel = reinterpret_cast<CChannel*>(param->idFrom)) {
					auto channelName = channel->GetChannelName();
					if (!channelName.empty()) {
						Join(channelName);
					}
				}
				return TRUE;
			}
			case Notifications::GUIN_BUTTON_CLICKED: {
				if (auto button = reinterpret_cast<CButtonRect*>(param->idFrom)) {
					switch (button->GetId())
					{
					case IDS::Element::BTN_MAIN_JOIN_CHANNEL:
						if (m_voipClient->IsOnServer() && !m_voipClient->IsOnChannel()) {
							m_guiManager->HideAllScenarios(IDS::Element::SCN_CHANNEL_LIST);
							m_guiManager->ShowScenario(IDS::Element::SCN_CHANNEL_LIST);
							m_guiManager->SetActiveScenario(IDS::Element::SCN_CHANNEL_LIST);
							// get the channels and fill the list
							auto channels = m_voipClient->GetChannelList();
							if (auto channelList = dynamic_cast<CChannelList*>(m_guiManager->GetScenario(IDS::Element::SCN_CHANNEL_LIST))) {
								channelList->Reset();
								channelList->UpdateChannelList(channels);
							}
						}
						break;
					case IDS::Element::BTN_MAIN_CREATE_CHANNEL: {
						//m_guiManager->HideAllScenarios(IDS::Element::SCN_CREATE_CHANNEL);
						m_guiManager->DeactivateAllScenarios();
						m_guiManager->ShowScenario(IDS::Element::SCN_CREATE_CHANNEL);
						m_guiManager->SetActiveScenario(IDS::Element::SCN_CREATE_CHANNEL);
						break;
					}
					case IDS::Element::BTN_MAIN_SETTINGS:
						m_guiManager->HideAllScenarios(IDS::Element::SCN_SETTTINGS);
						m_guiManager->ShowScenario(IDS::Element::SCN_SETTTINGS);
						m_guiManager->SetActiveScenario(IDS::Element::SCN_SETTTINGS);

						//m_guiManager->DeactivateAllScenarios();
						//m_guiManager->ShowScenario(IDS::Element::SCN_SETTTINGS);
						//m_guiManager->SetActiveScenario(IDS::Element::SCN_SETTTINGS);
						break;
					case IDS::Element::BTN_MAIN_CONNECT:
						Connect();
						break;
					case IDS::Element::BTN_MAIN_DISCONNECT:
						Disconnect();
						break;
					case IDS::Element::BTN_MAIN_MUTE_MIC:
						break;
					case IDS::Element::BTN_MAIN_MUTE_SPEAKER:
						break;
					case IDS::Element::BTN_MAIN_LEAVE_CHANNEL:
						Leave();
						break;
					case IDS::Element::BTN_MAIN_INFO:
						break;
					case IDS::Element::BTN_MAIN_QUIT:
						m_guiManager->DeactivateAllScenarios(IDS::Element::SCN_EXIT);
						m_guiManager->ShowScenario(IDS::Element::SCN_EXIT);
						m_guiManager->SetActiveScenario(IDS::Element::SCN_EXIT);
						break;
					case IDS::Element::BTN_CANCEL:
					case IDS::Element::BTN_SETTINGS_DONE:
						m_guiManager->Return();
						break;
					case IDS::Element::BTN_EXIT_CONFIRM:
						Fini();
						break;
					case IDS::INPUTBOX_BTN_CONFIRM: {
						if (auto inputField = m_guiManager->GetElement(IDS::INPUTBOX_INPUTFIELD)) {
							auto text = dynamic_cast<CInputfield*>(inputField)->GetText();
							if (!text.empty()) {
								CreateChannel(text);
								m_guiManager->Return();
							}
							else {
								m_guiManager->SetActiveInputElement(inputField);
								inputField->SetActiveState(true);
								dynamic_cast<CInputfield*>(inputField)->UpdateTextLayout();
							}
						}
						break;
					}
					default:
						break;
					}
				}
				return TRUE;
			}
			default:
				break;
			}

			return FALSE;
		}

		LRESULT CMainWindow::OnKeyUp(WPARAM wParam, LPARAM lParam)
		{
			switch (wParam)
			{
			case VK_F1: // displays help menu
				m_guiManager->ShowScenario(IDS::Element::SCN_HELP);
				break;
			case VK_F2:
				if (m_guiManager->GetActiveScenario() && m_guiManager->GetActiveScenario()->GetId() != IDS::Element::SCN_SETTTINGS) {
					m_guiManager->DeactivateAllScenarios(true);
					m_guiManager->ShowScenario(IDS::Element::SCN_SETTTINGS);
					m_guiManager->SetActiveScenario(IDS::Element::SCN_SETTTINGS);
				}
				break;
			case VK_F3: // bring window to center of the screen
				break;
			case VK_ESCAPE: // before quitting app
				if (auto activeScn = m_guiManager->GetActiveScenario()) {
					if (activeScn->GetId() == IDS::Element::SCN_MAIN) {
						m_guiManager->DeactivateAllScenarios(IDS::Element::SCN_EXIT);
						m_guiManager->ShowScenario(IDS::Element::SCN_EXIT);
						m_guiManager->SetActiveScenario(IDS::Element::SCN_EXIT);
						break;
					}
				}
				m_guiManager->Return();
				break;
			default:
				break;
			}

			return 1;
		}

		void CMainWindow::Tick(float time, float timeDelta)
		{
			// update
			m_guiManager->Update(time, timeDelta);

			// render
			m_guiManager->Render();

		}

		void CMainWindow::Connect()
		{
			if (auto scn = m_guiManager->GetScenario(IDS::Element::SCN_SETTTINGS)) {
				auto settings = dynamic_cast<CSettings*>(scn);
				if (!settings->GetServer().empty()) {
					if (settings->GetPort() > 0) {
						if (!settings->GetUsername().empty()) {
								auto error = m_voipClient->JoinServer(settings->GetUsername(), settings->GetServer(), settings->GetPort());
								if (error == Voip::Network::VoipError::OK) {
									m_guiManager->HideElement(IDS::Element::BTN_MAIN_CONNECT);
									m_guiManager->ShowElement(IDS::Element::BTN_MAIN_DISCONNECT);
									m_guiManager->EnableElement(IDS::Element::BTN_MAIN_CREATE_CHANNEL);
									m_guiManager->EnableElement(IDS::Element::BTN_MAIN_JOIN_CHANNEL);

									if (settings->IsAutoJoin() && !settings->GetChannel().empty()) {
										Join(settings->GetChannel(), true);
									}
								}
								else {
									ShowInfo(L"Can't connect to server!");
								}
						}
						else {
							ShowInfo(L"No username specified.");
						}
					}
					else {
						ShowInfo(L"Missing port!");
					}
				}
				else {
					ShowInfo(L"No server specified.");
				}
			}		
		}

		// leaves the current Channel and disconnect from the server
		void CMainWindow::Disconnect()
		{
			Leave();
			m_voipClient->LeaveServer();

			m_guiManager->HideElement(IDS::Element::BTN_MAIN_DISCONNECT);
			m_guiManager->ShowElement(IDS::Element::BTN_MAIN_CONNECT);
			m_guiManager->DisableElement(IDS::Element::BTN_MAIN_CREATE_CHANNEL);
			m_guiManager->DisableElement(IDS::Element::BTN_MAIN_JOIN_CHANNEL);

			m_guiManager->HideElement(IDS::Element::BTN_MAIN_INFO);
			m_guiManager->HideElement(IDS::Element::BTN_MAIN_MUTE_MIC);
			m_guiManager->HideElement(IDS::Element::BTN_MAIN_MUTE_SPEAKER);
			m_guiManager->HideElement(IDS::Element::BTN_MAIN_LEAVE_CHANNEL);

			m_guiManager->HideScenario(IDS::Element::SCN_CURRENT_CHANNEL);
		}

		void CMainWindow::Join(std::string channel, bool autoJoin)
		{
			if (m_voipClient->JoinChannel(channel)) {
				if (auto scenario = m_guiManager->GetScenario(IDS::Element::SCN_CURRENT_CHANNEL)) {
					m_guiManager->ShowScenario(IDS::Element::SCN_CURRENT_CHANNEL);
					auto users = m_voipClient->GetUsers();
					if (users.size()) {
						auto channelScenario = dynamic_cast<CGroup*>(scenario);
						channelScenario->UpdateUserList(users);
					}

					if (auto info = m_guiManager->GetElement(IDS::Element::BTN_MAIN_INFO)) {
						auto channelName = m_voipClient->GetChannelName();
						dynamic_cast<CButtonRect*>(info)->SetText(std::wstring(channelName.cbegin(), channelName.cend()));
					}

					m_guiManager->HideElement(IDS::Element::BTN_MAIN_CREATE_CHANNEL);
					m_guiManager->HideElement(IDS::Element::BTN_MAIN_JOIN_CHANNEL);
					m_guiManager->ShowElement(IDS::Element::BTN_MAIN_INFO);
					m_guiManager->ShowElement(IDS::Element::BTN_MAIN_MUTE_MIC);
					m_guiManager->ShowElement(IDS::Element::BTN_MAIN_MUTE_SPEAKER);
					m_guiManager->ShowElement(IDS::Element::BTN_MAIN_LEAVE_CHANNEL);

					if (!autoJoin)
						m_guiManager->Return();
				}
			}
			else {
				if (autoJoin) {
					ShowInfo(L"Can't auto-join <" + std::wstring(channel.begin(), channel.end()) + L">");
				}
				else {
					ShowInfo(L"Can't join <" + std::wstring(channel.begin(), channel.end()) + L">");
				}
			}
		}

		void CMainWindow::CreateChannel(std::string channelName) {
			if (m_voipClient->CreateChannel(channelName)) {
				if (auto channelScenario = m_guiManager->GetScenario(IDS::Element::SCN_CURRENT_CHANNEL)) {
					m_guiManager->ShowScenario(IDS::Element::SCN_CURRENT_CHANNEL);
					auto users = m_voipClient->GetUsers();
					if (users.size()) {
						auto channel = dynamic_cast<CGroup*>(channelScenario);
						channel->UpdateUserList(users);
					}

					if (auto info = m_guiManager->GetElement(IDS::Element::BTN_MAIN_INFO)) {
						auto channelName = m_voipClient->GetChannelName();
						dynamic_cast<CButtonRect*>(info)->SetText(std::wstring(channelName.cbegin(), channelName.cend()));
					}

					m_guiManager->HideElement(IDS::Element::BTN_MAIN_CREATE_CHANNEL);
					m_guiManager->HideElement(IDS::Element::BTN_MAIN_JOIN_CHANNEL);
					m_guiManager->ShowElement(IDS::Element::BTN_MAIN_INFO);
					m_guiManager->ShowElement(IDS::Element::BTN_MAIN_MUTE_MIC);
					m_guiManager->ShowElement(IDS::Element::BTN_MAIN_MUTE_SPEAKER);
					m_guiManager->ShowElement(IDS::Element::BTN_MAIN_LEAVE_CHANNEL);
				}
			}
			else {
				// TODO: display create Channel error
				ShowInfo(L"Can't create channel.");
			}
		}

		void CMainWindow::Leave()
		{
			m_voipClient->LeaveChannel();
			m_guiManager->SetActiveScenario(IDS::Element::SCN_MAIN);
			m_guiManager->HideScenario(IDS::Element::SCN_CURRENT_CHANNEL);

			m_guiManager->ShowElement(IDS::Element::BTN_MAIN_CREATE_CHANNEL);
			m_guiManager->ShowElement(IDS::Element::BTN_MAIN_JOIN_CHANNEL);
			m_guiManager->HideElement(IDS::Element::BTN_MAIN_INFO);
			m_guiManager->HideElement(IDS::Element::BTN_MAIN_MUTE_MIC);
			m_guiManager->HideElement(IDS::Element::BTN_MAIN_MUTE_SPEAKER);
			m_guiManager->HideElement(IDS::Element::BTN_MAIN_LEAVE_CHANNEL);
		}

		void CMainWindow::ShowChannelList()
		{

		}

		// create main scenario
		CScenario* CMainWindow::CreateMainScenario(int id)
		{
			CScenario* mainScenario = new CScenario();
			CButtonRect* mainScenario_btnJoinChannel = new CButtonRect();
			CButtonRect* mainScenario_btnCreateChannel = new CButtonRect();
			CButtonRect* mainScenario_btnSettings = new CButtonRect();
			CButtonRect* mainScenario_btnQuit = new CButtonRect();
			CButtonRect* mainScenario_btnConnect = new CButtonRect();
			CButtonRect* mainScenario_btnDisconnect = new CButtonRect();

			CButtonRect* mainScenario_btnMuteMic = new CButtonRect();
			CButtonRect* mainScenario_btnMuteSpeaker = new CButtonRect();
			CButtonRect* mainScenario_btnLeaveChannel = new CButtonRect();
			CButtonRect* mainScenario_btnInfo = new CButtonRect();

			mainScenario->Init(m_gui, id);
			mainScenario_btnCreateChannel->Init(m_gui, IDS::Element::BTN_MAIN_CREATE_CHANNEL, L"Create");
			mainScenario_btnJoinChannel->Init(m_gui, IDS::Element::BTN_MAIN_JOIN_CHANNEL, L"Join");
			mainScenario_btnSettings->Init(m_gui, IDS::Element::BTN_MAIN_SETTINGS, L"Settings");
			mainScenario_btnConnect->Init(m_gui, IDS::Element::BTN_MAIN_CONNECT, L"Connect");
			mainScenario_btnDisconnect->Init(m_gui, IDS::Element::BTN_MAIN_DISCONNECT, L"Connected");
			mainScenario_btnQuit->Init(m_gui, IDS::Element::BTN_MAIN_QUIT, L"Quit");
			mainScenario_btnInfo->Init(m_gui, IDS::Element::BTN_MAIN_INFO);
			mainScenario_btnMuteMic->Init(m_gui, IDS::Element::BTN_MAIN_MUTE_MIC, L"Mic");
			mainScenario_btnMuteSpeaker->Init(m_gui, IDS::Element::BTN_MAIN_MUTE_SPEAKER, L"Sp");
			mainScenario_btnLeaveChannel->Init(m_gui, IDS::Element::BTN_MAIN_LEAVE_CHANNEL, L"Leave");

			// set size
			mainScenario_btnSettings->SetSize(Dimensions::BUTTON_WIDTH, Dimensions::BUTTON_SMALL_HEIGHT);
			mainScenario_btnDisconnect->SetSize(Dimensions::BUTTON_WIDTH, Dimensions::BUTTON_WIDTH);
			mainScenario_btnConnect->SetSize(Dimensions::BUTTON_WIDTH, Dimensions::BUTTON_WIDTH);
			mainScenario_btnQuit->SetSize(Dimensions::BUTTON_WIDTH, Dimensions::BUTTON_SMALL_HEIGHT);
			mainScenario_btnInfo->SetSize(Dimensions::BUTTON_WIDTH * 2 + Dimensions::PADDING, Dimensions::BUTTON_SMALL_HEIGHT);
			mainScenario_btnMuteMic->SetSize(Dimensions::BUTTON_SMALL_WIDTH, Dimensions::BUTTON_SMALL_HEIGHT);
			mainScenario_btnMuteSpeaker->SetSize(Dimensions::BUTTON_SMALL_WIDTH, Dimensions::BUTTON_SMALL_HEIGHT);
			mainScenario_btnLeaveChannel->SetSize(Dimensions::BUTTON_WIDTH, Dimensions::BUTTON_SMALL_HEIGHT);

			// set colors
			mainScenario_btnDisconnect->SetBgColor(&Colors::GUIC_BUTTON_GREEN);
			mainScenario_btnConnect->SetBgColor(&Colors::GUIC_ICON_YELLOW);
			mainScenario_btnLeaveChannel->SetBgColor(&Colors::GUIC_BUTTON_GREEN);
			mainScenario_btnQuit->SetBgColor(&Colors::GUIC_BUTTON_BLUE);
			mainScenario_btnCreateChannel->SetBgColor(&Colors::GUIC_TEXT_BOX_BACKGROUND);
			mainScenario_btnJoinChannel->SetBgColor(&Colors::GUIC_TEXT_BOX_BACKGROUND);
			mainScenario_btnInfo->SetBgColor(&Colors::GUIC_ICON_YELLOW);
			mainScenario_btnMuteMic->SetBgColor(&Colors::GUIC_BUTTON_GREEN_HOVER);
			mainScenario_btnMuteSpeaker->SetBgColor(&Colors::GUIC_BUTTON_GREEN_HOVER);

			// set images
			mainScenario_btnConnect->SetBitmap(Images::DISCONNECTED);
			mainScenario_btnDisconnect->SetBitmap(Images::CONNECTED);

			// set positions / alignments
			mainScenario->AddElement(mainScenario_btnCreateChannel);
			mainScenario->AddElementRightTo(mainScenario_btnCreateChannel, mainScenario_btnJoinChannel);
			mainScenario->AddElementBelow(mainScenario_btnCreateChannel, mainScenario_btnConnect);
			mainScenario->AddElementBelow(mainScenario_btnCreateChannel, mainScenario_btnDisconnect);
			mainScenario->AddElementBelow(mainScenario_btnJoinChannel, mainScenario_btnSettings);
			mainScenario->AddElementBelow(mainScenario_btnSettings, mainScenario_btnQuit);
			mainScenario->AddElement(mainScenario_btnInfo);
			mainScenario->AddElementBelow(mainScenario_btnInfo, mainScenario_btnMuteMic);
			mainScenario->AddElementRightTo(mainScenario_btnMuteMic, mainScenario_btnMuteSpeaker);
			mainScenario->AddElementRightTo(mainScenario_btnMuteSpeaker, mainScenario_btnLeaveChannel);

			// disable some controls
			mainScenario_btnCreateChannel->SetInteractivity(false);
			mainScenario_btnJoinChannel->SetInteractivity(false);

			// hide some controls
			mainScenario_btnDisconnect->SetVisibility(false);
			mainScenario_btnInfo->SetVisibility(false);
			mainScenario_btnLeaveChannel->SetVisibility(false);
			mainScenario_btnMuteMic->SetVisibility(false);
			mainScenario_btnMuteSpeaker->SetVisibility(false);


			//mainScenario->SetOffset((m_rect.right - mainScenario->GetSize().x)/2, (m_rect.bottom - mainScenario->GetSize().y)/2);
			mainScenario->CenterWindow();
			return mainScenario;
			
		}

		CScenario * CMainWindow::CreateExitConfirmationScenario(int id)
		{
			CScenario* exitScenario = new CScenario();
			CLabel* message = new CLabel();
			CButtonRect* cancel = new CButtonRect();
			CButtonRect* confirm = new CButtonRect();
			CButtonRect* textBar = new CButtonRect();
			CButtonRect* buttonBar = new CButtonRect();

			exitScenario->Init(m_gui, id, true);
			message->Init(IDS::Element::L_EXIT_LABEL, L"EXITING APP. ARE YOU SURE?");
			cancel->Init(m_gui, IDS::Element::BTN_CANCEL, L"Cancel");
			confirm->Init(m_gui, IDS::Element::BTN_EXIT_CONFIRM, L"Yes");
			cancel->SetSize(Dimensions::BUTTON_WIDTH, Dimensions::BAR_HEIGHT);
			confirm->SetSize(Dimensions::BUTTON_WIDTH, Dimensions::BAR_HEIGHT);
			confirm->SetBgColor(&Colors::GUIC_STATUS_RED);
			cancel->SetOpacityMinMax(1.f, .8f);
			confirm->SetOpacityMinMax(1.f, .8f);

			message->SetBgColor(&Colors::WHITE);
			message->SetFontSize(24);

			textBar->Init(m_gui, 0);
			textBar->SetBgColor(&Colors::GUIC_BUTTON_BLUE);
			textBar->SetOpacityMinMax(0.8f, 0.8f);
			textBar->SetInteractivity(false);
			textBar->SetSize(Dimensions::WINDOW_WIDTH+Dimensions::PADDING_BIG, 1.5*Dimensions::BUTTON_SMALL_HEIGHT);
			buttonBar->Init(m_gui, 0);
			buttonBar->SetBgColor(&Colors::GUIC_BUTTON_BLUE_HOVER);
			buttonBar->SetOpacityMinMax(0.3f, 0.3f);
			buttonBar->SetSize(Dimensions::WINDOW_WIDTH+Dimensions::PADDING_BIG, Dimensions::BUTTON_SMALL_HEIGHT + 10);
			buttonBar->SetInteractivity(false);

			exitScenario->AddElement(textBar);
			exitScenario->AddElementBelow(textBar, buttonBar, 0);
			exitScenario->AddElement(message);
			exitScenario->AddElement(confirm);
			exitScenario->AddElement(cancel);

			// has to be called after all Scenario::AddElement() functions calls as these will override the position
			buttonBar->AlignBottom(textBar);
			message->AlignCenter(textBar, true, true);
			cancel->SetPositionUnder(textBar)->AlignLeft(message);
			confirm->SetPositionRightTo(cancel);
			

			exitScenario->CenterWindow();

			return exitScenario;
		}

		void CMainWindow::ShowInfo(std::wstring text)
		{
			m_guiManager->ShowInfoBox(text);
		}

		void CMainWindow::VoipCallback(int code, void * data)
		{
			switch (code)
			{
			case Network::VoipRequest::JOIN_CHANNEL: {
				if (auto scn = m_guiManager->GetScenario(IDS::Element::SCN_CURRENT_CHANNEL)) {
					dynamic_cast<CGroup*>(scn)->UpdateUserList(m_voipClient->GetUsers());
					string user((char*)data);
					ShowInfo(L"<" + wstring(user.begin(), user.end()) + L"> has joined");
				}
				break;
			}
			case Network::VoipRequest::LEAVE_CHANNEL: {
				if (auto scn = m_guiManager->GetScenario(IDS::Element::SCN_CURRENT_CHANNEL)) {
					dynamic_cast<CGroup*>(scn)->UpdateUserList(m_voipClient->GetUsers());
					string user((char*)data);
					ShowInfo(L"<" + wstring(user.begin(),user.end()) + L"> has left");
				}
				break;
			}
			case Network::VoipRequest::VOIP_DATA: {
				if (data) {
					auto talkers = (std::vector<string>*)(data);
					if (auto scn = m_guiManager->GetScenario(IDS::Element::SCN_CURRENT_CHANNEL)) {
						dynamic_cast<CGroup*>(scn)->UpdateTalkersList(talkers);
					}
				}
				break;
			}
			default:
				break;
			}
		}
	}
}