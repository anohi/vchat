#pragma once
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <IPHlpApi.h>

#include "common.h"
#include <string>
#include <iostream>

#pragma comment(lib, "Ws2_32.lib")

using namespace std;

class VTCPStream
{
public:
	friend class VTCPAcceptor;
	friend class VTCPConnector;

	~VTCPStream();

	int  Send(char* pcBuffer, size_t length);
	int  Receive(char* pcBuffer, size_t length);
	void Fini();

private:
	// private constructors so only VTCPAcceptor and VTCPConnector can create instances of VTCPStream
	VTCPStream();
	VTCPStream(const char* pcHostname, int iPort);
	VTCPStream(SOCKET socket);
	VTCPStream(const VTCPStream& stream);

	bool Init(bool bIsClient = true);
	bool Connect();
	bool BindAndListen();
	SOCKET Accept();

	WSADATA     m_wsaData;
	SOCKET      m_clientSocket, m_listenSocket;
	addrinfo   *m_pAddrinfoResult, *m_pAddrinfoPtr, m_AddrinfoHints;
	int		    m_iReceiveBufferLength;
	int		    m_iPort;
	std::string m_sHostname;
};

