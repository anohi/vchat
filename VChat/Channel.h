#pragma once
#include "GuiHelpers.h"
#include "GuiElement.h"

namespace Voip {
	namespace Gui {
		class CChannel : public CGuiElement
		{
			std::wstring m_channelName;
			IDWriteTextLayout* m_textLayout;

		public:
			CChannel();
			~CChannel();

			// Geerbt �ber CGuiElement
			virtual bool OnMouseMove(D2D1_POINT_2F & point) override;
			virtual bool HitTest(D2D1_POINT_2F & point) override;
			virtual void Update(float & time, float & timeDelta) override;
			virtual void Render() override;
			virtual void Move(float xDelta, float yDelta) override;
			virtual void Fini() override;
			virtual void SetSize(float x, float y) override;
			virtual void Reset() override;

			virtual void Init(HWND hwnd, int id, std::wstring channelName);
			void CChannel::SetChannelName(std::wstring channelName);
			std::string GetChannelName();

		};

	}
}

