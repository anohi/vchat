#pragma once
#include <vector>
#include <map>
#include "GuiHelpers.h"
#include "Scenario.h"
#include "Inputfield.h"
#include "InfoBox.h"

namespace Voip {
	namespace Gui {
		class CGuiManager
		{
			std::vector<CScenario*> m_scenarios;
			//std::map<int, CScenario*> m_scenarios;
			CScenario *m_activeScenario;
			
			bool m_bInputMode;
			CGuiElement* m_activeInputElement;
		public:
			CGuiManager();
			~CGuiManager();

			bool Init(HWND hWnd);
			void Fini();

			void ClearScreen(float r, float g, float b);
			void ClearScreen(int r, int g, int b);
			void ClearScreen(const D2D_COLOR_F *color);
			void Update(float &time, float &timeDelta);
			void Render();
			void Resize(D2D1_SIZE_U size);

			void AddScenario(CScenario* scenario);
			void SetActiveScenario(int id);
			void ActivateScenario(int id);
			void DeactivateScenario(int id);
			void ActivateAllScenarios(bool bVisible = false);	// bVisible = [true : deactivate only visible scenarios, false : all]
			void DeactivateAllScenarios(bool bVisible = false); // bVisible = [true : deactivate only visible scenarios, false : all]
			void ShowScenario(int, bool enabled = true);
			void HideScenario(int, bool enabled = false);
			void HideAllScenarios(int except=0);
			void Return();
			void DisableElement(int  id, bool hide = true, int scenario = 0);
			void EnableElement(int id, bool show = true, int scenario = 0);
			void ShowElement(int id, bool enable = true, int scenario = 0);
			void HideElement(int id, bool disable = true, int scenario = 0);
			void DisplayTip(D2D1_POINT_2F &pos, std::string text);
			void SetActiveInputElement(CGuiElement* element);
			void ExitInputMode();
			void ShowInfoBox(std::wstring text);

			bool OnMouseMove(D2D1_POINT_2F &point);
			CGuiElement* HitTest(D2D1_POINT_2F &point);
			
			CGuiElement* GetActiveInputElement();
			CGuiElement* GetElement(int id, int scenario = 0);
			CScenario* GetActiveScenario();
			CScenario* GetScenario(int id);
			
		};
	}
}