#pragma once
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <mmsystem.h>
#include <dsound.h>

#include "common.h"
#include <vector>
#include <map>
#include <string>
#include <thread>
#include <iostream>

#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "winmm.lib")

#include "SoundBuffer.h"

namespace Voip {
	namespace Sound {

		class CSoundRecorder
		{
		public:
			CSoundRecorder();
			~CSoundRecorder();

			enum RecorderError
			{
				NOT_INITIALIZED,
				CANT_CREATE_CAPTURE_DEVICE_OBJECT,
				CANT_CREATE_CAPTURE_BUFFER,
				CANT_CREATE_DEVICE_OBJECT,
				CANT_CREATE_SECONDARY_BUFFER,
				CANT_GET_DEVICE_CAPABILITIES,
				CANT_READ_CAPTURED_DATA,
				CANT_SET_COOPERATIVE_LEVEL,
				CANT_SET_CAPTURE_NOTIFICATIONS,
				CANT_SET_NOTIFICATIONS,
				CANT_WRITE_TO_BUFFER,
				NONE,
			};

			bool Init(HWND hwnd);
			bool InitMicrophone(HWND hwnd);
			RecorderError LastError();

			void StartCapture();
			void StopCapture();
			bool ToggleMuteMic();
			bool SetVolumeMin();
			bool SetVolumeMax();
			bool MuteMic(bool state);
			bool IsMicMuted();
			bool IsSpeakerMuted();

			HRESULT SetNotifications(DWORD capture);

			// returns capture notifications
			const std::vector<HANDLE>* GetNotifications();
			
			// returns the length of a capture segment
			DWORD GetSegmentLength();

			int ReadCapturedData(DWORD uiSegment, short *psBuffer);

			BOOL CreateSoundBuffer(std::string strIdentifier, WAVEFORMATEX wfxFormat);
			BOOL FillSoundBuffer(std::string strIdentifier, short *psSoundData, DWORD dwSize);
			BOOL FillSilence(std::string strIdentifier, DWORD dwSegment, DWORD dwSize);
			BOOL PlayBuffer(std::string strIdentifier, DWORD dwFlags = DSBPLAY_LOOPING);
			BOOL PlayAllSound(DWORD dwFlags = DSBPLAY_LOOPING);
			BOOL StopBuffer(std::string strIdentifier);
			BOOL StopAllSound();
			
			bool BufferExists(std::string strIdentifier);
			void DeleteSoundBuffer(std::string strIdentifier);
			CSoundBuffer* GetBuffer(std::string strIdentifier);

		private:
			// Playing
			LPDIRECTSOUND8			m_lpds;
			DSCAPS					m_DSCaps;
			RecorderError			m_lastError;
			
			bool					m_bIsPlaying;
			bool					m_bMicMuted;
			bool					m_bVolumeMuted;

			// can be used to play multiple tracks at once
			// <identifier, CSoundBuffer>
			std::map<std::string, CSoundBuffer> m_soundBuffers; 

			// Catpuring
			LPDIRECTSOUNDCAPTURE8	m_captureDevice;
			LPDIRECTSOUNDCAPTUREBUFFER8 m_pCaptureBuffer;
			WAVEFORMATEX			m_wfxCapture;
			std::vector<HANDLE>		m_hCaptureEvents;
			DWORD					m_nCaptureSegments;
			bool					m_bIsCapturing;

			HRESULT CreateCaptureBuffer(LPDIRECTSOUNDCAPTURE8 pCaptureDevice, LPDIRECTSOUNDCAPTUREBUFFER8* ppCaptureBuffer);
		};

	}
}

