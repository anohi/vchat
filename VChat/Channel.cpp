#include "Channel.h"


namespace Voip {
	namespace Gui {
		CChannel::CChannel()
		{
			m_offset = { 0,0 };
			m_position = { 0, 0 };
			m_dimensions = { 100, 100 };
			m_bMouseOver = false;
			m_bIsActive = false;
			m_bIsInteractive = true;
			m_textLayout = NULL;
			m_brush = NULL;
			m_opacity = 0;
			m_zoom = 0;
			m_minMaxOpacity = { 0.5f, 1.0f };
		}


		CChannel::~CChannel()
		{
			Fini();
		}

		bool CChannel::OnMouseMove(D2D1_POINT_2F & point)
		{
			if (m_bShouldRender && m_bIsInteractive && (m_bMouseOver = (
				point.x > (m_position.x + m_offset.x) &&
				point.y > (m_position.y + m_offset.y) &&
				point.x < (m_position.x + m_dimensions.x + m_offset.x) &&
				point.y < (m_position.y + m_dimensions.y + m_offset.y))))
			{
				Notify(m_gui, Notifications::GUIN_SETCURSOR_HAND);
				return true;
			}
			return false;
		}

		bool CChannel::HitTest(D2D1_POINT_2F & point)
		{
			if (OnMouseMove(point)) {
				Notify(m_gui, Notifications::GUIN_CHANNEL_ITEM_CLICKED);
				return true;
			}

			return false;
		}

		void CChannel::Update(float & time, float & timeDelta)
		{
		}

		void CChannel::Render()
		{
			if (m_bShouldRender) {
				CGuiHelpers::D2DBrush->SetColor(&Colors::GUIC_BUTTON_GREEN);
				CGuiHelpers::D2DRenderTarget->FillRectangle(m_rect, CGuiHelpers::D2DBrush);

				if (m_textLayout) {
					m_brush->SetColor(Colors::GUIC_TEXT_WHITE);
					CGuiHelpers::D2DRenderTarget->DrawTextLayout(D2D1::Point2F(m_rect.left, m_rect.top), m_textLayout, m_brush);
				}
			}
		}

		void CChannel::Move(float xDelta, float yDelta)
		{
		}

		void CChannel::Fini()
		{
			Helpers::SafeRelease(&m_textLayout);
			Helpers::SafeRelease(&m_brush);
		}

		void CChannel::SetSize(float x, float y)
		{
			m_dimensions = { x, y };
			m_rect = CGuiHelpers::RectToDips(m_position.x, m_position.y, m_dimensions.x, m_dimensions.y);
			SetChannelName(m_channelName);
		}

		void CChannel::Reset()
		{
		}

		void CChannel::Init(HWND hwnd, int id, std::wstring channelName)
		{
			m_gui = hwnd;
			m_id = id;
			m_channelName = channelName;

			SetBgColor(&Colors::GUIC_BUTTON_GREEN);
			SetHoverColor(&Colors::GUIC_BUTTON_GREEN_HOVER);
			CGuiHelpers::D2DRenderTarget->CreateSolidColorBrush(*m_bgColor, &m_brush);
		}

		void CChannel::SetChannelName(std::wstring text)
		{
			if (text.empty())
				return;

			if (m_textLayout) {
				m_textLayout->Release();
				m_textLayout = NULL;
			}

			// create new text layout
			m_channelName = text;
			CGuiHelpers::CreateTextLayout(text, m_dimensions.x, m_dimensions.y, &m_textLayout, &CGuiHelpers::DWTextFormatCenter);

		}

		std::string CChannel::GetChannelName()
		{
			return std::string(m_channelName.begin(), m_channelName.end());
		}

	}
}