#pragma once
#include <Windows.h>
#include <windowsx.h>
#include <Commctrl.h>
#include <tchar.h>
#include <string>
#include <vector>
#include <map>
#include <functional>

#include "GuiIDs.h"

namespace Voip {
	namespace Gui {

		class CGuiBaseControl
		{
		public:
			CGuiBaseControl();
			~CGuiBaseControl();

			void Init(std::string className);
			virtual void Fini() = 0;

			void RegisterControl();
			void UnRegisterControl();
			HWND CreateControl(HWND hwndParent);


			virtual LRESULT CALLBACK ControlProc(HWND, UINT, WPARAM, LPARAM) = 0;
			//virtual LRESULT OnPaint(WPARAM wParam, LPARAM lParam) = 0;
			//virtual LRESULT OnLButtonDown(WPARAM wParam, LPARAM lParam) = 0;
			//virtual LRESULT OnSetFont(WPARAM wParam, LPARAM lParam) = 0;

			enum ActionNotify {
				ON_PAINT,
				ON_LBUTTON_CLICK,
				ON_RBUTTON_CLICK,
				ON_LBUTTON_DOUBLE_CLICK,
				ON_MOUSEOVER,
				ON_MOUSEOUT
			};

			

			struct ControlData {
				HWND hwnd;
				BOOL bLButtonDown;
				BOOL bRButtonDown;
				POINT initialMouseDownPos;
				CGuiBaseControl *gui;
			};

		protected:
			HINSTANCE	m_hInstance;
			COLORREF	m_crForeground;
			COLORREF	m_crBackground;
			HFONT		m_hFont;
			HWND		m_hwnd;
			HWND		m_hwndParent;
			WNDCLASSEX  m_wc;
			std::wstring m_className;
			RECT		m_rect; // defines position and dimension of the control

			static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
			static ControlData * GetControlData(HWND hWnd);
			static void SetControlData(HWND hWnd, CGuiBaseControl *control);
			static void Notify(HWND hwnd, Constants::GUIN_NOTIFICATIONCODE code);

			std::map<ActionNotify, std::vector<std::function<void()>>> m_callbacks; // used to store callbacks to execute when actions are performed on the control
		};
	}
}