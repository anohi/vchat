#include "NetworkVoipChannel.h"

namespace Voip {
	namespace Network {

		CNetworkVoipChannel::CNetworkVoipChannel() : m_iPacketNumber(0), m_bBroadcastRunning(false)
		{
			StartBroadCast();
		}


		CNetworkVoipChannel::~CNetworkVoipChannel()
		{
		}

		VoipError CNetworkVoipChannel::AddUser(CNetworkVoipUser *user)
		{
			if (!HasUser(user)) {
				m_users.push_back(user);

				if (GetCurrentMembers() >= 2) {
					if (m_bBroadcastRunning == false) {
						StartBroadCast();
					}
				}
				return VoipError::OK;
			}
			return VoipError::ALREADY_ON_CHANNEL;
		}

		VoipError CNetworkVoipChannel::RemoveUser(CNetworkVoipUser *user)
		{
			if (HasUser(user)) {
				// remove user from operator list
				RemoveOperator(user);

				// clean up stream resources
				user->ClearVoipData();

				// remove user from user-list
				m_users.erase(std::find(m_users.begin(), m_users.end(), user));
				if (m_users.size() == 1) {
					m_bBroadcastRunning = false;
					m_users.front()->ClearVoipData(); // clean up stream resources for last user on the channel
				}

				return VoipError::OK;
			}
			return VoipError::USER_NOT_ON_CHANNEL;
		}

		VoipError CNetworkVoipChannel::SetOperator(CNetworkVoipUser *user)
		{
			// user on channel?
			if (!HasUser(user)) {
				return VoipError::USER_NOT_ON_CHANNEL;
			}
			// user already operator?
			if (!IsOperator(user)) {
				m_operators.push_back(user);
			}
			return VoipError::OK;
		}

		VoipError CNetworkVoipChannel::RemoveOperator(CNetworkVoipUser *user)
		{
			// user on channel?
			if (!HasUser(user)) {
				return VoipError::USER_NOT_ON_CHANNEL;
			}
			// user not operator?
			if (!IsOperator(user)) {
				return VoipError::USER_NOT_OPERATOR;
			}
			auto &toBeErased = std::find(m_operators.begin(), m_operators.end(), user);
			if (toBeErased != m_operators.end()) {
				m_operators.erase(toBeErased);
			}
			return VoipError::OK;
		}

		std::vector<CNetworkVoipUser*>* CNetworkVoipChannel::GetOperators()
		{
			return &m_operators;
		}

		std::vector<CNetworkVoipUser*>* CNetworkVoipChannel::GetUsers()
		{
			return &m_users;
		}

		bool CNetworkVoipChannel::HasUser(CNetworkVoipUser *user)
		{
			return (std::find(m_users.begin(), m_users.end(), user) != m_users.end());
		}

		bool CNetworkVoipChannel::IsOperator(CNetworkVoipUser *user)
		{
			return (std::find(m_operators.begin(), m_operators.end(), user) != m_operators.end());
		}

		int CNetworkVoipChannel::GetCurrentMembers()
		{
			return m_users.size();
		}

		int CNetworkVoipChannel::GetMaxMembers()
		{
			return m_iMaxMembers;
		}

		std::string CNetworkVoipChannel::GetChannelName()
		{
			return m_strChannelName;
		}

		void CNetworkVoipChannel::SetMaxMembers(int uiMaxMembers)
		{
			// can't set max members below current member count
			if (uiMaxMembers <= m_iMaxMembers) {
				return;
			}

			m_iMaxMembers = uiMaxMembers;
		}

		void CNetworkVoipChannel::SetChannelName(std::string strChannelName)
		{
			if (!strChannelName.empty()) {
				m_strChannelName = strChannelName;
			}
		}

		void CNetworkVoipChannel::FillBuffer(CNetworkVoipUser *user, char *data, int iSize)
		{
			if (HasUser(user)) {
				user->PushData(data, iSize);
			}
		}

		void CNetworkVoipChannel::StartBroadCast()
		{
			m_bBroadcastRunning = true;
			
			std::thread voicePacketHandler([=]() {
				const auto min_int16_t = (std::numeric_limits<int16_t>::min)();
				const auto max_int16_t = (std::numeric_limits<int16_t>::max)();
				const auto iSampleCount = 500;
				const auto iPacketSize = 1000;
				auto iNumberOfUserTalking = 0;
				int32_t mixedSample;
				vector<pair<CNetworkVoipUser*, short*>> streams;
				int16_t mixedPacket[iPacketSize];

				while (m_bBroadcastRunning) {
					// get necessary packets
					streams.clear();
					for (const auto &user : m_users) {
						int iSize = 0; 
						char* data;
						if (user->NextData(&data, &iSize)) {
							streams.push_back({ user, (short*)(data) });
						}
					}
					
					// only 1 person is talking, just redirect it to other users
					if ((iNumberOfUserTalking = streams.size()) == 1) {
						m_iPacketNumber++;
						for (const auto &user : m_users) {
							auto client = user->GetUDPClient();
							int method = VoipRequest::VOIP_DATA;
							client->ClearBuffers()
								->Collect(method, m_strChannelName, m_iPacketNumber)
								->Collect((char*)streams[0].second, iPacketSize)
								->Collect(iNumberOfUserTalking, streams[0].first->GetUserID())
								->SendCollection();
						}
					}

					// more than 1 person is talking, mix all the voice streams
					else if (iNumberOfUserTalking) {
						for (const auto &stream : streams) {
							auto copiedStreams = streams;
							copiedStreams.erase(std::find(copiedStreams.begin(), copiedStreams.end(), stream));

							// mix packets
							int usersOnChannel = copiedStreams.size();
							if (usersOnChannel) {
								m_iPacketNumber++;
								ZeroMemory(mixedPacket, iPacketSize);
								for (uint16_t sample = 0; sample < iSampleCount; sample++) {
									mixedSample = 0;
									for (size_t i = 0; i < usersOnChannel; i++) {
										mixedSample += static_cast<int32_t>(copiedStreams[i].second[sample]);

										if (max_int16_t < mixedSample) {
											mixedSample = max_int16_t;
										}
										else if (min_int16_t > mixedSample) {
											mixedSample = min_int16_t;
										}
									}
									mixedPacket[sample] = mixedSample;
								}

								// immediately send mixed voice packet
								auto client = stream.first->GetUDPClient();
								// collect data
								int method = VoipRequest::VOIP_DATA;
								client->ClearBuffers()
									->Collect(method, m_strChannelName, m_iPacketNumber)
									->Collect((char*)mixedPacket, iPacketSize)
									->Collect(usersOnChannel);
								// include the list of users who are talking
								for (const auto &copy : copiedStreams) {
									client->Collect(copy.first->GetUserID());
								}
								// send to user
								client->SendCollection();
							}
						}
					}

					// delete used voice packets
					for (auto &data : streams) {
						Helpers::SafeDelete(&data.second);
					}

					// 16 times per second or 62.5ms interval
					std::this_thread::sleep_for(std::chrono::microseconds(31250));
				}

				cout << "stop broadcasting" << endl;
			});
			voicePacketHandler.detach();
		}

		void CNetworkVoipChannel::StopBroadCast()
		{
			m_bBroadcastRunning = false;
			// clear everything
			m_iPacketNumber = 0;
			for (const auto &user : m_users) {
				user->ClearVoipData();
			}
		}

		void CNetworkVoipChannel::Close()
		{
		}

		void CNetworkVoipChannel::Notify(int iType, CNetworkVoipUser *user)
		{
			int currentMembers = GetCurrentMembers();
			switch (iType) {
			case JOIN_CHANNEL:
			case LEAVE_CHANNEL:
				if (currentMembers) {
					CNetworkUDPClient *client;
					for (const auto &currentUser : m_users) {
						if (currentUser->GetUserID() != user->GetUserID()) {
							client = currentUser->GetUDPClient();
							client->ClearBuffers()
								->Collect(iType, user->GetUserID(), currentMembers)
								->SendCollection();
						}
					}
				}
				break;
			case VOIP_DATA:
				// notify others who is talking
				break;
			default:
				break;
			}
		}

		bool CNetworkVoipChannel::Init(int iVoicePacketCount)
		{
			return false;
		}

		void CNetworkVoipChannel::ResetVoicePacketBuffer()
		{

		}
	}
}