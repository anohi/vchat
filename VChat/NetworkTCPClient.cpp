#include "NetworkTCPClient.h"

namespace Voip {
	namespace Network {
		CNetworkTCPClient::CNetworkTCPClient()
		{
		}


		CNetworkTCPClient::~CNetworkTCPClient()
		{
		}

		void CNetworkTCPClient::Init(char * acAddress, int iPort)
		{
			WORD version = MAKEWORD(2, 2);

			if (WSAStartup(version, &this->m_wsadata) == SOCKET_ERROR) {
				this->HandleError("CNetworkTCPClient::Init() -> WSAStartup()");
			}

			ZeroMemory(&m_AddrinfoHints, sizeof(m_AddrinfoHints));
			m_AddrinfoHints = { 0, AF_INET, SOCK_STREAM, IPPROTO_TCP };

			if (getaddrinfo(acAddress, std::to_string(iPort).c_str(), &m_AddrinfoHints, &m_pAddrinfoResult) == SOCKET_ERROR) {
				this->HandleError("CNetworkTCPClient::Init() -> getaddrinfo()");
			}

			// try creating socket addresses returned by getaddrinfo
			for (m_pAddrinfoPtr = this->m_pAddrinfoResult; m_pAddrinfoPtr != NULL; m_pAddrinfoPtr = m_pAddrinfoPtr->ai_next) {
				m_socket = socket(m_pAddrinfoPtr->ai_family, m_pAddrinfoPtr->ai_socktype, m_pAddrinfoPtr->ai_protocol);
				if (m_socket == INVALID_SOCKET) {
					continue;
				}

				break; // connection established
			}
			if (m_socket == INVALID_SOCKET) {
				this->HandleError("CNetworkTCPClient::Connect() -> socket()");
			}
		}

		void CNetworkTCPClient::Connect()
		{
			// Connect to remote address
			if (connect(m_socket, m_pAddrinfoPtr->ai_addr, (int)m_pAddrinfoPtr->ai_addrlen) == SOCKET_ERROR) {
				this->HandleError("CNetworkTCPClient::Connect() -> connect()");
				closesocket(m_socket);
			}
		}

	}
}