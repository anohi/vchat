#include "User.h"


namespace Voip {
	namespace Gui {
		CUser::CUser()
		{
			m_offset = { 0,0 };
			m_position = { 0, 0 };
			m_dimensions = { 49, 49 };
			m_bMouseOver = false;
			m_bIsActive = false;
			m_bIsInteractive = true;
			m_textLayout = NULL;
			m_brush = NULL;
			m_opacity = 0;
			m_zoom = 0;
			m_minMaxOpacity = { 0.3f, 1.0f };
			m_bIsEmpty = true;

			m_state = STATE::NORMAL;
			m_bgColor = &Colors::GUIC_TEXT_BOX_BACKGROUND;
			m_hoverColor = &Colors::GUIC_BUTTON_GREEN;
			m_timer.Reset();

		}


		CUser::~CUser()
		{
			Fini();
		}

		void CUser::Update(float &time, float &timeDelta) {
			if (m_bMouseOver) {
				if (m_bIsActive) {
					m_opacity = m_minMaxOpacity.y;
				}
				else {
					m_opacity += 4* timeDelta;
					if (m_opacity > m_minMaxOpacity.y) {
						m_opacity = m_minMaxOpacity.y;
					}
				}
			}
			else {
				if (m_bIsActive) {
					m_opacity = m_minMaxOpacity.y;
				}
				else {
					m_opacity -= 0.5f * timeDelta;
					if (m_opacity < m_minMaxOpacity.x) {
						m_opacity = m_minMaxOpacity.x;
					}
				}
			}

			m_brush->SetOpacity(m_opacity);

			if (m_state == TALKING) {
				m_timer.Tick();
				if (m_timer.GetTimeTotal() > .4f) {
					m_timer.Reset();
					m_state = NORMAL;
				}
			}
		}

		void CUser::Render()
		{
			if (m_bShouldRender) {
				m_brush->SetColor(m_bgColor);
				CGuiHelpers::D2DRenderTarget->FillRoundedRectangle(D2D1::RoundedRect(m_rect, 3, 3), m_brush);

				switch (m_state)
				{
				case Voip::Gui::CUser::TALKING:
					m_brush->SetColor(&Colors::GUIC_BUTTON_GREEN_HOVER);
					break;
				case Voip::Gui::CUser::AFK:
					m_brush->SetColor(&Colors::GUIC_ICON_YELLOW);
					break;
				case Voip::Gui::CUser::DND:
					m_brush->SetColor(&Colors::GUIC_STATUS_RED);
					break;
				case Voip::Gui::CUser::NORMAL:
					m_brush->SetColor(m_bgColor);
					break;
				case Voip::Gui::CUser::INVISIBLE:
					break;
				default:
					break;
				}

				CGuiHelpers::D2DRenderTarget->DrawRoundedRectangle(D2D1::RoundedRect(m_rect, 3, 3), m_brush);

				if (m_textLayout) {
					m_brush->SetColor(Colors::GUIC_TEXT_WHITE);
					CGuiHelpers::D2DRenderTarget->DrawTextLayout(D2D1::Point2F(m_rect.left, m_rect.top), m_textLayout, m_brush);
				}

			}
		}

		void CUser::SetEmpty()
		{
			m_bIsEmpty = true;
			m_bgColor = &Colors::GUIC_TEXT_BOX_BACKGROUND;
			m_strUserId.clear();
			SetText(L"");
		}

		void CUser::SetState(STATE state)
		{
			m_state = state;
		}

		std::string CUser::GetUserId()
		{
			return m_strUserId;
		}

		void CUser::SetUserId(std::string userId)
		{
			m_strUserId = userId;
		}

		CUser::STATE CUser::GetState()
		{
			return m_state;
		}
	}
}