#include "GuiListServer.h"

namespace Voip {
	namespace Gui {
		CGuiListServer::CGuiListServer(HWND hWndParent)
		{
			m_hwndParent = hWndParent;
			//m_crForeground = Colors::GUIC_TEXT_WHITE;
			//m_crBackground = Colors::GUIC_SERVER_BACKGROUND;
			//m_crUnselected = Colors::GUIC_BUTTON_SERVER_DEFAULT;
			//m_crSelected = Colors::GUIC_BUTTON_SERVER_HOVER;
			m_hFont = reinterpret_cast<HFONT>(GetStockObject(DEFAULT_GUI_FONT));

			// set position and dimensions. 
			m_rect = { 0, 0, 80, 200 }; // default: {left: 0, top : 0, right : 80, bottom : 250}
		}

		CGuiListServer::~CGuiListServer()
		{
		}

		LRESULT CGuiListServer::ControlProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			ControlData* cdata = GetControlData(hWnd);

			switch (msg) {
			case WM_NCDESTROY:		Fini();
				break;
			case WM_PAINT:			return OnPaint(wParam, lParam);
			case WM_ERASEBKGND:		return 1; // returns non-zero value to prevents flickering (that is we have to erase/paint the background in WM_PAINT)
			case WM_LBUTTONDBLCLK:	return OnLButtonClick(wParam, lParam);
			case WM_SETFONT:		return OnSetFont(wParam, lParam);
			case WM_LBUTTONDOWN: {
				if (cdata) {
					if (!cdata->bLButtonDown) {
						cdata->initialMouseDownPos = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
					}
					cdata->bLButtonDown = TRUE;
					SetCapture(hWnd);
				}
				break;
			}
			case WM_RBUTTONDOWN: {
				if (cdata) {
					if (!cdata->bRButtonDown) {
						cdata->initialMouseDownPos = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
					}
					cdata->bRButtonDown = TRUE;
					SetCapture(hWnd);
				}
				break;
			}
			case WM_LBUTTONUP: return OnLButtonUp(wParam, lParam);
			case WM_RBUTTONUP: return OnRButtonUp(wParam, lParam);
			case NM_RELEASEDCAPTURE:
				break;
			case WM_MOUSEMOVE:
				break;
			case WM_MOUSEHOVER:
				break;
			case WM_MOUSELEAVE:
				break;
			default:
				break;
			}

			return DefWindowProc(hWnd, msg, wParam, lParam);
		}
		LRESULT CGuiListServer::OnPaint(WPARAM wParam, LPARAM lParam)
		{
			HDC hdc;
			PAINTSTRUCT ps;
			HANDLE hOldFont;
			TCHAR szText[_MAX_DIR];
			RECT rect;
			SIZE sz;

			// if windows has already provided us with the hdc in the wParam, just us it
			// this results in faster drawing
			hdc = (wParam == 0) ? BeginPaint(m_hwnd, &ps) : (HDC)wParam;

			GetClientRect(m_hwnd, &rect);
			HBRUSH background = CreateSolidBrush(m_crBackground);
			HBRUSH selected = CreateSolidBrush(m_crSelected);
			HBRUSH unselected = CreateSolidBrush(m_crUnselected);

			int width = 50, height = 50;
			int margin = 10;
			int initialX = rect.left + (rect.right - rect.left - width) / 2;
			int initialY = initialX;

			// draw background
			FillRect(hdc, &rect, background);

			// draw first item
			rect = { initialX, initialY, initialX + width, initialY + height };
			FillRect(hdc, &rect, selected);
			
			LPCWSTR message = L"A";
			SetTextColor(hdc, m_crForeground);
			SetBkMode(hdc, TRANSPARENT);
			HFONT hFont = CreateFont(24, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
				CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, VARIABLE_PITCH, TEXT("Arial"));
			SelectObject(hdc, hFont);

			// draw all the other items
			for (int item = 0; item < 3; item++) {
				rect.top += margin+height;
				rect.bottom = rect.top + height;
				FillRect(hdc, &rect, unselected);

				
				DrawText(hdc, message, -1, &rect, DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
			}
			
			// draw text
			
			DeleteObject(selected);
			DeleteObject(unselected);
			DeleteObject(background);
			DeleteObject(hFont);
			if (wParam == 0) {
				EndPaint(m_hwnd, &ps);
			}

			return 1;
		}

		LRESULT CGuiListServer::OnLButtonClick(WPARAM wParam, LPARAM lParam)
		{
			return 0;
		}

		LRESULT CGuiListServer::OnSetFont(WPARAM wParam, LPARAM lParam)
		{
			return 0;
		}

		void CGuiListServer::Fini()
		{
			// remove extra control data
			ControlData *cdata = GetControlData(m_hwnd);
			if (cdata) {
				delete cdata;
			}
			UnRegisterControl();
		}
		LRESULT CGuiListServer::OnLButtonDown(WPARAM wParam, LPARAM lParam)
		{
			SetCapture(m_hwnd);
			return 0;
		}
		LRESULT CGuiListServer::OnLButtonUp(WPARAM wParam, LPARAM lParam)
		{
			ControlData* cdata = GetControlData(m_hwnd);

			POINT pt = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) }; // macro to get the position of the mouse when released
			RECT rc;
			GetClientRect(m_hwnd, &rc);
			if (PtInRect(&rc, pt)) {
				//SendMessage(GetParent(m_hwnd), WM_COMMAND, GetWindowLong(m_hwnd, GWL_ID), 0);

				if (cdata && cdata->bLButtonDown == TRUE) {
					// clicked or drag&drop on control
					// check whether clicked or drag&drop
					POINT diff = { abs(pt.x - cdata->initialMouseDownPos.x), abs(pt.y - cdata->initialMouseDownPos.y) };
					// clicked on control
					if (diff.x + diff.y <= 2) {
						MessageBox(m_hwnd, _T("hey, you clicked on the control"), _T("test"), MB_OK);
						Notify(m_hwnd, Constants::GUIN_LBCLICKED); // tell parent that this control is clicked on
					}
					// drag&drop on control
					else {
						MessageBox(m_hwnd, _T("hey, you dragged on the control"), _T("test"), MB_OK);
						Notify(m_hwnd, Constants::GUIN_DRAG_AND_DROP);
					}
				}
				else {
					// drag in
					MessageBox(m_hwnd, _T("hey, you clicked somewhere else and released on the control"), _T("test"), MB_OK);
					Notify(m_hwnd, Constants::GUIN_DROPPED_ON);
				}
			}
			else {
				if (cdata && cdata->bLButtonDown == TRUE) {
					// drag out
					MessageBox(m_hwnd, _T("hey, you clicked on the control and released outside of it"), _T("test"), MB_OK);
					Notify(m_hwnd, Constants::GUIN_DRAGGED_OUT);
				}
			}

			cdata->bLButtonDown = FALSE;
			ReleaseCapture();

			return 0;
		}

		LRESULT CGuiListServer::OnRButtonDown(WPARAM wParam, LPARAM lParam)
		{
			return 0;
		}

		LRESULT CGuiListServer::OnRButtonUp(WPARAM wParam, LPARAM lParam)
		{
			return 0;
		}
	}
}