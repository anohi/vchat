#pragma once
#include "GuiHelpers.h"
#include "GuiElement.h"

namespace Voip {
	namespace Gui {
		class CInputfield : public CGuiElement
		{
		protected:
			std::wstring m_text;
			std::wstring m_helpText;
			D2D_RECT_F	 m_caret;
			bool		 m_bRequired;
			bool		 m_bIsPassword;

			const D2D1_COLOR_F* m_caretColor;
			IDWriteTextLayout* m_layoutText;
			IDWriteTextLayout* m_layoutHelpText;
			ID2D1SolidColorBrush* m_brushLine;

			void CalculateCaretPosition();
			
		public:
			CInputfield();
			~CInputfield();

			// Geerbt �ber CGuiElement
			virtual bool OnMouseMove(D2D1_POINT_2F & point) override;
			virtual bool HitTest(D2D1_POINT_2F & point) override;
			virtual void Update(float & time, float & timeDelta) override;
			virtual void Render() override;
			virtual void Move(float xDelta, float yDelta) override;
			virtual void Fini() override;
			virtual void SetSize(float x, float y) override;
			virtual void Reset() override;
			virtual void Init(HWND hwnd, int id, std::wstring help = L"type here", bool bRequired = false, bool bIsPassword = false, D2D1_POINT_2F dimensions = { Dimensions::WINDOW_WIDTH / 2-10, Dimensions::INPUTFIELD_HEIGHT });
			virtual void ExitInputMode();
			virtual void UpdateTextLayout();

			virtual void HandleInput(WPARAM param);
			virtual void SetText(std::string text);
			virtual void SetHint(std::wstring hint);
			virtual void SetCaretColor(const D2D1_COLOR_F *color);
			virtual std::string GetText();
		};
	}
}