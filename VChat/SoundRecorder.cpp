#include "SoundRecorder.h"


namespace Voip {
	namespace Sound {
		CSoundRecorder::CSoundRecorder()
			: m_lastError(NOT_INITIALIZED), m_bIsPlaying(false), m_bIsCapturing(false), m_bMicMuted(false), m_bVolumeMuted(false)
		{
		}

		CSoundRecorder::~CSoundRecorder()
		{
		}

		bool CSoundRecorder::Init(HWND hwnd)
		{
			// Creates direct sound device object using default device
			if (FAILED(DirectSoundCreate8(NULL, &m_lpds, NULL))) {
				// failed to create ds device object
				// TODO: error handling here
#ifdef DEBUG 
				perror("Can't create device object");
#endif
				m_lastError = CSoundRecorder::CANT_CREATE_DEVICE_OBJECT;
				return false;
			}

			/* setting the cooperative levels
			* https://msdn.microsoft.com/de-de/library/windows/desktop/ee416970(v=vs.85).aspx [26.02.2016]
			*
			* by setting the cooperative level for an DirectSound application you can decide what access it has on the device
			* this is to ensure that each application has the right access on the device at the right time, and won't interfere each other
			* IDirectSound8::SetCooperativeLevel must be called after creating a device object in order for sound to be played / heard
			*
			* DSSCL_NORMAL
			* https://msdn.microsoft.com/de-de/library/windows/desktop/ee418683(v=vs.85).aspx [26.02.2016]
			* no write access to primary buffer, fixed format: 22kHz, 8-bit samples, 2 channels -> smooth switch between applications
			*
			* DSSCL_PRIORITY
			* https://msdn.microsoft.com/de-de/library/windows/desktop/ee418753(v=vs.85).aspx [26.02.2016]
			* like the name suggest, application has the priority to access hardware resources: hardware mixing, can set format of primary sound buffer
			* for games: IP telephony (third party) alongside game audio possible
			*
			* DSSCL_WRITEPRIMARY https://msdn.microsoft.com/de-de/library/windows/desktop/ee419049(v=vs.85).aspx [26.02.2016]
			*
			*/
			if (FAILED(m_lpds->SetCooperativeLevel(hwnd, DSSCL_PRIORITY))) {
#ifdef DEBUG 
				perror("Can't set cooperative level");
#endif
				m_lastError = CSoundRecorder::CANT_SET_COOPERATIVE_LEVEL;
				return false;
			}

			// Gets device capabilities
			m_DSCaps.dwSize = sizeof(DSCAPS);
			if (FAILED(m_lpds->GetCaps(&m_DSCaps))) {
#ifdef DEBUG 
				perror("Can't get device capabilities");
#endif
				m_lastError = CSoundRecorder::CANT_GET_DEVICE_CAPABILITIES;
				return false;
			}

			m_lastError = CSoundRecorder::NONE;
			return true;
		}

		bool CSoundRecorder::InitMicrophone(HWND hwnd)
		{
			if (FAILED(DirectSoundCaptureCreate8(NULL, &m_captureDevice, NULL))) {
#ifdef DEBUG
				perror("Can't create capture device object");
#endif
				m_lastError = RecorderError::CANT_CREATE_DEVICE_OBJECT;
				return false;
			}

			// Creates capture buffer
			if (FAILED(CreateCaptureBuffer(m_captureDevice, &m_pCaptureBuffer))) {
#ifdef DEBUG
				perror("Can't create capture buffer");
#endif
				m_lastError = RecorderError::CANT_CREATE_CAPTURE_BUFFER;
				return false;
			}

			return true;
		}

		CSoundRecorder::RecorderError CSoundRecorder::LastError()
		{
			return m_lastError;
		}

		void CSoundRecorder::StartCapture()
		{
			if (!m_bIsCapturing) {
				m_pCaptureBuffer->Start(DSCBSTART_LOOPING);
				m_bIsCapturing = true;
			}
		}

		void CSoundRecorder::StopCapture()
		{
			if (m_bIsCapturing) {
				m_pCaptureBuffer->Stop();
				m_bIsCapturing = false;
			}
		}

		bool CSoundRecorder::ToggleMuteMic()
		{
			
			return m_bMicMuted;
		}


		bool CSoundRecorder::SetVolumeMin()
		{
			for (auto &buffer : m_soundBuffers) {
				return (m_bVolumeMuted = buffer.second.SetVolumeMin());
			}
		}


		bool CSoundRecorder::SetVolumeMax()
		{
			for (auto &buffer : m_soundBuffers) {
				return (m_bVolumeMuted = !buffer.second.SetVolumeMax());
			}
		}

		bool CSoundRecorder::MuteMic(bool state)
		{
			return false;
		}

		bool CSoundRecorder::IsMicMuted()
		{
			return m_bMicMuted;
		}

		bool CSoundRecorder::IsSpeakerMuted()
		{
			return m_bVolumeMuted;
		}


		HRESULT CSoundRecorder::SetNotifications(DWORD capture)
		{
			HRESULT				 hr;
			LPDIRECTSOUNDNOTIFY8 pDSNotify;
			std::vector<DSBPOSITIONNOTIFY> captureNotifications;

			if (NULL == this->m_pCaptureBuffer) {
				return E_INVALIDARG;
			}
			if (FAILED(hr = this->m_pCaptureBuffer->QueryInterface(IID_IDirectSoundNotify, (LPVOID*)&pDSNotify))) {
				return hr;
			}

			// Create capture events.
			for (DWORD i = 0; i < capture; ++i) {
				HANDLE event = CreateEvent(NULL, FALSE, FALSE, NULL);
				if (event == NULL) {
					// delete all previously created events
					for each (auto event in m_hCaptureEvents) {
						CloseHandle(event);
					}
					m_hCaptureEvents.clear();
					return GetLastError();
				}
				m_hCaptureEvents.push_back(event);
			}

			// Describe capture notifications. 
			DWORD iSegmentSizeCapture = m_wfxCapture.nAvgBytesPerSec / capture;
			for (DWORD iPosition = 0; iPosition < capture; iPosition++) {
				captureNotifications.push_back({ ((iPosition + 1) * iSegmentSizeCapture) - 1, m_hCaptureEvents.at(iPosition) });
			}

			// Create capture notifications.
			this->m_nCaptureSegments = capture;
			hr = pDSNotify->SetNotificationPositions(captureNotifications.size(), &captureNotifications[0]);
			pDSNotify->Release();

			return hr;
		}

		const std::vector<HANDLE>* CSoundRecorder::GetNotifications()
		{
			return &m_hCaptureEvents;
		}

		DWORD CSoundRecorder::GetSegmentLength()
		{
			return m_hCaptureEvents.size() ? (DWORD)(m_wfxCapture.nAvgBytesPerSec / m_hCaptureEvents.size()) : m_wfxCapture.nAvgBytesPerSec;
		}

		int CSoundRecorder::ReadCapturedData(DWORD uiSegment, short *pcBuffer) {
			LPVOID  ppvAudioPtr1;
			DWORD   pdwAudioBytes1;
			LPVOID  ppvAudioPtr2;
			DWORD   pdwAudioBytes2;
			HRESULT hr;

			DWORD nBytes = this->m_wfxCapture.nAvgBytesPerSec / this->m_nCaptureSegments;

			hr = m_pCaptureBuffer->Lock(uiSegment * nBytes, nBytes, &ppvAudioPtr1, &pdwAudioBytes1, &ppvAudioPtr2, &pdwAudioBytes2, 0);
			if (SUCCEEDED(hr)) {
				CopyMemory(pcBuffer, ppvAudioPtr1, pdwAudioBytes1);
				if (ppvAudioPtr2 != NULL) {
					CopyMemory(pcBuffer + pdwAudioBytes1, ppvAudioPtr2, pdwAudioBytes2); // if data to be read wraps around at the end to the beginning
				}
			}

			hr = m_pCaptureBuffer->Unlock(ppvAudioPtr1, pdwAudioBytes1, ppvAudioPtr2, pdwAudioBytes2);
			if (SUCCEEDED(hr)) {
				return pdwAudioBytes1 + pdwAudioBytes2;
			}

			m_lastError = RecorderError::CANT_READ_CAPTURED_DATA;
			return 0;
		}


		BOOL CSoundRecorder::CreateSoundBuffer(std::string strIdentifier, WAVEFORMATEX wfxFormat)
		{
			if (m_soundBuffers.find(strIdentifier) == m_soundBuffers.end()) {
				m_soundBuffers[strIdentifier] = {};
				m_soundBuffers[strIdentifier].CreateSoundBuffer(m_lpds, wfxFormat);
				m_soundBuffers[strIdentifier].SetNotifications(16);
				return m_soundBuffers[strIdentifier].IsInit();
			}
			return FALSE; // buffer already exists or failed creating
		}

		BOOL CSoundRecorder::FillSoundBuffer(std::string strIdentifier, short * psSoundData, DWORD dwSize)
		{
			if (m_soundBuffers.find(strIdentifier) != m_soundBuffers.end()) {
				return m_soundBuffers[strIdentifier].WriteData(psSoundData, dwSize);
			}
			return FALSE;
		}

		BOOL CSoundRecorder::FillSilence(std::string strIdentifier, DWORD dwSegment, DWORD dwSize)
		{
			if (m_soundBuffers.find(strIdentifier) != m_soundBuffers.end()) {
				return m_soundBuffers[strIdentifier].FillSilence(dwSegment, dwSize);
			}
			return FALSE;
		}

		BOOL CSoundRecorder::PlayBuffer(std::string strIdentifier, DWORD dwFlags)
		{
			if (m_soundBuffers.find(strIdentifier) != m_soundBuffers.end()) {
				return m_soundBuffers[strIdentifier].PlayBuffer(dwFlags);
			}
			return FALSE;
		}

		BOOL CSoundRecorder::StopBuffer(std::string strIdentifier)
		{
			if (m_soundBuffers.find(strIdentifier) != m_soundBuffers.end()) {
				return m_soundBuffers[strIdentifier].StopBuffer();
			}
			return FALSE;
		}

		BOOL CSoundRecorder::PlayAllSound(DWORD dwFlags)
		{
			for (auto buffer = m_soundBuffers.begin(); buffer != m_soundBuffers.end(); ++buffer) {
				if (buffer->second.PlayBuffer(dwFlags) == FALSE) {
					return FALSE;
				}
			}

			return TRUE;
		}

		BOOL CSoundRecorder::StopAllSound()
		{
			for (auto buffer = m_soundBuffers.begin(); buffer != m_soundBuffers.end(); ++buffer) {
				if (buffer->second.StopBuffer() == FALSE) {
					return FALSE;
				}
			}
			
			return TRUE;
		}

		bool CSoundRecorder::BufferExists(std::string strIdentifier)
		{
			return (m_soundBuffers.find(strIdentifier) != m_soundBuffers.end());
		}

		CSoundBuffer * CSoundRecorder::GetBuffer(std::string strIdentifier)
		{
			if (m_soundBuffers.find(strIdentifier) != m_soundBuffers.end()) {
				return &m_soundBuffers[strIdentifier];
			}
			return nullptr;
		}

		void CSoundRecorder::DeleteSoundBuffer(std::string strIdentifier)
		{
			if (m_soundBuffers.find(strIdentifier) != m_soundBuffers.end()) {
				m_soundBuffers[strIdentifier].Cleanup();
				m_soundBuffers.erase(strIdentifier);
			}
		}

		HRESULT CSoundRecorder::CreateCaptureBuffer(LPDIRECTSOUNDCAPTURE8 pCaptureDevice, LPDIRECTSOUNDCAPTUREBUFFER8 * ppCaptureBuffer)
		{
			HRESULT hr;
			DSCBUFFERDESC               bufferDescription;
			LPDIRECTSOUNDCAPTUREBUFFER  tempSoundCaptureBuffer;
			ZeroMemory(&m_wfxCapture, sizeof(WAVEFORMATEX));
			m_wfxCapture = PCM_8000_MONO;

			if ((NULL == pCaptureDevice) || (NULL == ppCaptureBuffer)) return E_INVALIDARG;
			bufferDescription.dwSize = sizeof(DSCBUFFERDESC);
			bufferDescription.dwFlags = 0;
			bufferDescription.dwBufferBytes = m_wfxCapture.nAvgBytesPerSec;
			bufferDescription.dwReserved = 0;
			bufferDescription.lpwfxFormat = &m_wfxCapture;
			bufferDescription.dwFXCount = 0;
			bufferDescription.lpDSCFXDesc = NULL;

			if (SUCCEEDED(hr = pCaptureDevice->CreateCaptureBuffer(&bufferDescription, &tempSoundCaptureBuffer, NULL)))
			{
				hr = tempSoundCaptureBuffer->QueryInterface(IID_IDirectSoundCaptureBuffer8, (LPVOID*)ppCaptureBuffer);
				tempSoundCaptureBuffer->Release();
			}
			return hr;
		}
	}
}