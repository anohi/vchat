#include "SoundProcessing.h"


namespace Voip {
	namespace Sound {
		CSoundProcessing::CSoundProcessing()
		{
		}


		CSoundProcessing::~CSoundProcessing()
		{
		}

		uint8_t * CSoundProcessing::Encode(CODEC codec, int16_t * source, uint8_t * dest, uint32_t length)
		{
			for (uint32_t i = 0; i < length; ++i) {
				switch (codec)
				{
				case CSoundProcessing::ALAW:
					break;
				case CSoundProcessing::MLAW:
					break;
				default:
					break;
				}
			}

			return dest;
		}

		int16_t * CSoundProcessing::Decode(CODEC codec, uint8_t * sourse, int16_t * dest, uint32_t length)
		{
			for (uint32_t i = 0; i < length; ++i) {

			}

			return dest;
		}

		void CSoundProcessing::Analyse(uint8_t * samples, uint32_t numSamples)
		{
			uint32_t diffValues;
			map<int32_t, vector<uint32_t>> sampleGroups;

			if (numSamples) {
				for (uint32_t i = 0; i < numSamples; ++i) {
					if (sampleGroups.find(samples[i]) == sampleGroups.end()) {
						sampleGroups[samples[i]] = { i };
					}
					else {
						sampleGroups[samples[i]].push_back(i);
					}
				}

				int bytes = 0;
				for each (auto &sample in sampleGroups)
				{
					bytes++;
					cout << sample.first << " { ";
					for each (auto index in sample.second)
					{
						cout << index << ", ";
						bytes++;
					}
					cout << "}\n";
				}

				cout << "original: " << numSamples << "Bytes\n";
				cout << "possible compression: " << bytes << " Bytes\n";
			}
		}

		void CSoundProcessing::printBits(int8_t value) {
			for (int i = 8 - 1; i >= 0; i--) {
				std::cout << ((value >> i) & 1);
				if (i % 4 == 0 && i != 0) std::cout << ".";
			}
			std::cout << std::endl;
		}

		long double CSoundProcessing::Energy(int16_t * souce, uint32_t length)
		{
			long double energy = 0;
			for (uint32_t i = 0; i < length; i++) {
				energy += (abs(souce[i]) * abs(souce[i]));
			}
			energy /= length;
			energy = sqrtl(energy);

			return energy;
		}

		int8_t * CSoundProcessing::Compress(int16_t * source, uint32_t sourceLen, uint32_t &destLen)
		{
			vector<int8_t> audio;
			int16_t threshold = 100;
			uint8_t fragments = 5;
			uint16_t window = sourceLen / fragments;

			for (uint32_t i = 0; i < sourceLen; i+= window) {
				long double energy = Energy(&source[i], window);
				if (energy > threshold) {
					audio.push_back(i/window);
				}
			}

			uint8_t countAudio = audio.size();
			uint32_t bufferSize = 4 + countAudio*(1 + 2*window);
			// [count fragments:8bit][count audio:8bit][window:16bit][pos a0:8bit][data a0]...[pos ax:8bit][data ax]
			if (countAudio == fragments) {
				destLen = sourceLen * 2;
				return (int8_t*)source;
			}
			if (countAudio) {
				int8_t *buffer = new int8_t[bufferSize];
				uint16_t position = 0;
				ZeroMemory(buffer, bufferSize);
				CopyMemory(buffer, &fragments, 1); position++;
				CopyMemory(buffer, &countAudio, 1);
				CopyMemory(&buffer[++position], &window, 2); position += 2;
				for (uint32_t i = 0; i < countAudio; ++i) {
					CopyMemory(&buffer[position++], &audio[i], 1);
					CopyMemory(&buffer[position], &source[audio[i] * window], window*2);
					position += (window*2);
				}

				destLen = bufferSize;
				return buffer;
			}

			return 0;
		}

		int16_t * CSoundProcessing::Decompress(int8_t * source, uint32_t &destLen)
		{
			if (!source) {
				return 0;
			}
			uint8_t fragments=0, countAudio=0, fragment=0;
			uint16_t window = 0, position = 0;
			CopyMemory(&fragments, &source[position++], 1);
			CopyMemory(&countAudio, &source[position++], 1);
			CopyMemory(&window, &source[position], 2); position += 2;

			if (fragments == countAudio) {
				destLen = 0;
				return (int16_t*)source;
			}
			uint8_t *buffer = new uint8_t[fragments*window*2];
			for (uint8_t i = 0; i < countAudio; ++i) {
				CopyMemory(&fragment, &source[position++], 1);
				CopyMemory(&buffer[fragment*window*2], &source[position], window*2);
				position += (window * 2);
			}
			destLen = window*fragments * 2;
			return (int16_t*)buffer;
		}


		void CSoundProcessing::fft(CArray& x)
		{
			const size_t N = x.size();
			if (N <= 1) return;

			// divide
			CArray even = x[slice(0, N / 2, 2)];
			CArray odd = x[slice(1, N / 2, 2)];

			// conquer
			fft(even);
			fft(odd);

			// combine
			for (size_t k = 0; k < N / 2; ++k)
			{
				Complex t = polar(1.0, -2 * 3.141592653589793238460 * k / N) * odd[k];
				x[k] = even[k] + t;
				x[k + N / 2] = even[k] - t;
			}
		}

		// inverse fft (in-place)
		void CSoundProcessing::ifft(CArray& x)
		{
			// conjugate the complex numbers
			x = x.apply(std::conj);

			// forward fft
			fft(x);

			// conjugate the complex numbers again
			x = x.apply(std::conj);

			// scale the numbers
			x /= x.size();
		}

		int16_t *CSoundProcessing::Mix(int16_t *sample1, int16_t *sample2, uint16_t sampleCount)
		{
			int16_t *mixed = new int16_t[sampleCount];

			for (uint16_t sample = 0; sample < sampleCount; sample++) {
				const int32_t result(static_cast<int32_t>(sample1[sample]) + static_cast<int32_t>(sample2[sample]));
				if ((std::numeric_limits<int16_t>::max)() < result)
					mixed[sample] = (std::numeric_limits<int16_t>::max)();
				else if ((std::numeric_limits<int16_t>::min)() > result)
					mixed[sample] = (std::numeric_limits<int16_t>::min)();
				else
					mixed[sample] = result;
			}

			return mixed;
		}

		int8_t *CSoundProcessing::Mix(vector<char*> *channels, uint16_t sampleCount)
		{
			int8_t *mixed = new int8_t[sampleCount];
			int32_t mixedSample;
			size_t channelCount = channels->size();

			for (uint16_t sample = 0; sample < sampleCount; sample++) {
				mixedSample = 0;
				for (size_t i = 0; i < channelCount - 1; i++) {
					mixedSample = static_cast<int32_t>((*channels)[i][sample]) + static_cast<int32_t>((*channels)[i + 1][sample]);
				}

				if ((std::numeric_limits<int8_t>::max)() < mixedSample)
					mixed[sample] = (std::numeric_limits<int8_t>::max)();
				else if ((std::numeric_limits<int8_t>::min)() > mixedSample)
					mixed[sample] = (std::numeric_limits<int8_t>::min)();
				else
					mixed[sample] = mixedSample;
				
			}

			return mixed;
		}
	}
}
