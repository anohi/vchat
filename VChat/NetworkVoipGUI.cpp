#include "NetworkVoipGUI.h"


namespace Voip {
	namespace Gui {

		// ######### BEGIN Implementation of CNetworkVoipGUIServerItem ######################################################################

		// test
		CNetworkVoipGUIServerItem::CNetworkVoipGUIServerItem()
			: m_handle(0), m_parentHandle(0), m_imageIndex(0), m_selectedImageIndex(0), m_itemType(ItemType::SERVER)
		{
		}

		CNetworkVoipGUIServerItem::CNetworkVoipGUIServerItem(std::string servername, std::string extra, HTREEITEM handle, ItemType itemType, HTREEITEM parentHandle, int imageIndex, int selectedImageIndex)
		{
			Init(servername, extra, handle, itemType, parentHandle, imageIndex, selectedImageIndex);
		}

		CNetworkVoipGUIServerItem::~CNetworkVoipGUIServerItem()
		{
		}

		std::string CNetworkVoipGUIServerItem::GetName()
		{
			return m_serverName;
		}

		HTREEITEM CNetworkVoipGUIServerItem::self()
		{
			return m_handle;
		}

		HTREEITEM CNetworkVoipGUIServerItem::parent()
		{
			return m_parentHandle;
		}

		HTREEITEM CNetworkVoipGUIServerItem::FindChild(std::string childName)
		{
			for (auto child : m_children) {
				if (child.GetName() == childName) {
					return child.self();
				}
			}
			return NULL;
		}

		void CNetworkVoipGUIServerItem::Init(std::string serverName, std::string extra, HTREEITEM self, ItemType itemType, HTREEITEM parent, int imageIndex, int selectedImageIndex)
		{
			m_serverName = serverName;
			m_extra = extra;
			m_handle = self;
			m_parentHandle = parent;
			m_imageIndex = imageIndex;
			m_selectedImageIndex = selectedImageIndex;
			m_itemType = itemType;
		}

		void CNetworkVoipGUIServerItem::AddChild(CNetworkVoipGUIServerItem child)
		{
			m_children.push_back(child);
		}

		void CNetworkVoipGUIServerItem::RemoveChild(std::string childName)
		{
			for (int i = 0; i < m_children.size(); i++) {
				if (m_children[i].GetName() == childName) {
					m_children[i].RemoveAllChildren();
					m_children.erase(m_children.begin() + i);
					break;
				}
			}
		}

		void CNetworkVoipGUIServerItem::RemoveAllChildren()
		{
			if (m_children.size()) {
				for (auto child : m_children) {
					child.RemoveAllChildren();
				}
				m_children.clear();
			}
		}

		const std::vector<CNetworkVoipGUIServerItem>& CNetworkVoipGUIServerItem::GetChildren()
		{
			return m_children;
		}

		void CNetworkVoipGUIServerItem::Fini()
		{
			RemoveAllChildren();
		}

		CNetworkVoipGUIServerItem::ItemType CNetworkVoipGUIServerItem::GetType()
		{
			return m_itemType;
		}

		// ######### END ##########################################################################################################





		// ######### BEGIN Implementation of CNetworkVoipGUI ######################################################################

		CNetworkVoipGUI::CNetworkVoipGUI()
		{

		}

		// creates a new dialog based gui
		// if parent = NULL, gui is modeless
		// callback specifies the event handler which the gui calls. If none is passed the default CNetworkVoipGUI::GuiProcess() is called
		// hInstance defines the application context this dialog is in
		CNetworkVoipGUI::CNetworkVoipGUI(HINSTANCE hInstance, Network::CNetworkVoipClient* voipClient, DLGPROC callback, HWND parent)
			: m_inputDialog(nullptr)
		{
			m_parent = parent;
			m_bGuiVisible = false;
			m_hInstance = hInstance;
			m_voipClient = voipClient;

			

			// Create GUI
			m_gui = CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_VOIP), parent, callback);

			if (m_gui == NULL) {
				DWORD error = GetLastError();
				TCHAR szLE[16];

				MessageBox(NULL, (LPCWSTR)std::to_string(error).c_str(), TEXT("Error"), MB_OK);
			}
			else {
				// saves the pointer to this class for later usage
				SetWindowLongPtr(m_gui, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
				m_inputDialog = new CGuiInputDialog(hInstance, m_gui, callback);
				// set controls table
				m_controls.insert({ 
					{ IDC_TREE_SERVERLIST, GetDlgItem(m_gui, IDC_TREE_SERVERLIST) }, 
					{ IDC_LIST_MESSAGES, GetDlgItem(m_gui, IDC_LIST_MESSAGES) },
					{ IDC_EDIT_CHATBOX, GetDlgItem(m_gui, IDC_EDIT_CHATBOX) },
					{ IDC_BUTTON_ADDSERVER, GetDlgItem(m_gui, IDC_BUTTON_ADDSERVER) },
					{ IDC_BUTTON_REMOVESERVER, GetDlgItem(m_gui, IDC_BUTTON_REMOVESERVER) },
					{ IDC_BUTTON_CONNECTSERVER, GetDlgItem(m_gui, IDC_BUTTON_CONNECTSERVER) },
					{}
				});
			}

		}


		CNetworkVoipGUI::~CNetworkVoipGUI()
		{
		}

		void CNetworkVoipGUI::Init()
		{
			// create an ImageList control
			int IMAGE_WIDTH = 32, IMAGE_HEIGHT = 32, GROW_SIZE = 32;
			int IMAGE_COUNT = 2;

			m_imageList = ::ImageList_Create(IMAGE_WIDTH, IMAGE_HEIGHT, ILC_COLORDDB | ILC_MASK, IMAGE_COUNT, GROW_SIZE);

			// load icons and add them to ImageList
			HICON icon;
			icon = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDI_ICON_SERVER2));
			//icon = reinterpret_cast<HICON>(LoadImage(NULL, MAKEINTRESOURCE(IDI_ICON1), IMAGE_ICON, IMAGE_WIDTH, IMAGE_HEIGHT, LR_SHARED));
			ImageList_AddIcon(m_imageList, icon);
			icon = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDI_ICON_CHATROOM));
			ImageList_AddIcon(m_imageList, icon);

			// check if all icons are in the ImageList
			if (ImageList_GetImageCount(m_imageList) == IMAGE_COUNT) {
				HWND serverlist = m_controls[IDC_TREE_SERVERLIST];
				SendMessage(serverlist, TVM_SETIMAGELIST, (WPARAM)TVSIL_NORMAL, (LPARAM)m_imageList);
			}
		}

		void CNetworkVoipGUI::Fini()
		{
			DestroyWindow(m_gui);
			if (m_inputDialog) {
				m_inputDialog->Fini();
				delete m_inputDialog;
			}
		}

		// helper functions
		std::vector<std::string> CNetworkVoipGUI::explode(const std::string& str, const char& delimeter) {
			int first = str.find(delimeter);
			return{ str.substr(0, first), str.substr(first + 1) };
		}

		HTREEITEM CNetworkVoipGUI::insertItem(const std::string str, HTREEITEM parent = TVI_ROOT, HTREEITEM insertAfter = TVI_LAST, int imageIndex = 0, int selectedImageIndex = 0)
		{
			std::wstring server;
			for (int i = 0; i < str.length(); ++i) {
				server += wchar_t(str[i]);
			}

			TVINSERTSTRUCT insertStruct;
			insertStruct.hParent = parent;
			insertStruct.hInsertAfter = insertAfter;
			insertStruct.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
			insertStruct.item.pszText = (LPWSTR)server.c_str();
			insertStruct.item.cchTextMax = sizeof(server) / sizeof(server[0]);
			insertStruct.item.iImage = imageIndex;
			insertStruct.item.iSelectedImage = selectedImageIndex;

			return reinterpret_cast<HTREEITEM>(SendMessage(m_controls[IDC_TREE_SERVERLIST], TVM_INSERTITEM, 0, reinterpret_cast<LPARAM>(&insertStruct)));
		}

		bool CNetworkVoipGUI::DrawControl(DRAWITEMSTRUCT * control)
		{
			switch (control->CtlID)
			{
			case IDC_BUTTON_REMOVESERVER: {
				HGDIOBJ OldObj;
				char text[500];
				HBRUSH aBrush;
				RECT rt;

				// Damit man sp�ter nicht immer "dis->rcItem" schreiben muss: 
				rt = control->rcItem;
				// https://www.c-plusplus.net/forum/39375-full owner draw (14.10.2016)

				RECT rectText, rectFrame, rectSquare;
				if (control->CtlID == IDC_BUTTON_REMOVESERVER) {
					if (control->itemAction == ODA_DRAWENTIRE) {
						// Hintergrund zeichnen: 
						aBrush = GetSysColorBrush(COLOR_BTNFACE);
						OldObj = SelectObject(control->hDC, aBrush);
						PatBlt(control->hDC, 1, 1, (rt.right - 2), (rt.bottom - 2), PATCOPY);
						DeleteObject(SelectObject(control->hDC, OldObj));

						// Text holen: 
						//GetWindowText(control->hwndItem, text, 500);

						// Text zeichnen: 
						// Standard-Schriftart holen: 
						OldObj = SelectObject(control->hDC, GetStockObject(DEFAULT_GUI_FONT));
						// Hintergrund des Textes auf Transparent schalten: 
						SetBkMode(control->hDC, TRANSPARENT);
						// Textfarbe setzen: 
						SetTextColor(control->hDC, RGB(0, 0, 0));
						// Und nun endlich zeichnen: 
						rt.top = rt.top + 2;    // Nur, damits besser passt: 
						DrawText(control->hDC, wstring(L"test").c_str(), 4, &rt, DT_CENTER);  // Caption zeichnen 
						rt.top = rt.top - 2;    // Damits wieder stimmt! 
												// Schriftart wieder freigeben und altes Objekt wieder selektieren: 
						DeleteObject(SelectObject(control->hDC, OldObj));
					} if (control->itemAction == ODA_FOCUS) {
						aBrush = GetSysColorBrush(COLOR_BTNHILIGHT);
						OldObj = SelectObject(control->hDC, aBrush);
						RoundRect(control->hDC, rt.left, rt.top, rt.right, rt.bottom, 5, 5);
						DeleteObject(SelectObject(control->hDC, OldObj));
					}
					
					
					//if (control->itemAction && (ODA_SELECT | ODA_DRAWENTIRE)) {
					//	if (control->itemState && ODS_SELECTED) {
					//		aBrush = GetSysColorBrush(COLOR_BTNHILIGHT);
					//		OldObj = SelectObject(control->hDC, aBrush);
					//		RoundRect(control->hDC, rt.left, rt.top, rt.right, rt.bottom, 5, 5);
					//		DeleteObject(SelectObject(control->hDC, OldObj));
					//	}
					//	else {
					//		aBrush = GetSysColorBrush(COLOR_BTNSHADOW);
					//		OldObj = SelectObject(control->hDC, aBrush);
					//		PatBlt(control->hDC, (rt.right - 1), 0, (rt.right - 1), (rt.bottom - 1), PATCOPY);
					//		PatBlt(control->hDC, 0, (rt.bottom - 1), rt.right, 1, PATCOPY);
					//		DeleteObject(SelectObject(control->hDC, OldObj));
					//	}
					//}
				}
			}
			break;
			default:
				return false;
			}

			return true;
		}

		bool CNetworkVoipGUI::AddServer(std::string serverName, std::string serverAddr)
		{
			if (m_servers.find(serverName) == m_servers.end()) {
				// add server entry to the TreeView
				HTREEITEM serverHandle = insertItem(serverName);
				if (serverHandle) {
					m_servers[serverName] = CNetworkVoipGUIServerItem(serverName, serverAddr, serverHandle);
					Log(LogType::INFO, "New server added: <" + serverName + ", " + serverAddr + ">");
					return true;
				}
			}
			Log(LogType::UNKNOWN_ERROR, "Can't add server: <" + serverName + ", " + serverAddr + ">");
			return false;
		}

		bool CNetworkVoipGUI::RemoveServerByName(std::string serverName)
		{
			if (m_servers.find(serverName) != m_servers.end()) {
				m_servers.erase(serverName);
			}
			return 0;
		}

		bool CNetworkVoipGUI::RemoveServerByIndex(int index)
		{
			return 0;
		}

		void CNetworkVoipGUI::UpdateChannelList(std::string serverName)
		{
			
		}

		void CNetworkVoipGUI::Log(LogType logType, std::string message)
		{
			std::wstring widestr;

			switch (logType)
			{
			case Voip::Gui::CNetworkVoipGUI::INFO:
				widestr = L"(I) ";
				break;
			case Voip::Gui::CNetworkVoipGUI::WARNING:
				widestr = L"(W) ";
				break;
			case Voip::Gui::CNetworkVoipGUI::UNKNOWN_ERROR:
				widestr = L"(E) ";
				break;
			case Voip::Gui::CNetworkVoipGUI::TEXT_MESSAGE:
				break;
			default:
				break;
			}

			// inserting log message
			widestr += std::wstring(message.begin(), message.end());
			LRESULT lResult = SendMessage(m_controls[IDC_LIST_MESSAGES], LB_INSERTSTRING, -1, (LPARAM)widestr.c_str());

			// auto scroll by selecting last inserted item
			lResult = SendMessage(m_controls[IDC_LIST_MESSAGES], LB_GETCOUNT, 0, 0);
			SendMessage(m_controls[IDC_LIST_MESSAGES], LB_SETCURSEL, lResult-1, 0);
		}

		LRESULT CALLBACK CNetworkVoipGUI::DlgProcess(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
		{
			switch (Message) {
			case WM_DRAWITEM:
			{
				return this->DrawControl((LPDRAWITEMSTRUCT)lParam);
				break;
			}
			case WM_NOTIFY: {
				LPNMHDR lpnmh = (LPNMHDR)lParam;
				if (lpnmh->idFrom == IDC_TREE_SERVERLIST) {
					switch (lpnmh->code) {
						case NM_DBLCLK: { // if double left-click is performed on a server on the list
							HTREEITEM item = TreeView_GetNextItem(lpnmh->hwndFrom, 0, TVGN_CARET);
							if (item) {
								// http://stackoverflow.com/questions/17774633/how-can-i-find-treeview-node-by-which-label-equals-string-variable (14.10.2016)
								WCHAR buffer[_MAX_DIR+1];
								TVITEMW tvi = { 0 };
								tvi.hItem = item;
								tvi.mask = TVIF_TEXT;
								tvi.cchTextMax = _MAX_DIR;
								tvi.pszText = &buffer[0];
								if (TreeView_GetItem(lpnmh->hwndFrom, &tvi)) {
									wstring temp = wstring(tvi.pszText);
									string itemID = string(temp.begin(), temp.end());
									// check if it's a server
									if (m_servers.find(itemID) != m_servers.end()) {
										// server item clicked, get channel list
										vector<string> channels = m_voipClient->GetChannelList();

										// get all children and remove them from treeview
										const std::vector<CNetworkVoipGUIServerItem>& oldChannels =  m_servers[itemID].GetChildren();
										for (auto channel : oldChannels) {
											TreeView_DeleteItem(lpnmh->hwndFrom, channel.self());
										}

										// remove all children in the structure
										m_servers[itemID].RemoveAllChildren();
										
										for (auto channel : channels) {
											// create channel items and update tree structure
											HTREEITEM newChannel = insertItem(channel, item, TVI_LAST, 1, 1);
											if (newChannel) {
												m_servers[itemID].AddChild(CNetworkVoipGUIServerItem(channel, "", newChannel, CNetworkVoipGUIServerItem::ItemType::CHANNEL, item, 1, 1));
											}
										}

										// keep node expanded
										TreeView_Expand(lpnmh->hwndFrom, item, TVE_COLLAPSE);
										this->Log(LogType::INFO, "selected server: <" + itemID + ">");
									}
									else {
										// a channel is selected, join it
										m_voipClient->JoinChannel(itemID);
										this->Log(LogType::INFO, "joined channel: <" + itemID + ">");
									}

								}
							}
							break;
						}
						default:
							break;
					}
				}
				break;
			}

			case WM_CLOSE:
			case WM_QUIT:
				this->Hide();
				break;

			case WM_COMMAND:
				switch (LOWORD(wParam)) { // handles controls of chat window 
					case IDC_BUTTON_ADDSERVER: {
						// display new dialog to enter message
						//this->AddServer(std::to_string(rand()), "192.168.178.140:50024");
						if (m_inputDialog) {
							string serverInfo;
							function<void(std::string)> getText = [=](std::string text) {
								this->AddServer("Vektoria Test Server", "127.0.0.1:50024");
							};
							m_inputDialog->SetInfo(getText, "Server settings", "Format: <servername, serverAddress>")->Show();
							EnableWindow(m_gui, false);
						}
						break;
					}
					case IDC_BUTTON_REMOVESERVER:
						break;
						
					case IDC_BUTTON_CONNECTSERVER: {
						vector<string> server_data = explode("192.168.178.22:50024", ':');
						Voip::Network::VoipError result = m_voipClient->JoinServer("bobby", server_data[0], stoi(server_data[1]), "password");
						if (result == Voip::Network::VoipError::OK) {
							this->Log(LogType::INFO, "connected to <" + server_data[0] + ", " + server_data[1] + "> successfully.");

							// get channel list
						}
						else {
							// display some error on the status bar of gui
							this->Log(LogType::UNKNOWN_ERROR, "can't connect to <" + server_data[0] + ", " + server_data[1] + ">: " + std::to_string(result));
						}
						break;
					}

					case IDC_EDIT_CHATBOX:
						break;

					case IDC_TREE_SERVERLIST:
						break;
				}
				break;

			default:
				return FALSE;	// no messages handled
			}
			return TRUE;
		}
		// ######### END ##############################################################################################



		// ######### BEGIN Implementation GUI Dialog classes ##########################################################

		CGuiInputDialog::CGuiInputDialog(HINSTANCE hInstance, HWND parent, DLGPROC callback)
		{
			m_hInstance = hInstance;
			m_parent = parent;
			m_bGuiVisible = false;

			
			m_gui = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_INPUT), parent, callback);

			if (m_gui == NULL) {
				DWORD error = GetLastError();
				TCHAR szLE[16];

				MessageBox(NULL, (LPCWSTR)std::to_string(error).c_str(), TEXT("Error"), MB_OK);
			}
			else {
				// saves the pointer to this class for later usage
				SetWindowLongPtr(m_gui, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));

				// saves the window rect
				GetWindowRect(m_gui, &m_rect);

				m_list = new CGuiListServer(m_gui);
				if (m_list) {
					m_list->Init("Server List");
				}
			}
		}

		void CGuiInputDialog::Init()
		{
		}

		void CGuiInputDialog::Fini()
		{
			DestroyWindow(m_gui);
		}

		CGuiInputDialog* CGuiInputDialog::SetInfo(function<void(std::string)>& callback, std::string title = "", std::string info = "")
		{
			m_title = title;
			m_info = info;
			m_input = "";
			m_callback = callback;

			SetWindowText(GetDlgItem(m_gui, IDC_LABEL_INFO), wstring(info.begin(), info.end()).c_str());
			SetWindowText(m_gui, wstring(title.begin(), title.end()).c_str());

			return this;
		}

		std::string CGuiInputDialog::GetText()
		{
			return m_bHasData? m_input : "";
		}

		LRESULT CGuiInputDialog::DlgProcess(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			switch (msg)
			{

			case WM_INITDIALOG:
				SetFocus(GetDlgItem(hWnd, IDC_EDIT_INPUTBOX));
				break;
			case WM_CLOSE:
			case WM_QUIT: {
				EnableWindow(m_parent, true);
				this->Hide();
			}
				break;
				
			case WM_COMMAND:
				switch (LOWORD(wParam)) {
					case IDOK: {
						// retrieve text of the Edit-Control
						TCHAR buffer[_MAX_DIR];
						GetWindowText(GetDlgItem(hWnd, IDC_EDIT_INPUTBOX), buffer, _MAX_DIR);
						wstring temp(&buffer[0]);
						this->m_input = string(temp.begin(), temp.end());
						this->m_bHasData = true;
						// if a callback is set, execute it
						if (this->m_callback) {
							this->m_callback(this->m_input);
						}

						EnableWindow(m_parent, true);
						this->Hide();
						break;
					}
					case IDCANCEL:
						EnableWindow(m_parent, true);
						this->m_bHasData = false;
						this->Hide();
						break;
				}
				break;
			default:
				return FALSE;
			}
			return TRUE;
		}
	}
}