#pragma once
#include "GuiHelpers.h"

namespace Voip {
	namespace Gui {
		class CGuiElement
		{
		public:
			virtual bool OnMouseMove(D2D1_POINT_2F &point) = 0;
			virtual bool HitTest(D2D1_POINT_2F &point) = 0;
			virtual void Update(float &time, float &timeDelta) = 0;
			virtual void Render() = 0;
			virtual void Move(float xDelta, float yDelta) = 0;
			virtual void SetPosition(float x, float y);
			virtual void SetPosition(D2D1_POINT_2F &point);
			virtual void Fini() = 0;
			virtual void SetSize(float x, float y) = 0;
			virtual void Reset() = 0;

			CGuiElement* SetVisibility(bool visible);
			CGuiElement* SetBgColor(const D2D1_COLOR_F *color);
			CGuiElement* SetHoverColor(const D2D1_COLOR_F *color);
			CGuiElement* SetId(int id);
			CGuiElement* SetInteractivity(bool isInteractive);
			CGuiElement* SetActiveState(bool isActive);
			CGuiElement* SetOffset(float x, float y);
			CGuiElement* AlignLeft(CGuiElement* anchor);
			CGuiElement* AlignRight(CGuiElement* anchor);
			CGuiElement* AlignBottom(CGuiElement* anchor);
			CGuiElement* AlignTop(CGuiElement* anchor);
			CGuiElement* AlignCenter(CGuiElement* anchor, bool horizontal = true, bool vertical = false);
			CGuiElement* SetPositionLeftTo(CGuiElement* anchor, float padding = Dimensions::PADDING_BIG);
			CGuiElement* SetPositionRightTo(CGuiElement* anchor, float padding = Dimensions::PADDING_BIG);
			CGuiElement* SetPositionAbove(CGuiElement* anchor, float padding = Dimensions::PADDING_BIG);
			CGuiElement* SetPositionUnder(CGuiElement* anchor, float padding = Dimensions::PADDING_BIG);


			D2D_POINT_2F GetPosition();
			D2D_POINT_2F GetDimensions();
			const D2D1_COLOR_F *GetBgColor();
			const D2D1_COLOR_F *GetHoverColor();

			bool ShouldRender();
			bool IsInteractive();
			bool IsActive();
			int GetId();
			void SetOpacityMinMax(float min, float max);
			void SetOpacity(float opacity);

			void Notify(HWND hwnd, Notifications code);
		protected:
			HWND			m_gui;
			int				m_id;
			std::wstring	m_description;
			bool			m_bShouldRender;
			bool			m_bMouseOver;
			bool			m_bIsInteractive;
			bool			m_bIsActive;
			float			m_opacity;
			float			m_zoom;
			D2D_POINT_2F	m_position;
			D2D_POINT_2F	m_dimensions;
			D2D_POINT_2F	m_offset;
			D2D_POINT_2F	m_minMaxOpacity;
			D2D_RECT_F		m_rect;
			D2D_MATRIX_3X2_F m_transformation;
			const D2D1_COLOR_F *m_bgColor;
			const D2D1_COLOR_F *m_hoverColor;

			ID2D1SolidColorBrush* m_brush;
		};
	}
}