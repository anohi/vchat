#pragma once
#include "NetworkBase.h"
#include <string>

namespace Voip {
	namespace Network {
		class CNetworkTCPClient : Voip::Network::CNetworkBase
		{
		private:
			addrinfo   *m_pAddrinfoResult, *m_pAddrinfoPtr, m_AddrinfoHints;

		public:
			CNetworkTCPClient();
			~CNetworkTCPClient();

			// Geerbt �ber CNetworkBase
			virtual void Init(char * acAddress, int iPort = DEFAULT_PORT) override;
			//virtual void SendI(int & i) override;
			//virtual void SendB(bool & b) override;
			//virtual void SendS(char * ac) override;
			//virtual void RecvS(char * ac) override;
			//virtual bool RecvB() override;
			//virtual int RecvI() override;
			//virtual void Collect(bool & b) override;
			//virtual void Collect(int & i) override;
			//virtual void Collect(char * ac) override;
			//virtual void SendCollection() override;
			//virtual void RecvCollection() override;
			//virtual void ExtractS(char * ac) override;
			//virtual bool ExtractB() override;
			//virtual int ExtractI() override;
			//virtual void SendHandshake() override;
			//virtual void RecvHandshake() override;
			//virtual void RecvHandshakes(int i) override;
			//virtual bool IsCollectionEmpty() override;

			void Connect();
		};
	}
}