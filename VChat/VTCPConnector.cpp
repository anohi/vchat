#include "VTCPConnector.h"

VTCPStream * VTCPConnector::Connect(const char * hostname, int port)
{
	VTCPStream *stream = new VTCPStream(hostname, port);
	stream->m_AddrinfoHints = { 0, AF_UNSPEC, SOCK_STREAM, IPPROTO_TCP };

	if (stream == NULL) {
		return nullptr;
	}

	if (!stream->Init()) {
		free(stream);
		return nullptr;
	}

	if (!stream->Connect()) {
		free(stream);
		return nullptr;
	}

	return stream;
}