#include "VALawCodec.h"



VALawCodec::VALawCodec()
{
	man_shift = { 1, 1, 2, 3, 4, 5, 6, 7 };
	seg_mask = { 0x1F, 0x3F, 0x7F, 0xFF, 0x1FF, 0x3FF, 0x7FF, 0xFFF };
	trail_mask = { 1, 1, 2, 4, 8, 16, 32, 64 };
}


VALawCodec::~VALawCodec()
{
}

void VALawCodec::encode(short * psSourceAudio, char * pucDestAudio, unsigned int uiLength)
{
	unsigned char sign, segment, mantissa;
	for (unsigned int i = 0; i < uiLength; i++) {
		//psDestAudio[i] = decodeSample(pucSourceAudio[i]);
		sign = (psSourceAudio[i] & 0x8000) >> 8;

		if (psSourceAudio[i] < 0) {
			psSourceAudio[i] = ~(~(-psSourceAudio[i]) + 1); // 2s complement and invert all bits
		}

		segment = getSegment(psSourceAudio[i]);
		mantissa = psSourceAudio[i] >> man_shift[segment];
		segment <<= 4;

		// put eveything together
		// the standard suggest XORing the value prior to transmission
		// ensuring lots of 0/1 transitions for best clock recovery on the receiving side
		// so in our case it'd be 0x55 = 0101 0101 (2)
		pucDestAudio[i] = (sign | segment | mantissa) ^ 0x55;
	}
}

void VALawCodec::decode(char * pucSourceAudio, short * psDestAudio, unsigned int uiLength)
{
	short temp, segment, mantissa, sign;
	for (size_t i = 0; i < uiLength; i++) {
		temp = pucSourceAudio[i] ^ 0x55; //
		sign = temp >> 7;
		temp &= (0x7F); // the sign bit is removed
		segment = temp >> 4;
		mantissa = temp & 0x0F; // only 4 LSBs are considered
		if (segment > 0) {
			mantissa |= 0x10;
		}
		mantissa <<= man_shift[segment];
		mantissa |= trail_mask[segment];

		psDestAudio[i] = sign? -mantissa : mantissa;
	}
}

void VALawCodec::encode2(short * psSourceAudio, unsigned char * pucDestAudio, uint32_t uiLength)
{

	for (uint32_t i = 0; i < uiLength; i++) {
		const uint16_t ALAW_MAX = 0xFFF;
		uint16_t mask = 0x800;
		uint8_t sign = 0;
		uint8_t position = 11;
		uint8_t lsb = 0;
		if (psSourceAudio[i] < 0)
		{
			psSourceAudio[i] = -psSourceAudio[i];
			sign = 0x80;
		}
		if (psSourceAudio[i] > ALAW_MAX)
		{
			psSourceAudio[i] = ALAW_MAX;
		}
		for (; ((psSourceAudio[i] & mask) != mask && position >= 5); mask >>= 1, position--);
		lsb = (psSourceAudio[i] >> ((position == 4) ? (1) : (position - 4))) & 0x0f;
		pucDestAudio[i] = (sign | ((position - 4) << 4) | lsb) ^ 0x55;
	}

}

void VALawCodec::decode2(unsigned char * pucSourceAudio, short * psDestAudio, uint32_t uiLength)
{
	for (uint32_t i = 0; i < uiLength; i++) {
		uint8_t sign = 0x00;
		uint8_t position = 0;
		psDestAudio[i] = 0;

		pucSourceAudio[i] ^= 0x55;
		if (pucSourceAudio[i] & 0x80)
		{
			pucSourceAudio[i] &= ~(1 << 7);
			sign = -1;
		}
		position = ((pucSourceAudio[i] & 0xF0) >> 4) + 4;
		if (position != 4)
		{
			psDestAudio[i] = ((1 << position) | ((pucSourceAudio[i] & 0x0F) << (position - 4)) | (1 << (position - 5)));
		}
		else
		{
			psDestAudio[i] = (pucSourceAudio[i] << 1) | 1;
		}
		if (sign != 0) {
			psDestAudio[i] = -psDestAudio[i];
		}
	}
}


const char VALawCodec::getSegment(short value) {
	for (unsigned char i = 0; i < 8; i++) {
		if (value <= seg_mask[i]) {
			return i;
		}
	}
	return seg_mask[7];
}