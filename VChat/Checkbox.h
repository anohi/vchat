#pragma once
#include "GuiHelpers.h"
#include "ButtonRect.h"


namespace Voip {
	namespace Gui {
		class CCheckbox : public CButtonRect
		{
		protected:
			bool m_bChecked;
			ID2D1Bitmap* m_pBitmapChecked;
			ID2D1Bitmap* m_pBitmapUnchecked;
			D2D1_POINT_2F m_dimensionsText;

		public:
			CCheckbox(bool bChecked = false);
			~CCheckbox();

			// Geerbt �ber CGuiElement
			virtual void Update(float & time, float & timeDelta) override;
			virtual void Render() override;
			virtual void Fini() override;
			virtual void Init(
				HWND hwnd,
				int id,
				std::wstring text = L"",
				bool bChecked = false,
				D2D1_POINT_2F position = { 0, 0 },
				D2D1_POINT_2F dimensions = { Dimensions::CHECKBOX, Dimensions::CHECKBOX },
				D2D1_POINT_2F dimensions_text = { Dimensions::WINDOW_WIDTH/2, Dimensions::CHECKBOX }
				);

			virtual bool OnMouseMove(D2D1_POINT_2F &point);
			virtual bool HitTest(D2D1_POINT_2F &point);
			virtual bool IsChecked();
			virtual void Check();
			virtual void Uncheck();
			virtual void ToggleCheck();

			virtual void SetText(std::wstring text);
			virtual void SetSize(float x, float y);

		};

	}
}