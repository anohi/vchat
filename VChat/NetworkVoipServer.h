#pragma once
#include "NetworkUDPServer.h"
#include "NetworkUDPClient.h"
#include "NetworkVoipChannel.h"
#include "NetworkVoipUser.h"
#include "Timer.h"

#include <time.h>
#include <vector>
#include <thread>
#include <map>

namespace Voip {
	namespace Network {
		using namespace std;

		class CNetworkVoipServer : public Voip::Network::CNetworkUDPServer
		{
		public:
			CNetworkVoipServer();
			~CNetworkVoipServer();

			void Start(); // Start thread to handle requests
			void Stop();  // Stop thread that handles requests
			
		private:
			bool m_bServerStarted;
			Voip::Helpers::CTimer m_timer;
			std::string m_strServerPassword;
			map<std::string, CNetworkVoipChannel*> m_channels;
			map<std::string, CNetworkVoipUser*> m_users;

			void HandleRequest();

			CNetworkVoipUser* AcceptUser(std::string strUsername, std::string strID);
			bool RemoveUserFromServer(std::string strUsername, std::string strID);
			bool RemoveUserFromChannel(std::string strUsername, std::string strID, std::string strChannelName = "");
			bool AddUserToChannel(std::string strUsername, std::string strID, std::string strChannelName);
			bool CreateChannel(std::string strChannelName, std::string strCreator, std::string strID, int iMaxMembers = 6);
			void RemoveChannel(std::string strChannelName);
			void ChangeUsername(std::string strOldUsername, std::string strNewUsername);
			
			void RespondWithChannelList();

			CNetworkVoipUser* GetUser(std::string strUsername, std::string strUserID);
			CNetworkVoipChannel *GetChannel(std::string strChannelName);
			CNetworkVoipChannel *OnWhatChannelIsUser(CNetworkVoipUser* user);

			bool UserExists(std::string strUserName, std::string strID);
			bool ChannelExists(std::string strChannelName);
		};
	}
}