#include "Settings.h"

namespace Voip {
	namespace Gui {

		CSettings::CSettings()
		{
			m_file.clear();

			m_autoConnect = false;
			m_autoJoin = false;
			m_port = 50024;
			m_serverAddr.clear();
			m_userName.clear();
			m_channel.clear();
		}


		CSettings::~CSettings()
		{
			Fini();
		}

		void CSettings::Init(HWND hwnd, int id, bool bShowBackground)
		{
			CScenario::Init(hwnd, id, bShowBackground);
			CCheckbox* cbAutoConnect = new CCheckbox();
			CCheckbox* cbAutoJoin = new CCheckbox();
			CInputfield* inUsername = new CInputfield();
			CInputfield* inServerAddr = new CInputfield();
			CInputfield* inChannel = new CInputfield();
			CInputfield* inPort = new CInputfield();

			CLabel* labelUsername = new CLabel();
			CLabel* labelServer = new CLabel();
			CLabel* labelChannel = new CLabel();
			CLabel* labelPort = new CLabel();

			CButtonRect* btnDone = new CButtonRect();

			// initialize
			cbAutoConnect->Init(m_gui, IDS::Element::CB_SETTINGS_AUTOCONNECT, L"Connect on app-start", m_autoConnect);
			cbAutoJoin->Init(m_gui, IDS::Element::CB_SETTINGS_AUTOJOIN, L"Join channel on connection", m_autoJoin);
			inServerAddr->Init(m_gui, IDS::Element::IN_SETTINGS_SERVER_ADDR, L"127.0.0.1", true);
			inPort->Init(m_gui, IDS::Element::IN_SETTINGS_PORT, L"", true);
			inUsername->Init(m_gui, IDS::Element::IN_SETTINGS_USERNAME, L"enter username", true);
			inChannel->Init(m_gui, IDS::Element::IN_SETTINGS_CHANNEL, L"enter channel name");
			btnDone->Init(m_gui, IDS::Element::BTN_SETTINGS_DONE, L"Done");
			labelServer->Init(0, L"Server");
			labelUsername->Init(0, L"Username");
			labelChannel->Init(0, L"Channel");
			labelPort->Init(0, L"Port");

			// set dimensions
			inServerAddr->SetSize(Dimensions::WINDOW_WIDTH - 20, Dimensions::INPUTFIELD_HEIGHT);

			// set values
			inUsername->SetText(m_userName);
			inServerAddr->SetText(m_serverAddr);
			inPort->SetText(std::to_string(m_port));
			inChannel->SetText(m_channel);

			// add elements
			AddElement(cbAutoConnect);

			AddElementBelow(cbAutoConnect, labelServer, Dimensions::PADDING_BIG);
			AddElementBelow(labelServer, inServerAddr);

			AddElementBelow(inServerAddr, labelPort, Dimensions::PADDING_BIG);
			AddElementBelow(labelPort, inPort);

			AddElementRightTo(inPort, inUsername);
			AddElementAbove(inUsername, labelUsername);

			AddElementBelow(inPort, cbAutoJoin, 2 *Dimensions::PADDING_BIG);

			AddElementBelow(cbAutoJoin, labelChannel, Dimensions::PADDING_BIG);
			AddElementBelow(labelChannel, inChannel);

			AddElement(btnDone);

			btnDone->SetSize(Dimensions::BUTTON_WIDTH, Dimensions::BAR_HEIGHT);
			D2D1_POINT_2F pos = { 0, Dimensions::WINDOW_HEIGHT - (2 * Dimensions::BAR_HEIGHT + 20) - Dimensions::BAR_HEIGHT };
			btnDone->SetPosition(pos);
			btnDone->AlignRight(inUsername);

			SetOffset(Dimensions::PADDING_BIG, Dimensions::BAR_HEIGHT + Dimensions::PADDING_BIG);
		}

		bool CSettings::LoadSettings(std::string file)
		{
			if (file.empty())
				if (m_file.empty())
					file = "settings.ini";
				else
					file = m_file;

			//std::ifstream inputFile("C:\\Users\\TienThanh\\Source\\Repos\\vchat\\VChat\\settings.ini");
			std::ifstream inputFile(file);

			for (std::string line; getline(inputFile, line);) {
				if (line.length()) {
					auto pos = line.find('=');
					std::string var = line.substr(0, pos);
					std::string value = line.substr(pos + 1);
					if (value.empty() || var.empty())
						continue;

					if (var == "auto_join") {
						if (isdigit(value.at(0))) {
							bool state = std::stoi(value.substr(0, 1));
							SetAutoJoin(state);
							GetElement(IDS::IN_SETTINGS_CHANNEL)->SetInteractivity(state);
						}
						else {
							SetAutoJoin(false);
							GetElement(IDS::IN_SETTINGS_CHANNEL)->SetInteractivity(false);
						}
					}
					else if (var == "auto_connect") {
						if (isdigit(value.at(0))) {
							SetAutoConnect(std::stoi(value.substr(0, 1)));
						}
						else {
							SetAutoConnect(false);
						}
					}
					else if (var == "server") {
						SetServer(value);
					}
					else if (var == "port") {
						SetPort(std::stoi(value));
					}
					else if (var == "username") {
						SetUsername(value);
					}
					else if (var == "channel") {
						SetChannel(value);
					}
					else {
						inputFile.close();
						return false; // wrong settings file
					}
				}
			}

			inputFile.close();
			return true;
		}

		void CSettings::SaveSettings(std::string file)
		{
			if (file.empty())
				if (m_file.empty())
					file = "settings.ini";
				else
					file = m_file;

			m_autoConnect = dynamic_cast<CCheckbox*>(GetElement(IDS::Element::CB_SETTINGS_AUTOCONNECT))->IsChecked();
			m_autoJoin = dynamic_cast<CCheckbox*>(GetElement(IDS::Element::CB_SETTINGS_AUTOJOIN))->IsChecked();
			m_userName = dynamic_cast<CInputfield*>(GetElement(IDS::Element::IN_SETTINGS_USERNAME))->GetText();
			m_channel = dynamic_cast<CInputfield*>(GetElement(IDS::Element::IN_SETTINGS_CHANNEL))->GetText();
			m_serverAddr = dynamic_cast<CInputfield*>(GetElement(IDS::Element::IN_SETTINGS_SERVER_ADDR))->GetText();
			try {
				m_port = stoi(dynamic_cast<CInputfield*>(GetElement(IDS::Element::IN_SETTINGS_PORT))->GetText());
			}
			catch (exception e) {
				m_port = 50024;
			}

			std::ofstream settings(file);
			settings << "auto_connect=" << m_autoConnect << std::endl;
			settings << "auto_join=" << m_autoJoin << std::endl;
			settings << "server=" << m_serverAddr << std::endl;
			settings << "port=" << m_port << std::endl;
			settings << "username=" << m_userName << std::endl;
			settings << "channel=" << m_channel << std::endl;

			settings.close();
		}

		void CSettings::SetAutoConnect(bool state)
		{
			m_autoConnect = state;
			if (state)
				dynamic_cast<CCheckbox*>(GetElement(IDS::Element::CB_SETTINGS_AUTOCONNECT))->Check();
			else
				dynamic_cast<CCheckbox*>(GetElement(IDS::Element::CB_SETTINGS_AUTOCONNECT))->Uncheck();
		}

		void CSettings::SetAutoJoin(bool state)
		{
			m_autoJoin = state;
			if (state)
				dynamic_cast<CCheckbox*>(GetElement(IDS::Element::CB_SETTINGS_AUTOJOIN))->Check();
			else
				dynamic_cast<CCheckbox*>(GetElement(IDS::Element::CB_SETTINGS_AUTOJOIN))->Uncheck();
		}

		void CSettings::SetUsername(std::string name)
		{
			m_userName = name;
			dynamic_cast<CInputfield*>(GetElement(IDS::Element::IN_SETTINGS_USERNAME))->SetText(name);
		}

		void CSettings::SetChannel(std::string channel)
		{
			m_channel = channel;
			dynamic_cast<CInputfield*>(GetElement(IDS::Element::IN_SETTINGS_CHANNEL))->SetText(channel);
		}

		void CSettings::SetPort(int port)
		{
			m_port = port;
			dynamic_cast<CInputfield*>(GetElement(IDS::Element::IN_SETTINGS_PORT))->SetText(std::to_string(port));
		}

		void CSettings::SetServer(std::string addr)
		{
			m_serverAddr = addr;
			dynamic_cast<CInputfield*>(GetElement(IDS::Element::IN_SETTINGS_SERVER_ADDR))->SetText(addr);
		}

		std::string CSettings::GetChannel()
		{
			return m_channel;
		}

		std::string CSettings::GetServer()
		{
			return m_serverAddr;
		}

		std::string CSettings::GetUsername()
		{
			return m_userName;
		}

		int CSettings::GetPort()
		{
			return m_port;
		}

		bool CSettings::IsAutoConnect()
		{
			return m_autoConnect;
		}

		bool CSettings::IsAutoJoin()
		{
			return m_autoJoin;
		}

		CGuiElement * CSettings::HitTest(D2D1_POINT_2F & point)
		{
			if (auto element = CScenario::HitTest(point)) {
				if (element->GetId() == IDS::CB_SETTINGS_AUTOJOIN) {
					auto cb = dynamic_cast<CCheckbox*>(element);
					if (auto channel = GetElement(IDS::IN_SETTINGS_CHANNEL)) {
						channel->SetInteractivity(cb->IsChecked());
					}
				}
				return element;
			}
			return nullptr;
		}

	}
}