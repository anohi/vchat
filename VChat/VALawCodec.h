#pragma once
#include <iostream>
#include <array>
#include <inttypes.h>

using namespace std;

class VALawCodec
{
public:
	VALawCodec();
	~VALawCodec();

	void printBits(char *msg, short len, unsigned short value) {
		char encoded[16];

		cout << msg;
		for (int i = len -1; i >= 0; i--) {
			cout << ((value >> i) & 1);
			if (i % 4 == 0 && i != 0) cout << ".";
		}
		cout << endl;
	}
	void encode(short *psSourceAudio, char *pucDestAudio, unsigned int uiLength);
	void decode(char *pucSourceAudio, short *psDestAudio, unsigned int uiLength);

	static void encode2(short *psSourceAudio, unsigned char *pucDestAudio, uint32_t uiLength);
	static void decode2(unsigned char *pucSourceAudio, short *psDestAudio, uint32_t uiLength);

private:

	array<char, 8> man_shift;
	array<short, 8> seg_mask;
	array<short, 8> trail_mask;

	//char man_shift[8] = { 1, 1, 2, 3, 4, 5, 6, 7 };
	//short seg_mask[8] = { 0x1F, 0x3F, 0x7F, 0xFF, 0x1FF, 0x3FF, 0x7FF, 0xFFF };

	//short trail_mask[8] = { 1, 1, 2, 4, 8, 16, 32, 64 };


	// determine in which segment on the curve the value falls into
	const char getSegment(short value);
};