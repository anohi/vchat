#pragma once
#include <thread>
#include <map>
#include <vector>
#include <string>

#include "NetworkVoipClient.h"
#include "BaseGuiDialog.h"
#include "GuiListServer.h"

#include "../VektoriaApp/resource.h"

#pragma comment(lib, "comctl32.lib")

namespace Voip {
	namespace Gui {

		class CNetworkVoipGUIServerItem
		{
		public:
			enum ItemType {
				SERVER,
				CHANNEL,
				USER
			};

			CNetworkVoipGUIServerItem();
			CNetworkVoipGUIServerItem(std::string serverName, std::string extra, HTREEITEM self = NULL, ItemType itemType = ItemType::SERVER, HTREEITEM parent = NULL, int imageIndex = 0, int selectedImageIndex = 0);
			~CNetworkVoipGUIServerItem();

			std::string GetName();
			ItemType GetType();
			HTREEITEM self();
			HTREEITEM parent();
			HTREEITEM FindChild(std::string childName);

			void Init(std::string serverName, std::string extra, HTREEITEM self = NULL, ItemType itemType = ItemType::SERVER, HTREEITEM parent = NULL, int imageIndex = 0, int selectedImageIndex = 0);
			void Fini();

			void AddChild(CNetworkVoipGUIServerItem child);
			void RemoveChild(std::string childName);
			void RemoveAllChildren();
			const std::vector<CNetworkVoipGUIServerItem>& GetChildren();

		private:
			std::string m_serverName;
			std::string m_extra;
			HTREEITEM m_handle;
			HTREEITEM m_parentHandle;
			int m_imageIndex;
			int m_selectedImageIndex;
			ItemType m_itemType;

			std::vector<CNetworkVoipGUIServerItem> m_children;

		};


		class CGuiInputDialog : public CBaseGuiDialog
		{
		private:
			std::string m_title;
			std::string m_info;
			std::string m_input;
			function<void(std::string)> m_callback;

			bool m_bHasData;
			CGuiListServer *m_list;

		public:
			CGuiInputDialog(HINSTANCE hInstance, HWND parent = NULL, DLGPROC callback = CGuiInputDialog::helperDlgProcess);
			LRESULT CALLBACK DlgProcess(HWND, UINT, WPARAM, LPARAM) override;

			void Init();
			void Fini();

			CGuiInputDialog* SetInfo(function<void(std::string)>& callback, std::string title, std::string info);
			std::string GetText();
		};


		class CNetworkVoipGUI : public CBaseGuiDialog
		{
		private:
			HIMAGELIST						m_imageList;
			CGuiInputDialog*				m_inputDialog;
			Network::CNetworkVoipClient*	m_voipClient;

			std::map<std::string, CNetworkVoipGUIServerItem> m_servers;
			std::map<int, HWND> m_controls;

			// some helper functions
			std::vector<std::string> explode(const std::string& str, const char& delimeter);
			HTREEITEM insertItem(const std::string str, HTREEITEM parent, HTREEITEM insertAfter, int imageIndex, int selectedImageIndex);
			bool DrawControl(DRAWITEMSTRUCT *dis);

		public:
			enum LogType
			{
				INFO,
				WARNING,
				UNKNOWN_ERROR,
				TEXT_MESSAGE,
			};

			CNetworkVoipGUI();
			CNetworkVoipGUI(HINSTANCE hInstance, Network::CNetworkVoipClient* voipClient = nullptr, DLGPROC callback = CNetworkVoipGUI::helperDlgProcess, HWND parent = NULL);
			~CNetworkVoipGUI();

			void Init();
			void Fini();

			bool AddServer(std::string serverName, std::string serverAddr);
			bool RemoveServerByName(std::string serverName);
			bool RemoveServerByIndex(int index);

			void UpdateChannelList(std::string serverName);

			void Log(LogType logType, std::string message);

			LRESULT CALLBACK DlgProcess(HWND, UINT, WPARAM, LPARAM) override;
		};

	}
}
