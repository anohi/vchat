#pragma once

namespace Voip {
	namespace Gui {
		namespace Constants {
			// range from 0x1 to 0x7fffffff to avoid collision with system notifications which are negative
			enum GUIN_NOTIFICATIONCODE {
				GUIN_LBCLICKED = 0x1,
				GUIN_RBCLICKED,
				GUIN_DRAG_AND_DROP,
				GUIN_DROPPED_ON,
				GUIN_DRAGGED_OUT,
			};
		}

		namespace IDS {
			enum Element {
				// scenarios
				SCN_MAIN = 1000,
				SCN_EXIT,
				SCN_CURRENT_CHANNEL,
				SCN_CREATE_CHANNEL,
				SCN_JOIN_CHANNEL,
				SCN_HELP,
				SCN_SETTTINGS,
				SCN_CHANNEL_LIST,
				SCN_INFO_BOX,

				// buttons
				BTN_MAIN_JOIN_CHANNEL = 2000,
				BTN_MAIN_CREATE_CHANNEL,
				BTN_MAIN_SETTINGS,
				BTN_MAIN_QUIT,
				BTN_MAIN_CONNECT,
				BTN_MAIN_DISCONNECT,
				BTN_MAIN_MUTE_MIC,
				BTN_MAIN_MUTE_SPEAKER,
				BTN_MAIN_LEAVE_CHANNEL,
				BTN_MAIN_INFO,
				BTN_CANCEL,
				BTN_EXIT_CONFIRM,
				BTN_CREATE_CHANNEL_DONE,
				BTN_SETTINGS_DONE,
				BTN_PREVIOUS_PAGE,
				BTN_NEXT_PAGE,
				INPUTBOX_BTN_CANCEL,
				INPUTBOX_BTN_CONFIRM,

				// checkboxes
				CB_SETTINGS_AUTOCONNECT = 3000,
				CB_SETTINGS_AUTOJOIN,

				// text inputs
				IN_CREATE_CHANNEL_NAME = 4000,
				IN_CREATE_CHANNEL_PASSWORD,
				IN_SETTINGS_CHANNEL,
				IN_SETTINGS_USERNAME,
				IN_SETTINGS_SERVER_ADDR,
				IN_SETTINGS_PORT,
				IN_SEARCH_BOX,
				INPUTBOX_INPUTFIELD,

				// labels
				L_EXIT_LABEL = 5000,
				L_CREATE_CHANNEL_NAME_LABEL,
				L_CREATE_CHANNEL_PASSWORD_LABEL,
				L_SETTINGS_CHANNEL_LABEL,
				L_SETTINGS_USERNAME_LABEL,
				L_SETTINGS_SERVER_ADDR_LABEL,
				L_JOIN_PAGE_INFO,
				LABEL_INPUT_BOX,
				INPUTBOX_TITLE,


				CHANNEL_ITEM = 42,
				USER_ITEM,
				INFO_BOX_TEXT,
			};
		}
	}
}