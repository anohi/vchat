#pragma once

#ifndef _WINSOCK_DEPRECATED_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#endif


#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <winsock2.h>
#include <ws2tcpip.h>
#include <mstcpip.h> // INETADDR_ADDRESS

#include <iostream>
#include <string>
#include <deque>
#include "NetworkConstants.h"

#pragma comment(lib, "Ws2_32.lib")

namespace Voip {
	namespace Network {
#define DEFAULT_PORT 50023 // there is always an overloaded funtion to specify a port for yourself
#define MAX_CLIENTS 2 //tcp how many clients can be threaded
#define UDP_BUFFER_SIZE 1024 // udp buffer size f�r normale single brodcasts
#define MAX_UDP_BUFFER_SIZE 262142 // maximale zul�ssige Gr��e von winsock f�r ein UDP-Paket in Bytes

		class CNetworkBase
		{
		protected:
			struct Collection {
				char IP[50];
				char extra1[50];
				char extra2[50];

				
			};

			char		m_ac[UDP_BUFFER_SIZE];
			char		m_acCollect[MAX_UDP_BUFFER_SIZE];
			WSADATA		m_wsadata;

			SOCKET		m_socket;
			sockaddr_in m_si_info;
			int			m_si_info_len;
			unsigned int m_iCollectionPosition;
			unsigned int m_iCollectionCount;

			char m_clientHost[NI_MAXHOST];
			char m_clientService[NI_MAXSERV];
			//std::deque<std::pair<int, char*>> m_collection;
			std::deque<std::pair<unsigned int, char*>> m_collection;

			bool m_bStopReceiving;

			SOCKADDR_STORAGE m_from;
			int m_iFromLen = sizeof(m_from);
			SOCKET m_servSock[FD_SETSIZE];
			fd_set m_fdSockSet;
			timeval m_stTimeOut;
			int m_iNumSock;
			int m_ilastError;

		public:
			virtual ~CNetworkBase() {};

			virtual void Init(char *acAddress, int iPort = DEFAULT_PORT) = 0;

			void Cleanup();
			CNetworkBase* ClearBuffers();

			void HandleError(char * acMethod);

			void GetOwnIP(char * acIPAddress);
			void GetOwnName(char * acHost);
			void SetTimeOut(long lSecond = 86400, long lmSecond = 0);

			DWORD NextCollectionItemSize();
			bool IsCollectionEmpty();

			// Multiple Collects can be chained
			// CNetworkBase::Collect()->Collect()->Collect() etc...
			// order of extraction is from left to right
			CNetworkBase* Collect(bool &b);
			CNetworkBase* Collect(int &i);
			CNetworkBase* Collect(char *ac, int iLength);
			CNetworkBase* Collect(std::string strBuffer);

			template<typename ... Param>
			CNetworkBase* Collect(Param& ... param) {
				(void)initializer_list<int>{ (Collect(param), 0)... };
				return this;
			}

			// Extracting can be chained like this:
			// CNetworkBase::ExtractS()->ExtractS()->ExtractB();
			// CNetworkBase::ExtractS()->ExtractS()->ExtractI()->ExtractI();
			// notice ExtractB() and ExtractI() ends the chaining, so no further chaining possible
			// order of extraction is from left to right
			CNetworkBase* ExtractS(char *acBuffer, rsize_t bufferSize = UDP_BUFFER_SIZE);
			CNetworkBase* ExtractB(bool &param);
			CNetworkBase* ExtractI(int &param);

			bool ExtractB();
			int ExtractI();

			void SendHandshake();
			void RecvHandshake();
			void RecvHandshakes(int i);
			virtual void SendCollection() = 0;
		};
	}
}


