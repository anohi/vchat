#pragma once
#include "VTCPStream.h"
#include <vector>

class VTCPAcceptor
{
	std::string m_address;
	int			m_port;
	bool		m_listening;

public:
	VTCPAcceptor(int port, const char* address = "");
	~VTCPAcceptor();

	bool Start();
	VTCPStream* Accept();

private:
	VTCPStream *m_acceptorStream; // respoonsible for accepting (new) client connections
};

