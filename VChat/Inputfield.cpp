#include "Inputfield.h"


namespace Voip {
	namespace Gui {


		CInputfield::CInputfield()
		{
			m_bIsActive = false;
			m_bIsInteractive = true;
			m_bMouseOver = false;
			m_bRequired = false;
			m_bIsPassword = false;
			m_text.clear();
			m_helpText.clear();

			m_text.clear();
			m_brush = NULL;
			m_brushLine = NULL;
			m_layoutText = NULL;
			m_layoutHelpText = NULL;

			m_offset = { 0,0 };
			m_position = { 0, 0 };
			m_opacity = 0;
			m_zoom = 0;
			m_minMaxOpacity = { 0.5f, 1.0f };
		}


		CInputfield::~CInputfield()
		{
			Fini();
		}

		bool Voip::Gui::CInputfield::OnMouseMove(D2D1_POINT_2F & point)
		{
			if (m_bShouldRender && m_bIsInteractive && (m_bMouseOver = (
				point.x > (m_position.x + m_offset.x) &&
				point.y > (m_position.y + m_offset.y) &&
				point.x < (m_position.x + m_dimensions.x + m_offset.x) &&
				point.y < (m_position.y + m_dimensions.y + m_offset.y))))
			{
				Notify(m_gui, Notifications::GUIN_SETCURSOR_EDIT);
				return true;
			}
			return false;

		}

		bool Voip::Gui::CInputfield::HitTest(D2D1_POINT_2F & point)
		{
			if (OnMouseMove(point))
			{
				m_bIsActive = true;
				UpdateTextLayout();
				Notify(m_gui, Notifications::GUIN_ENTER_INPUT_MODE);
				return true;
			}

			m_bIsActive = false;
			Notify(m_gui, Notifications::GUIN_EXIT_INPUT_MODE);
			return false;
		}

		void Voip::Gui::CInputfield::Update(float & time, float & timeDelta)
		{
			if (m_bIsActive) {
				m_opacity -= timeDelta;
				if (m_opacity < 0.f)
					m_opacity = 1.f;
			}
			else {
				m_opacity = 1.f;
			}
		}

		void Voip::Gui::CInputfield::Render()
		{
			// draw help text
			if (m_layoutHelpText && m_text.empty()) {
				m_brush->SetColor(&Colors::GUIC_TEXT_BOX_BACKGROUND);
				m_brush->SetOpacity(.8f);
				CGuiHelpers::D2DRenderTarget->DrawTextLayout({ m_rect.left, m_rect.top }, m_layoutHelpText, m_brush);
			}

			// draw bottom line
			m_brushLine->SetOpacity(1.f);
			if (m_bRequired && m_text.empty()) {
				// draw required notification
				m_brushLine->SetColor(Colors::GUIC_STATUS_RED);
			}
			else {
				m_brushLine->SetColor(m_bIsActive ? &Colors::GUIC_BUTTON_GREEN : &Colors::GUIC_TEXT_BOX_BACKGROUND);
			}
			CGuiHelpers::D2DRenderTarget->DrawLine({ m_rect.left, m_rect.bottom }, { m_rect.right, m_rect.bottom }, m_brushLine);

			if (m_layoutText) {
				// draw text
				m_brush->SetColor(m_bgColor);
				CGuiHelpers::D2DRenderTarget->DrawTextLayout({ m_rect.left, m_rect.top }, m_layoutText, m_brush);
			}

			// draw caret
			if (m_bIsActive) {
				m_brushLine->SetOpacity(m_opacity);
				m_brushLine->SetColor(m_caretColor);
				CGuiHelpers::D2DRenderTarget->DrawLine({ m_caret.left, m_caret.top }, { m_caret.right, m_caret.bottom }, m_brushLine);
			}

		}

		void Voip::Gui::CInputfield::Move(float xDelta, float yDelta)
		{
		}

		void CInputfield::Init(HWND hwnd, int id, std::wstring help, bool bRequired, bool bIsPassword, D2D1_POINT_2F dimensions)
		{
			m_helpText = help;
			m_gui = hwnd;
			m_id = id;
			m_bRequired = bRequired;
			m_bIsPassword = bIsPassword;
			SetSize(dimensions.x, dimensions.y);
			m_bgColor = &Colors::GUIC_TEXT_WHITE;
			m_hoverColor = &Colors::GUIC_BUTTON_GREEN_HOVER;
			m_caretColor = &Colors::GUIC_TEXT_WHITE;
			CGuiHelpers::D2DRenderTarget->CreateSolidColorBrush(*m_bgColor, &m_brush);
			CGuiHelpers::D2DRenderTarget->CreateSolidColorBrush(*m_hoverColor, &m_brushLine);
			CGuiHelpers::CreateTextLayout(help, m_dimensions.x, m_dimensions.y, &m_layoutHelpText, &CGuiHelpers::DWTextFormat);
			UpdateTextLayout();
		}

		void CInputfield::ExitInputMode()
		{
			// exit input mode
			m_bIsActive = false;
			// notify Window about exit
			Notify(m_gui, Notifications::GUIN_EXIT_INPUT_MODE);
		}

		void Voip::Gui::CInputfield::Fini()
		{
			Helpers::SafeRelease(&m_brush);
			Helpers::SafeRelease(&m_brushLine);
			Helpers::SafeRelease(&m_layoutText);
			Helpers::SafeRelease(&m_layoutHelpText);
		}

		void Voip::Gui::CInputfield::SetSize(float x, float y)
		{
			m_dimensions = { x,y };
			SetPosition(m_position);
		}

		void Voip::Gui::CInputfield::Reset()
		{
			m_bMouseOver = false;
		}


		void CInputfield::CalculateCaretPosition()
		{
			DWRITE_TEXT_METRICS textMetrics;
			m_layoutText->GetMetrics(&textMetrics);

			// draw cursor at the end of text
			float left = textMetrics.widthIncludingTrailingWhitespace;
			
			m_caret = { 
				textMetrics.widthIncludingTrailingWhitespace + textMetrics.left + m_rect.left,
				(textMetrics.top/2) + m_rect.top, 
				textMetrics.widthIncludingTrailingWhitespace + textMetrics.left + m_rect.left,
				m_rect.bottom - textMetrics.top
			};
		}


		void CInputfield::SetText(std::string text)
		{
			m_text = std::wstring(text.begin(), text.end());
			UpdateTextLayout();
		}

		std::string CInputfield::GetText()
		{
			return std::string(m_text.begin(), m_text.end());
		}

		void CInputfield::SetHint(std::wstring hint)
		{
			m_helpText = hint;
			if (m_layoutHelpText) {
				Helpers::SafeRelease(&m_layoutHelpText);
			}
			CGuiHelpers::CreateTextLayout(hint, m_dimensions.x, m_dimensions.y, &m_layoutHelpText, &CGuiHelpers::DWTextFormat);
			CalculateCaretPosition();
		}

		void CInputfield::SetCaretColor(const D2D1_COLOR_F * color)
		{
			m_caretColor = color;
		}

		void CInputfield::UpdateTextLayout()
		{
			if (m_layoutText) {
				Helpers::SafeRelease(&m_layoutText);
			}
			CGuiHelpers::CreateTextLayout(m_text, m_dimensions.x, m_dimensions.y, &m_layoutText, &CGuiHelpers::DWTextFormat);
			CalculateCaretPosition();
		}

		void CInputfield::HandleInput(WPARAM param)
		{
			switch (param) {
			case VK_BACK: // delete char
				if (m_text.length()) {
					m_text.pop_back();
					UpdateTextLayout();
				}
				else {
					MessageBeep(UINT(-1));
				}
				break;
			case VK_TAB:
				break;
			case VK_RETURN:
				if (m_text.empty()) {
					MessageBeep(UINT(-1));
					break;
				}
				else {
					Notify(m_gui, Notifications::GUIN_INPUT_TEXT_RETURN);
				}
			case VK_ESCAPE:
				// exit input mode
				m_bIsActive = false;
				// notify Window about exit
				Notify(m_gui, Notifications::GUIN_EXIT_INPUT_MODE);
				break;
			default: // displayable characters
			{
				DWRITE_TEXT_METRICS textMetrics;
				m_layoutText->GetMetrics(&textMetrics);
				// only draw insert key if it's still in visible area of the inputfield
				if (textMetrics.widthIncludingTrailingWhitespace < (m_rect.right-m_rect.left)-10) {
					m_text.push_back(WCHAR(param));
					UpdateTextLayout();
				}
				else {
					MessageBeep(UINT(-1));
				}
				break;
			}
			}

		}
	}
}

