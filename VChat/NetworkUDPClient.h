#pragma once
#include "NetworkBase.h"
#include <string>
#include <sstream>

namespace Voip {
	namespace Network {
		class CNetworkUDPClient : public Voip::Network::CNetworkBase
		{
		public:
			CNetworkUDPClient();
			~CNetworkUDPClient();


			virtual void Init(char * acAddress, int iPort = DEFAULT_PORT) override;
			bool InitIPv6(char *acAddress, int iPort = DEFAULT_PORT);

			void SendI(int &i);
			void SendB(bool &b);
			void SendS(char *ac);

			int RecvS(char *ac);
			bool RecvB();
			int RecvI();

			void SendCollection();
			int RecvCollection();

			int Select();
		};
	}
}


