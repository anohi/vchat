#include "BaseGuiDialog.h"


namespace Voip {
	namespace Gui {

		LRESULT CBaseGuiDialog::helperDlgProcess(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			CBaseGuiDialog* self = reinterpret_cast<CBaseGuiDialog*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
			if (self == NULL) {
				return DefWindowProc(hWnd, msg, wParam, lParam);
			}
			return self->DlgProcess(hWnd, msg, wParam, lParam);
		}

		CBaseGuiDialog::CBaseGuiDialog()
		{
		}


		CBaseGuiDialog::~CBaseGuiDialog()
		{
		}

		void CBaseGuiDialog::ToggleZoom(bool state, int iSteps=20, int iDuration=100)
		{
			if (state) {
				AnimateWindow(m_gui, 200, AW_CENTER);
				// redraw the window because animation does not update client area
				RECT rect;
				GetClientRect(m_gui, &rect);
				RedrawWindow(m_gui, NULL, NULL, RDW_ERASE | RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN);
			}
			else {
				AnimateWindow(m_gui, 200, AW_CENTER | AW_HIDE);
			}
		}

		void CBaseGuiDialog::Show()
		{
			ToggleZoom(true);
			if (ShowWindow(m_gui, SW_SHOW)) {
				m_bGuiVisible = true;
				m_bIsRunning = true;
				SetFocus(m_gui);
			}
		}

		void CBaseGuiDialog::Hide()
		{
			ToggleZoom(false);
			if (ShowWindow(m_gui, SW_HIDE)) {
				m_bGuiVisible = false;
			}
			if (m_parent != NULL) {
				SetFocus(m_parent);
			}
		}

	}
}
