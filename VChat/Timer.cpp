#include "Timer.h"

namespace Voip {
	namespace Helpers {
		void CTimer::Tick()
		{
			lastCallToTick = currentCallToTick;
			LARGE_INTEGER t;
			QueryPerformanceCounter(&t);
			currentCallToTick = t.QuadPart;
		}

		void CTimer::Stop()
		{
		}

		void CTimer::Reset()
		{
			LARGE_INTEGER t;
			QueryPerformanceCounter(&t);
			startTime = t.QuadPart;
			currentCallToTick = t.QuadPart;
			lastCallToTick = t.QuadPart;
		}

		float CTimer::GetTimeTotal()
		{
			return (float)(currentCallToTick - startTime) / frequency;
		}

		float CTimer::GetTimeDelta()
		{
			return (float)(currentCallToTick - lastCallToTick) / frequency;
		}

		CTimer::CTimer()
		{
			LARGE_INTEGER t;
			QueryPerformanceFrequency(&t);
			frequency = t.QuadPart;

			Reset();
		}


		CTimer::~CTimer()
		{
		}
	}
}