#include "SoundBuffer.h"


namespace Voip {
	namespace Sound {
		CSoundBuffer::CSoundBuffer()
			:m_lpDSBuffer(NULL), m_nPlaybackSegments(0), m_bMuted(false)
		{
		}

		CSoundBuffer::~CSoundBuffer()
		{
			StopBuffer();
			if (m_lpDSBuffer)
			{
				m_lpDSBuffer->Release();
			}
		}

		bool CSoundBuffer::SetVolumeMin()
		{
			if (SUCCEEDED(m_lpDSBuffer->SetVolume(DSBVOLUME_MIN))) {
				return (m_bMuted = true);
			}
			return false;
		}

		bool CSoundBuffer::SetVolumeMax()
		{
			if (SUCCEEDED(m_lpDSBuffer->SetVolume(DSBVOLUME_MIN))) {
				m_bMuted = false;
				return true;
			}
			return false;
		}

		BOOL CSoundBuffer::PlayBuffer(DWORD dwFlags)
		{
			if (m_lpDSBuffer) {
				if (FAILED(m_lpDSBuffer->Play(0, 0, dwFlags))) {
					return FALSE;
				}
			}

			return TRUE;
		}

		BOOL CSoundBuffer::StopBuffer()
		{
			if (m_lpDSBuffer) {
				if (FAILED(m_lpDSBuffer->Stop())) {
					return FALSE;
				}
			}
			return TRUE;
		}

		BOOL CSoundBuffer::CreateSoundBuffer(LPDIRECTSOUND lpDS, WAVEFORMATEX wfxFormat)
		{
			DSBUFFERDESC dsbdesc;
			LPDIRECTSOUNDBUFFER tempDSBuffer = NULL;
			m_wfxFormat = wfxFormat;

			// Sets up DSBUFFERDESC structure. 
			// Flags determine what capabilities of the sound device should be enabled
			// DSBCAPS_CTRLVOLUME			change the volume
			// DSBCAPS_CTRLFREQUENCY		control the frequency at which the audio samples are played (on secondary buffer)
			// DSBCAPS_GLOBALFOCUS			sound can be played even if application is not focused, ideal for voip thread in background
			// DSBCAPS_CTRLPOSITIONNOTIFY	notifications on playing position so we can synchronize read and write access to the buffer
			// DSBCAPS_GETCURRENTPOSITION2  get and set the play cursor
			ZeroMemory(&dsbdesc, sizeof(DSBUFFERDESC));
			dsbdesc.dwSize = sizeof(DSBUFFERDESC);
			dsbdesc.dwFlags = DSBCAPS_CTRLVOLUME | DSBCAPS_CTRLFREQUENCY | DSBCAPS_GLOBALFOCUS | DSBCAPS_CTRLPOSITIONNOTIFY | DSBCAPS_GETCURRENTPOSITION2;
			dsbdesc.dwBufferBytes = m_wfxFormat.nAvgBytesPerSec;
			dsbdesc.lpwfxFormat = &m_wfxFormat;

			// creates secondary buffer
			if (SUCCEEDED(lpDS->CreateSoundBuffer(&dsbdesc, &tempDSBuffer, NULL))) {  // creates temporary primary buffer
				if (SUCCEEDED(tempDSBuffer->QueryInterface(IID_IDirectSoundBuffer8, (LPVOID*)(&m_lpDSBuffer)))) { // use primary buffer to get the address to a IID_IDirectSoundBuffer8 interface
					tempDSBuffer->Release();
					return TRUE;
				}
			}

			return FALSE;
		}

		BOOL CSoundBuffer::WriteData(short *data, DWORD dwSize)
		{
			if (!IsInit()) {
				return FALSE;
			}

			LPVOID  lpvPtr1;   // address of 1st write position on sound buffer
			DWORD   dwBytes1;  // number of bytes till the end of sound buffer, 
			LPVOID  lpvPtr2;   // address of 2nd write position on sound buffer if data to be written wraps around (NULL if no wrap)
			DWORD   dwBytes2;  // number of bytes to be written from 2nd address onwards (0 if no wrap)

			DWORD currentPlayPos, currentWritePos;
			m_lpDSBuffer->GetCurrentPosition(&currentPlayPos, &currentWritePos);

			DWORD writePos = ceil((double)currentWritePos / dwSize) * dwSize;
			if (writePos > (m_nPlaybackSegments - 1)*dwSize) {
				writePos = 0;
			}

			// Obtain memory address of write block. This will be in two parts if the block wraps around.
			if (DSERR_BUFFERLOST == m_lpDSBuffer->Lock(writePos, dwSize, &lpvPtr1, &dwBytes1, &lpvPtr2, &dwBytes2, 0)) {
				// If the buffer was lost, restore and retry lock.
				m_lpDSBuffer->Restore();
				if (FAILED(m_lpDSBuffer->Lock(writePos, dwSize, &lpvPtr1, &dwBytes1, &lpvPtr2, &dwBytes2, 0))) {
					return FALSE;
				}
			}

			// Write first part
			CopyMemory(lpvPtr1, data, dwBytes1);
			// Write the remaining data if wraps around
			if (NULL != lpvPtr2) {
				CopyMemory(lpvPtr2, data + dwBytes1, dwBytes2);
			}

			// Release locked data block back to DirectSound.
			if (FAILED(m_lpDSBuffer->Unlock(lpvPtr1, dwBytes1, lpvPtr2, dwBytes2))) {
				return FALSE;
			}

			return TRUE;
		}

		BOOL CSoundBuffer::FillSilence(DWORD segment, DWORD dwSize)
		{
			if (!IsInit()) {
				return FALSE;
			}

			LPVOID  lpvPtr1;   // address of 1st write position on sound buffer
			DWORD   dwBytes1;  // number of bytes till the end of sound buffer, 
			LPVOID  lpvPtr2;   // address of 2nd write position on sound buffer if data to be written wraps around (NULL if no wrap)
			DWORD   dwBytes2;  // number of bytes to be written from 2nd address onwards (0 if no wrap)

			// Obtain memory address of write block. This will be in two parts if the block wraps around.
			if (DSERR_BUFFERLOST == m_lpDSBuffer->Lock(segment*dwSize, dwSize, &lpvPtr1, &dwBytes1, &lpvPtr2, &dwBytes2, 0)) {
				// If the buffer was lost, restore and retry lock.
				m_lpDSBuffer->Restore();
				if (FAILED(m_lpDSBuffer->Lock(segment*dwSize, dwSize, &lpvPtr1, &dwBytes1, &lpvPtr2, &dwBytes2, 0))) {
					return FALSE;
				}
			}

			// Write first part
			ZeroMemory(lpvPtr1, dwBytes1);
			// Write the remaining data if wraps around
			if (NULL != lpvPtr2) {
				ZeroMemory(lpvPtr2, dwBytes2);
			}

			// Release the data back to DirectSound.
			if (FAILED(m_lpDSBuffer->Unlock(lpvPtr1, dwBytes1, lpvPtr2, dwBytes2))) {
				return FALSE;
			}

			return TRUE;
		}

		BOOL CSoundBuffer::SetNotifications(size_t count)
		{
			HRESULT	hr;
			LPDIRECTSOUNDNOTIFY8 pDSNotify;
			std::vector<DSBPOSITIONNOTIFY> playbackNotifications;

			if (NULL == m_lpDSBuffer) {
				return E_INVALIDARG;
			}
			if (FAILED(m_lpDSBuffer->QueryInterface(IID_IDirectSoundNotify, (LPVOID*)&pDSNotify))) {
				return FALSE;
			}

			// Create playback events.
			for (int i = 0; i < count; ++i) {
				HANDLE event = CreateEvent(NULL, FALSE, FALSE, NULL);
				if (event == NULL) {
					for each (auto event in m_hPlaybackEvents) {
						CloseHandle(event);
					}
					m_hPlaybackEvents.clear();
					return GetLastError();
				}
				m_hPlaybackEvents.push_back(event);
			}

			// Describe playback notifications. 
			int iSegmentSize = m_wfxFormat.nAvgBytesPerSec / count;
			for (int iPosition = 0; iPosition < count; iPosition++) {
				playbackNotifications.push_back({ (DWORD)(((iPosition + 1) * iSegmentSize) - 1), m_hPlaybackEvents.at(iPosition) });
			}

			// Create playback notifications.
			m_nPlaybackSegments = count;
			hr = pDSNotify->SetNotificationPositions(playbackNotifications.size(), &playbackNotifications[0]);
			pDSNotify->Release();
		}

		BOOL CSoundBuffer::IsInit()
		{
			if (m_lpDSBuffer) {
				return TRUE;
			}
			return FALSE;
		}

		DWORD CSoundBuffer::GetSegmentLength()
		{
			return m_hPlaybackEvents.size() ? m_wfxFormat.nAvgBytesPerSec / m_hPlaybackEvents.size() : m_wfxFormat.nAvgBytesPerSec;
		}

		const std::vector<HANDLE> CSoundBuffer::GetNotifications()
		{
			return m_hPlaybackEvents;
		}

		void CSoundBuffer::Cleanup()
		{
			StopBuffer();
			Helpers::SafeRelease(&m_lpDSBuffer);

			for each (auto &event in m_hPlaybackEvents)
			{
				if (event) {
					CloseHandle(event);
				}
			}
		}

		LPDIRECTSOUNDBUFFER8 CSoundBuffer::GetBuffer()
		{
			return m_lpDSBuffer;
		}

	}
}