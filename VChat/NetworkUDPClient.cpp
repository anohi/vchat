#include "NetworkUDPClient.h"

namespace Voip {
	namespace Network {
		CNetworkUDPClient::CNetworkUDPClient()
		{
			ZeroMemory(m_ac, UDP_BUFFER_SIZE);
			ZeroMemory(m_acCollect, MAX_UDP_BUFFER_SIZE);
			m_iCollectionPosition = 0;
			m_iFromLen = sizeof(m_from);
			m_socket = 0;
			FD_ZERO(&m_fdSockSet);
		}


		CNetworkUDPClient::~CNetworkUDPClient()
		{
		}

		bool CNetworkUDPClient::InitIPv6(char * acAddress, int iPort) {
			int i, RetVal;
			ADDRINFO Hints, *AddrInfo, *AI;

			// initialize winsock
			if (WSAStartup(MAKEWORD(2, 2), &m_wsadata) != 0) {
				this->HandleError("CNetworkUDPClient::Init() - WSAStartup()");
				WSACleanup();
			}

			ZeroMemory(&Hints, sizeof(Hints));
			Hints.ai_family = PF_UNSPEC;
			Hints.ai_socktype = SOCK_DGRAM;
			RetVal = getaddrinfo(strcmp(acAddress, "") ? acAddress : NULL, std::to_string(iPort).c_str(), &Hints, &AddrInfo);
			if (RetVal != 0) {
				this->HandleError("CNetworkUDPClient::Init() - getaddrinfo()");
				Cleanup();
				return false;
			}

			for (AI = AddrInfo; AI != NULL; AI = AI->ai_next) {
				m_socket = socket(AI->ai_family, AI->ai_socktype, AI->ai_protocol);
				if (m_socket == INVALID_SOCKET) {
					this->HandleError("CNetworkUDPClient::Init() - socket()");
					continue;
				}

				// reuse socket?
				int opt = 1;
				if (setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, (const char *)&opt, sizeof(opt)) < 0) {
					this->HandleError("CNetworkUDPClient::Init() -> setsockopt()");
				}

				if (connect(m_socket, AI->ai_addr, (int) AI->ai_addrlen) != SOCKET_ERROR) {
					break; // sucessfully connected to the address
				}

				i = WSAGetLastError();
				if (getnameinfo(AI->ai_addr, (int)AI->ai_addrlen, m_clientHost, sizeof(m_clientHost), NULL, 0, NI_NUMERICHOST) != 0) {
					strcpy_s(m_clientHost, sizeof(m_clientHost), "<unknown>");
				}

				this->HandleError("CNetworkUDPClient::Init() -> connect()");
				closesocket(m_socket);
			}

			if (AI == NULL) {
				this->HandleError("CNetworkUDPClient::Init() -> can't connect to server!");
				Cleanup();
				return false;
			}

			if (getpeername(m_socket, (LPSOCKADDR)& m_from, (int *)&m_iFromLen) == SOCKET_ERROR) {
				fprintf(stderr, "getpeername() failed with error %d\n", WSAGetLastError());
			}
			else {
				if (getnameinfo((LPSOCKADDR)& m_from, m_iFromLen, m_clientHost, sizeof(m_clientHost), NULL, 0, NI_NUMERICHOST) != 0) {
					strcpy_s(m_clientHost, sizeof(m_clientHost), "<unknown>");
				}
				printf("Connected to %s, port %d, protocol %s, protocol family %s\n",
					m_clientHost, ntohs(SS_PORT(&m_from)),
					(AI->ai_socktype == SOCK_STREAM) ? "TCP" : "UDP",
					(AI->ai_family == PF_INET) ? "PF_INET" : "PF_INET6");
			}

			freeaddrinfo(AddrInfo);

			if (getsockname(m_socket, (LPSOCKADDR)& m_from, &m_iFromLen) == SOCKET_ERROR) {
				fprintf(stderr, "getsockname() failed with error %d: %s\n", WSAGetLastError());
				Cleanup();
				return false;
			}
			else {
				if (getnameinfo((LPSOCKADDR)& m_from, m_iFromLen, m_clientHost, sizeof(m_clientHost), NULL, 0, NI_NUMERICHOST) != 0) {
					strcpy_s(m_clientHost, sizeof(m_clientHost), "<unknown>");
				}
				printf("Using local address %s, port %d\n", m_clientHost, ntohs(SS_PORT(&m_from)));
			}

			SetTimeOut();
			return true;
		}

		void CNetworkUDPClient::Init(char * acAddress, int iPort)
		{
			// initialize winsock
			if (WSAStartup(MAKEWORD(2, 2), &m_wsadata) != 0) {
				this->HandleError("CNetworkUDPClient::Init() - WSAStartup()");
				WSACleanup();
			}

			// BEGIN CLIENT SETTINGS ##########################################################
			// create socket
			if ((m_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR) {
				this->HandleError("CNetworkUDPClient::Init() -> socket()");
			}

			// configure address structure of client
			ZeroMemory((char*)&m_si_info, sizeof(m_si_info));
			m_si_info.sin_family = AF_INET;
			m_si_info.sin_port = htons(iPort);
			m_si_info.sin_addr.S_un.S_addr = inet_addr(acAddress);

			// reuse socket?
			int opt = 1;
			if (setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, (const char *)&opt, sizeof(opt)) < 0) {
				this->HandleError("CNetworkUDPClient::Init() -> setsockopt()");
			}

			// connect for later usage
			if (connect(m_socket, (sockaddr*)&m_si_info, sizeof(m_si_info)) == SOCKET_ERROR) {
				this->HandleError("CNetworkUDPClient::Init() -> connect()");
			}

			SetTimeOut(2, 0);
			// END CLIENT SETTINGS ############################################################
		}

		void CNetworkUDPClient::SendI(int & i)
		{
			std::string s = std::to_string(i);

			if (send(m_socket, s.c_str(), s.size(), 0) == SOCKET_ERROR) {
				this->HandleError("CNetworkUDPClient::SendI()");
			}
		}

		void CNetworkUDPClient::SendB(bool & b)
		{
			std::string s = "false";
			if (b) {
				s = "true";
			}

			if (send(m_socket, s.c_str(), s.size(), 0) == SOCKET_ERROR) {
				this->HandleError("CNetworkUDPClient::SendB()");
			}
		}

		void CNetworkUDPClient::SendS(char * ac)
		{
			if (send(m_socket, ac, strlen(ac), 0) == SOCKET_ERROR) {
				this->HandleError("CNetworkUDPClient::SendS()");
			}
		}

		int CNetworkUDPClient::RecvS(char * ac)
		{
			Select();
			if (m_ilastError == NetworkError::FAIL_SELECT) {
				this->HandleError("CNetworkUDPClient::RecvI() -> Select()");
				return m_ilastError;
			}
			else if (m_ilastError == NetworkError::TIME_OUT) {
				return m_ilastError;
			}

			ZeroMemory(m_ac, UDP_BUFFER_SIZE);
			if (recv(m_socket, ac, UDP_BUFFER_SIZE, 0) == SOCKET_ERROR) {
				this->HandleError("CNetworkUDPClient::RecvS()");
				return NetworkError::FAIL_RECEIVE;
			}
			return NetworkError::NONE;
		}

		bool CNetworkUDPClient::RecvB()
		{
			Select();
			if (m_ilastError == NetworkError::FAIL_SELECT) {
				this->HandleError("CNetworkUDPClient::RecvI() -> Select()");
				return 0;
			}
			else if (m_ilastError == NetworkError::TIME_OUT) {
				return 0;
			}

			ZeroMemory(m_ac, UDP_BUFFER_SIZE);
			if (recv(m_socket, m_ac, UDP_BUFFER_SIZE, 0) == SOCKET_ERROR) {
				this->HandleError("CNetworkUDPClient::RecvB()");
				return false;
			}
			if (strcmp("true", m_ac)) {
				return false;
			}
			return true;
		}

		int CNetworkUDPClient::RecvI()
		{
			Select();
			if (m_ilastError == NetworkError::FAIL_SELECT) {
				this->HandleError("CNetworkUDPClient::RecvI() -> Select()");
				return 0;
			}
			else if (m_ilastError == NetworkError::TIME_OUT) {
				return 0;
			}

			ZeroMemory(m_ac, UDP_BUFFER_SIZE);
			if (recv(m_socket, m_ac, UDP_BUFFER_SIZE, 0) == SOCKET_ERROR) {
				this->HandleError("CNetworkUDPClient::RecvB()");
				return 0;
			}

			try {
				return std::stoi(m_ac);
			}
			catch (std::invalid_argument) {
				return 0;
			}
			catch (std::out_of_range) {
				return 0;
			}
		}

		void CNetworkUDPClient::SendCollection()
		{
			// write number of collection items at beginning
			CopyMemory(m_acCollect, &m_iCollectionCount, ITEM_COUNT_FIELD);

			if (send(m_socket, m_acCollect, m_iCollectionPosition, 0) == SOCKET_ERROR) {
				this->HandleError("CNetworkBase::SendCollection()");
			}
			ClearBuffers();
		}

		int CNetworkUDPClient::RecvCollection() {
			ClearBuffers();

			Select();
			if (m_ilastError == NetworkError::FAIL_SELECT) {
				this->HandleError("CNetworkUDPClient::RecvCollection() -> Select()");
				return m_ilastError;
			}
			else if (m_ilastError == NetworkError::TIME_OUT) {
				return m_ilastError;
			}

			if (m_ilastError == NetworkError::NONE) {
				if (recv(m_socket, m_acCollect, MAX_UDP_BUFFER_SIZE, 0) == SOCKET_ERROR) {
					this->HandleError("CNetworkUDPClient::RecvCollection() -> recvfrom()");
					return NetworkError::FAIL_RECEIVE;
				}
				else {
					ZeroMemory(m_clientHost, sizeof(m_clientHost));
					getnameinfo((LPSOCKADDR)&m_from, m_iFromLen, m_clientHost, sizeof(m_clientHost), m_clientService, sizeof(m_clientService), NI_NUMERICHOST | NI_NUMERICSERV);

					unsigned short itemCount = 0;
					unsigned int fieldSize = 0;
					unsigned int position = ITEM_COUNT_FIELD;

					CopyMemory(&itemCount, m_acCollect, ITEM_COUNT_FIELD);

					for (unsigned short i = 0; i < itemCount; ++i) {
						fieldSize = 0;
						CopyMemory(&fieldSize, &m_acCollect[position], ITEM_SIZE_FIELD);
						position += ITEM_SIZE_FIELD; // moved pointer to data field

						if (fieldSize) {
							m_collection.push_back(std::pair<unsigned int, char*>(fieldSize, &m_acCollect[position]));
							position += fieldSize;
						}
					}

					return NetworkError::NONE;
				}
			}
			return m_ilastError;
		}

		int CNetworkUDPClient::Select() {
			if (!FD_ISSET(m_socket, &m_fdSockSet)) {
				FD_SET(m_socket, &m_fdSockSet);
			}

			int ret = SOCKET_ERROR;
			if ((ret = select(m_iNumSock, &m_fdSockSet, 0, 0, &m_stTimeOut)) == SOCKET_ERROR) {
				fprintf(stderr, "select() failed with error %d\n", WSAGetLastError());
				WSACleanup();
				m_ilastError = NetworkError::FAIL_SELECT;
				return m_ilastError;
			}

			if (FD_ISSET(m_socket, &m_fdSockSet)) {
				FD_CLR(m_socket, &m_fdSockSet);
			}

			if (ret == 0) {
				m_ilastError = NetworkError::TIME_OUT;
			}
			else {
				m_ilastError = NetworkError::NONE;
			}

			return ret;
		}
	}
}