#include "VTCPStream.h"


VTCPStream::VTCPStream()
	: m_pAddrinfoResult(NULL), m_pAddrinfoPtr(NULL), m_clientSocket(INVALID_SOCKET), m_listenSocket(INVALID_SOCKET)
{
}


VTCPStream::VTCPStream(const char *pcHostname, int iPort) :
	m_pAddrinfoResult(NULL),
	m_pAddrinfoPtr(NULL),
	m_clientSocket(INVALID_SOCKET),
	m_listenSocket(INVALID_SOCKET),
	m_sHostname(pcHostname),
	m_iPort(iPort)
{
}


VTCPStream::VTCPStream(SOCKET socket)
{
	this->m_clientSocket = socket;
}



bool VTCPStream::Init(bool bIsClient)
{
	// WSAStartup(DWORD version, &WSADATA wsaData)
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms742213(v=vs.85).aspx [02.03.2016]
	// 
	// Starts Winsock with a specified version (2.2).
	// 
	// returns 0 if successfull
	// otherwise:
	// WSASYSNOTREADY		The underlying network subsystem is not ready for network communication.
	// WSAVERNOTSUPPORTED	The version of Windows Sockets support requested is not provided by this particular Windows Sockets implementation.
	// WSAEINPROGRESS		A blocking Windows Sockets 1.1 operation is in progress.
	// WSAEPROCLIM			A limit on the number of tasks supported by the Windows Sockets implementation has been reached.
	// WSAEFAULT			The lpWSAData parameter is not a valid pointer.
	int iResult;
	WORD version = MAKEWORD(2, 2);

	if ((iResult = WSAStartup(version, &this->m_wsaData)) != 0) {
		cout << "WSAStartup failed: " << iResult << endl;
		return false;
	}

	// Makes sure that version 2.2 is supported
	//if (LOBYTE(this->m_wsaData.wVersion) != 2 || HIBYTE(this->m_wsaData.wVersion) != 2) {
	//	cout << "Could not find a useable version of Windock.dll" << endl;
	//	return false;
	//}

	//typedef struct addrinfo
	//{
	//	int                 ai_flags;       // AI_PASSIVE, AI_CANONNAME, AI_NUMERICHOST
	//	int                 ai_family;      // PF_xxx
	//	int                 ai_socktype;    // SOCK_xxx
	//	int                 ai_protocol;    // 0 or IPPROTO_xxx for IPv4 and IPv6
	//	size_t              ai_addrlen;     // Length of ai_addr
	//	char *              ai_canonname;   // Canonical name for nodename
	//	_Field_size_bytes_(ai_addrlen) struct sockaddr *   ai_addr;        // Binary address
	//	struct addrinfo *   ai_next;        // Next structure in linked list
	//}
	// We are using unspecified internet address family (so IPv6 and IPv4 address can be returned)
	// and stream socket for TCP
	ZeroMemory(&m_AddrinfoHints, sizeof(m_AddrinfoHints));
	if (bIsClient) { // client side
		this->m_AddrinfoHints = { 0, AF_INET, SOCK_STREAM, IPPROTO_TCP };
		iResult = getaddrinfo(this->m_sHostname.c_str(), std::to_string(this->m_iPort).c_str(), &this->m_AddrinfoHints, &this->m_pAddrinfoResult);
	}
	else { // server side
		this->m_AddrinfoHints = { AI_PASSIVE, AF_INET, SOCK_STREAM, IPPROTO_TCP };
		iResult = getaddrinfo(NULL, std::to_string(this->m_iPort).c_str(), &this->m_AddrinfoHints, &this->m_pAddrinfoResult);
	}
	if (iResult != 0) {
		cout << "getaddrinfo failed: " << iResult << endl;
		//WSACleanup();
		return false;
	}

	return true;
}


bool VTCPStream::BindAndListen() { 
	int iResult;

	this->m_listenSocket = socket(this->m_pAddrinfoResult->ai_family, this->m_pAddrinfoResult->ai_socktype, this->m_pAddrinfoResult->ai_protocol);
	if (this->m_listenSocket == INVALID_SOCKET) {
		DEBUG("Socket failed with error: ", WSAGetLastError());
		freeaddrinfo(this->m_pAddrinfoResult);
		//WSACleanup();
		return false;
	}

	iResult = bind(this->m_listenSocket, this->m_pAddrinfoResult->ai_addr, (int)this->m_pAddrinfoResult->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		DEBUG("bind failed with error: ", WSAGetLastError());
		freeaddrinfo(this->m_pAddrinfoResult);
		closesocket(this->m_listenSocket);
		//WSACleanup();
		return false;
	}

	iResult = listen(m_listenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		DEBUG("listen failed with error: ", WSAGetLastError());
		closesocket(this->m_listenSocket);
		//WSACleanup();
		return false;
	}

	return true; // bound and listening to incoming connections
}


bool VTCPStream::Connect() {
	// try connecting to one of the addresses returned by getaddrinfo
	for (this->m_pAddrinfoPtr = this->m_pAddrinfoResult; this->m_pAddrinfoPtr != NULL; this->m_pAddrinfoPtr = this->m_pAddrinfoPtr->ai_next) {
		// Create a Socket for connecting to server
		this->m_clientSocket = socket(this->m_pAddrinfoPtr->ai_family, this->m_pAddrinfoPtr->ai_socktype, this->m_pAddrinfoPtr->ai_protocol);
		if (this->m_clientSocket == INVALID_SOCKET) {
			DEBUG("Error at socket(): ", WSAGetLastError());
			//WSACleanup();
			return false;
		}

		// Connect to remote address
		int iResult = connect(this->m_clientSocket, this->m_pAddrinfoPtr->ai_addr, (int)this->m_pAddrinfoPtr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			DEBUG("Can't connect, retrying. err: ", GetLastError());
			closesocket(this->m_clientSocket);
			continue;
		}

		break; // connection established
	}

	return true; // everything went fine
}


SOCKET VTCPStream::Accept() {
	// Accept a client socket on server side
	this->m_clientSocket = accept(this->m_listenSocket, NULL, NULL);
	if (this->m_clientSocket == INVALID_SOCKET) {
		DEBUG("accept failed with error: ", WSAGetLastError());
		closesocket(this->m_listenSocket);
		//WSACleanup();
		return -1;
	}
	//u_long iMode = 1;
	//ioctlsocket(this->m_clientSocket, FIONBIO, &iMode);
	return this->m_clientSocket;
}


int VTCPStream::Send(char* pcBuffer, size_t length)
{
	return send(this->m_clientSocket, pcBuffer, length, 0);
}


int VTCPStream::Receive(char* pcBuffer, size_t length)
{
	return recv(this->m_clientSocket, pcBuffer, length, 0);
}


void VTCPStream::Fini()
{
	//freeaddrinfo(this->m_pAddrinfoResult);
	closesocket(this->m_clientSocket);
	shutdown(this->m_clientSocket, SD_BOTH);
	//WSACleanup();

	this->m_clientSocket = INVALID_SOCKET;
}


VTCPStream::~VTCPStream()
{
	Fini();
}