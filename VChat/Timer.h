#pragma once
#include <Windows.h>

namespace Voip {
	namespace Helpers{
		class CTimer
		{
		private:
			long long startTime;
			long long lastCallToTick;
			long long currentCallToTick;
			long long frequency;


		public:
			void Tick();
			void Stop();
			void Reset();

			float GetTimeTotal(); // time since Reset() was called
			float GetTimeDelta(); // time difference between two ticks

			CTimer();
			~CTimer();
		};
	}
}