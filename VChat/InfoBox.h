#pragma once
#include "GuiHelpers.h"
#include "Scenario.h"
#include "Label.h"
#include "ButtonRect.h"
#include "Timer.h"

namespace Voip {
	namespace Gui {
		class CInfoBox : public CScenario
		{
		protected:
			std::wstring m_text;
			Voip::Helpers::CTimer m_timer;
			float m_timeout;

		public:
			CInfoBox();
			~CInfoBox();

			void Init(HWND hwnd, int id, bool bShowBackground = false);
			void SetText(std::wstring text);
			void Show();
			void Update(float &time, float &timeDelta);
			void Render();
			void SetTimeout(float time);
		};
	}
}


