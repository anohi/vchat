#include "Checkbox.h"

namespace Voip {
	namespace Gui {

		CCheckbox::CCheckbox(bool bChecked)
		{
			m_bChecked = bChecked;
			m_pBitmapChecked = NULL;
			m_pBitmapUnchecked = NULL;
			m_dimensionsText = { 0,0 };
		}

		CCheckbox::~CCheckbox()
		{
			Fini();
		}

		void CCheckbox::Update(float & time, float & timeDelta)
		{
			if (m_bChecked) {
				m_opacity += timeDelta;
				if (m_opacity > m_minMaxOpacity.y)
					m_opacity = m_minMaxOpacity.y;
			}
			else {
				m_opacity -= timeDelta;
				if (m_opacity < m_minMaxOpacity.x)
					m_opacity = m_minMaxOpacity.x;
			}

			m_brush->SetOpacity(m_opacity);
		}
		
		void CCheckbox::Render()
		{
			if (m_bChecked) {
				m_brush->SetColor(m_hoverColor);
				CGuiHelpers::D2DRenderTarget->DrawRoundedRectangle(D2D1::RoundedRect(m_rect, 3, 3), m_brush);
				CGuiHelpers::D2DRenderTarget->FillRoundedRectangle(D2D1::RoundedRect(m_rect, 3, 3), m_brush);
				if (m_pBitmapChecked)
					CGuiHelpers::D2DRenderTarget->DrawBitmap(m_pBitmapChecked, m_rect);
			}
			else {
				m_brush->SetColor(m_bgColor);
				CGuiHelpers::D2DRenderTarget->DrawRoundedRectangle(D2D1::RoundedRect(m_rect, 3, 3), m_brush);
			}

			if (m_textLayout) {
				m_brush->SetColor(Colors::GUIC_TEXT_WHITE);
				CGuiHelpers::D2DRenderTarget->DrawTextLayout(D2D1::Point2F(m_rect.left + m_dimensions.x + 10, m_rect.top), m_textLayout, m_brush);
			}
		}

		void CCheckbox::Fini()
		{
			Helpers::SafeRelease(&m_pBitmapChecked);
			Helpers::SafeRelease(&m_brush);
		}

		void CCheckbox::Init(HWND hwnd, int id, std::wstring text, bool bChecked, D2D1_POINT_2F position, D2D1_POINT_2F dimensions, D2D1_POINT_2F dimensions_text)
		{
			m_gui = hwnd;
			m_id = id;
			m_bChecked = bChecked;
			m_position = position;
			m_dimensionsText = dimensions_text;
			m_text = text;
			m_pBitmapChecked = CGuiHelpers::LoadImage(Images::CHECKBOX_CHECKED);
			m_pBitmapUnchecked = CGuiHelpers::LoadImage(Images::CHECKBOX_UNCHECKED);
			SetBgColor(&Colors::WHITE);
			SetHoverColor(&Colors::GUIC_BUTTON_GREEN);

			CGuiHelpers::D2DRenderTarget->CreateSolidColorBrush(*m_bgColor, &m_brush);
			SetSize(dimensions.x, dimensions.y);
		}

		bool CCheckbox::OnMouseMove(D2D1_POINT_2F &point) {
			m_bMouseOver = (
				point.x > (m_position.x + m_offset.x) &&
				point.y > (m_position.y + m_offset.y) &&
				point.x < (m_position.x + m_dimensions.x + m_offset.x + (m_textLayout? m_dimensionsText.x : 0) ) &&
				point.y < (m_position.y + m_dimensions.y + m_offset.y));

			if (m_bShouldRender && m_bIsInteractive && m_bMouseOver) {
				Notify(m_gui, Notifications::GUIN_SETCURSOR_HAND);
				return true;
			}
			return false;
		}

		bool CCheckbox::HitTest(D2D1_POINT_2F & point)
		{
			if (OnMouseMove(point)) {
				ToggleCheck();
				return true;
			}
			return false;
		}

		bool CCheckbox::IsChecked()
		{
			return m_bChecked;
		}

		void CCheckbox::Check()
		{
			m_bChecked = true;
		}
		void CCheckbox::Uncheck()
		{
			m_bChecked = false;
		}
		void CCheckbox::ToggleCheck()
		{
			m_bChecked = !m_bChecked;
		}
		void CCheckbox::SetText(std::wstring text)
		{
			if (text.empty())
				return;

			Helpers::SafeRelease(&m_textLayout);
			m_text = text;
			CGuiHelpers::CreateTextLayout(text, m_dimensionsText.x, m_dimensionsText.y, &m_textLayout, &CGuiHelpers::DWTextFormat);
		}

		void CCheckbox::SetSize(float x, float y)
		{
			m_dimensions = { x,y };
			SetPosition(m_position.x, m_position.y);
			SetText(m_text);
		}
	}
}