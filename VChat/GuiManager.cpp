#include "GuiManager.h"


namespace Voip {
	namespace Gui {
		CGuiManager::CGuiManager()
		{
			m_activeScenario = NULL;
			m_activeInputElement = NULL;
			m_bInputMode = false;
		}


		CGuiManager::~CGuiManager()
		{
			Fini();
		}

		bool CGuiManager::Init(HWND hWnd)
		{
			// initialize helper
			return CGuiHelpers::Init(hWnd);
		}

		void CGuiManager::Fini()
		{
			for (auto &scenario : m_scenarios) {
				Helpers::SafeDelete(&scenario);
			}
			m_scenarios.clear();
			m_activeScenario = NULL;

			CGuiHelpers::Fini(true);
		}

		void CGuiManager::ClearScreen(float r, float g, float b)
		{
			CGuiHelpers::D2DRenderTarget->Clear(D2D1::ColorF(r, g, b));
		}

		void CGuiManager::ClearScreen(int r, int g, int b)
		{
			ClearScreen(r / 255.0f, g / 255.0f, b / 255.0f);
		}

		void CGuiManager::ClearScreen(const D2D_COLOR_F * color)
		{
			CGuiHelpers::D2DRenderTarget->Clear(color);
		}

		void CGuiManager::Update(float &time, float &timeDelta)
		{
			for (auto &scenario : m_scenarios) {
				if (scenario->ShouldRender()) {
					scenario->Update(time, timeDelta);
				}
			}
		}

		void CGuiManager::Render()
		{
			CGuiHelpers::D2DRenderTarget->BeginDraw();
			ClearScreen(&Colors::GUIC_SERVER_BACKGROUND);

			CGuiHelpers::D2DBrush->SetOpacity(1);
			CGuiHelpers::D2DBrush->SetColor(&Colors::GUIC_SERVER_BACKGROUND_HOVER);
			CGuiHelpers::D2DRenderTarget->FillRectangle({ 0,0, Dimensions::WINDOW_WIDTH, Dimensions::BAR_HEIGHT }, CGuiHelpers::D2DBrush);
			CGuiHelpers::D2DRenderTarget->FillRectangle({ 0, Dimensions::WINDOW_HEIGHT - Dimensions::BAR_HEIGHT, Dimensions::WINDOW_WIDTH, Dimensions::WINDOW_HEIGHT }, CGuiHelpers::D2DBrush);

			for (auto &scenario : m_scenarios) {
				if (scenario->ShouldRender()) {
					scenario->Render();
				}
			}
			CGuiHelpers::D2DRenderTarget->EndDraw();
		}

		void CGuiManager::Resize(D2D1_SIZE_U size)
		{
			CGuiHelpers::D2DRenderTarget->Resize(size);
		}

		void CGuiManager::AddScenario(CScenario* scenario)
		{
			if (scenario) {
				for (auto oldScenario : m_scenarios) {
					if (oldScenario->GetId() == scenario->GetId()) {
						delete oldScenario;
						m_scenarios.erase(std::remove(m_scenarios.begin(), m_scenarios.end(), oldScenario), m_scenarios.end());
					}
				}
				m_scenarios.push_back(scenario);
			}

			//if (m_scenarios.find(scenario->GetId()) != m_scenarios.end()) {
			//	// scenario with same id already exists, remove it
			//	m_scenarios.at(scenario->GetId())->Fini();
			//}
			// add new scenario
			//m_scenarios.insert({ scenario->GetId(), scenario });
		}

		void CGuiManager::SetActiveScenario(int id)
		{
			for (auto &scenario : m_scenarios) {
				if (scenario->GetId() == id) {
					if (m_activeScenario) {
						m_activeScenario->ResetControlState();
					}
					m_activeScenario = scenario;
					m_activeScenario->SetActiveState(true);
				}
			}

			//if (m_scenarios.find(id) != m_scenarios.end()) {
			//	if (m_activeScenario) {
			//		//m_activeScenario->SetActiveState(false);
			//		m_activeScenario->ResetControlState();
			//	}
			//	m_activeScenario = m_scenarios.at(id);
			//	m_activeScenario->SetActiveState(true);
			//}
		}

		CGuiElement * CGuiManager::GetElement(int id, int scenario)
		{
			if (!scenario) {
				// search in all scenarios
				for (auto &scenario : m_scenarios) {
					if (auto element = scenario->GetElement(id))
						return element;
				}
			}
			else {
				// search for element in provided scenario
				for (auto &scn : m_scenarios) {
					if (scn->GetId() == scenario) {
						if (auto element = m_scenarios.at(scenario)->GetElement(id))
							return element;
					}
				}
			}

			return nullptr;
		}


		CScenario * CGuiManager::GetActiveScenario()
		{
			return m_activeScenario;
		}

		CScenario * CGuiManager::GetScenario(int id)
		{
			for (auto &scenario : m_scenarios) {
				if (scenario->GetId() == id)
					return scenario;
			}

			return nullptr;
		}

		void CGuiManager::ActivateScenario(int id)
		{
			for (auto &scenario : m_scenarios) {
				if (scenario->GetId() == id)
					scenario->SetActiveState(true);
			}
		}

		void CGuiManager::DeactivateScenario(int id)
		{
			for (auto &scenario : m_scenarios) {
				if (scenario->GetId() == id)
					scenario->SetActiveState(false);
			}
		}

		void CGuiManager::ActivateAllScenarios(bool bVisible)
		{
			if (bVisible) {
				// only visible scenarios
				for (auto &scenario : m_scenarios) {
					if (scenario->ShouldRender()) {
						scenario->SetActiveState(true);
					}
				}
			}
			else {
				// all scenarios
				for (auto &scenario : m_scenarios) {
					scenario->SetActiveState(true);
				}
			}
		}

		void CGuiManager::DeactivateAllScenarios(bool bVisible)
		{
			if (bVisible) {
				// only visible scenarios
				for (auto &scenario : m_scenarios) {
					if (scenario->ShouldRender()) {
						scenario->SetActiveState(false);
					}
				}
			}
			else {
				// all scenarios
				for (auto &scenario : m_scenarios) {
					scenario->SetActiveState(false);
				}
			}
		}

		void CGuiManager::ShowScenario(int id, bool enabled)
		{
			for (auto &scenario : m_scenarios) {
				if (scenario->GetId() == id) {
					scenario->Show();
					scenario->SetActiveState(enabled);
				}
			}

		}

		void CGuiManager::HideScenario(int id, bool enabled)
		{
			for (auto &scenario : m_scenarios) {
				if (scenario->GetId() == id) {
					scenario->Hide();
					scenario->SetActiveState(enabled);
				}
			}
		}


		void CGuiManager::HideAllScenarios(int except)
		{
			for (auto &scenario : m_scenarios) {
				if (scenario->GetId() != except) {
					scenario->Hide();
					scenario->SetActiveState(false);
				}
			}
		}


		void CGuiManager::ShowElement(int id, bool enable, int scenario)
		{
			if (!scenario) {
				for (auto &scn : m_scenarios) {
					if (auto element = scn->GetElement(id)) {
						element->SetInteractivity(enable);
						element->SetVisibility(true);
					}
				}
			}
			else {
				for (auto &scn : m_scenarios) {
					if (scn->GetId() == scenario) {
						if (auto element = scn->GetElement(id)) {
							element->SetInteractivity(enable);
							element->SetVisibility(true);
						}
					}
				}
			}
		}

		void CGuiManager::HideElement(int id, bool disable, int scenario)
		{
			if (!scenario) {
				for (auto &scn : m_scenarios) {
					if (auto element = scn->GetElement(id)) {
						element->SetInteractivity(disable);
						element->SetVisibility(false);
					}
				}
			}
			else {
				for (auto &scn : m_scenarios) {
					if (scn->GetId() == scenario) {
						if (auto element = scn->GetElement(id)) {
							element->SetInteractivity(disable);
							element->SetVisibility(false);
						}
					}
				}
			}
		}


		void CGuiManager::DisplayTip(D2D1_POINT_2F & pos, std::string text)
		{
		}

		void CGuiManager::DisableElement(int id, bool hide, int scenario)
		{
			if (!scenario) {
				for (auto &scn : m_scenarios) {
					if (auto element = scn->GetElement(id)) {
						element->SetInteractivity(false);
						element->SetVisibility(hide);
						element->SetBgColor(&Colors::GUIC_TEXT_BOX_BACKGROUND);
					}
				}
			}
			else {
				for (auto &scn : m_scenarios) {
					if (scn->GetId() == scenario) {
						if (auto element = scn->GetElement(id)) {
							element->SetInteractivity(false);
							element->SetVisibility(hide);
							element->SetBgColor(&Colors::GUIC_TEXT_BOX_BACKGROUND);
						}
					}
				}
			}
		}

		void CGuiManager::EnableElement(int id, bool show, int scenario)
		{
			if (!scenario) {
				for (auto &scn : m_scenarios) {
					if (auto element = scn->GetElement(id)) {
						element->SetInteractivity(true);
						element->SetVisibility(show);
						element->SetBgColor(element->GetHoverColor());
					}
				}
			}
			else {
				for (auto &scn : m_scenarios) {
					if (scn->GetId() == scenario) {
						if (auto element = scn->GetElement(id)) {
							element->SetInteractivity(true);
							element->SetVisibility(show);
							element->SetBgColor(element->GetHoverColor());
						}
					}
				}
			}
		}

		bool CGuiManager::OnMouseMove(D2D1_POINT_2F & point)
		{
			for (auto &scenario : m_scenarios) {
				if (scenario->OnMouseMove(point))
					return true;
			}
			return false;
		}

		CGuiElement *CGuiManager::HitTest(D2D1_POINT_2F &point)
		{
			for (auto &scenario : m_scenarios) {
				if (auto elem = scenario->HitTest(point)) {
					return elem;
				}
			}

			return nullptr;
		}

		CGuiElement* CGuiManager::GetActiveInputElement()
		{
			return m_activeInputElement;
		}

		void CGuiManager::SetActiveInputElement(CGuiElement* element)
		{
			m_activeInputElement = element;
		}

		void CGuiManager::ExitInputMode() {
			if (m_activeInputElement) {
				dynamic_cast<CInputfield*>(m_activeInputElement)->ExitInputMode();
			}
		}

		void CGuiManager::Return() {
			if (m_activeScenario) {
				if (m_activeScenario->ShouldReturn()) {
					m_activeScenario = m_activeScenario->Return();
				}
			}
		}

		void Voip::Gui::CGuiManager::ShowInfoBox(std::wstring text)
		{
			for (auto &scenario : m_scenarios) {
				if (scenario->GetId() == IDS::Element::SCN_INFO_BOX) {
					auto infoBox = dynamic_cast<CInfoBox*>(scenario);
					// don't show if on exit or in info scenario
					if (m_activeScenario && (m_activeScenario->GetId() == IDS::Element::SCN_EXIT || m_activeScenario->GetId() == IDS::Element::SCN_INFO_BOX))
						return;

					infoBox->SetText(text);
					DeactivateAllScenarios();
					ShowScenario(IDS::Element::SCN_INFO_BOX);

					if (m_activeScenario) {
						infoBox->SetReturnScenario(m_activeScenario);
					}
					else {
						infoBox->SetReturnScenario(GetScenario(IDS::Element::SCN_MAIN));
					}

				}
			}
		}
	}
}