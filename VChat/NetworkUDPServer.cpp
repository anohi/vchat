#include "NetworkUDPServer.h"

namespace Voip {
	namespace Network {
		CNetworkUDPServer::CNetworkUDPServer()
		{
			ZeroMemory(m_ac, UDP_BUFFER_SIZE);
			ZeroMemory(m_acCollect, MAX_UDP_BUFFER_SIZE);
			m_iCollectionPosition = 0;
			m_iFromLen = sizeof(m_from);
			m_iNumSock = 0;
			m_socket = 0;
			FD_ZERO(&m_fdSockSet);
		}


		CNetworkUDPServer::~CNetworkUDPServer()
		{
		}


		void CNetworkUDPServer::Init(char * acAddress, int iPort)
		{
			// Initialize winsock
			if (WSAStartup(MAKEWORD(2, 2), &m_wsadata) != 0) {
				this->HandleError("CNetworkUDPServer::Init() -> WSAStartup()");
			}

			// BEGIN SERVER SETTINGS ##########################################################
			// Create socket
			if ((m_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == INVALID_SOCKET) {
				this->HandleError("CNetworkUDPServer::Init() -> socket()");
			}

			// Configure sockaddr_in structure of server
			ZeroMemory((char*)&m_si_info, m_si_info_len);
			m_si_info.sin_family = AF_INET;
			m_si_info.sin_port = htons(iPort);
			m_si_info.sin_addr.S_un.S_addr = INADDR_ANY;

			// reuse socket?
			int opt = 1;
			if (setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, (const char *)&opt, sizeof(opt)) < 0) {
				this->HandleError("CNetworkUDPServer::Init() -> setsockopt()");
			}

			// Bind socket
			if (bind(m_socket, (sockaddr*)&m_si_info, m_si_info_len) == SOCKET_ERROR) {
				this->HandleError("CNetworkUDPServer::Init() -> bind()");
			}

			SetTimeOut(2, 0);
			// END SERVER SETTINGS ############################################################
		}

		bool CNetworkUDPServer::InitIPv6(char * acAddress, int iPort)
		{
			int i, RetVal;
			ADDRINFO Hints, *AddrInfo, *AI;
			
			if ((RetVal = WSAStartup(MAKEWORD(2, 2), &m_wsadata)) != 0) {
				fprintf(stderr, "WSAStartup failed with error %d: %s\n", RetVal, gai_strerror(RetVal));
				WSACleanup();
				return false;
			}

			ZeroMemory(&Hints, sizeof(Hints));
			Hints.ai_family = PF_UNSPEC;
			Hints.ai_socktype = SOCK_DGRAM;
			Hints.ai_flags = AI_NUMERICHOST | AI_PASSIVE;
			RetVal = getaddrinfo(strcmp(acAddress, "") ? acAddress : NULL, std::to_string(iPort).c_str(), &Hints, &AddrInfo);
			if (RetVal != 0) {
				fprintf(stderr, "getaddrinfo failed with error %d: %s\n", RetVal, gai_strerror(RetVal));
				WSACleanup();
				return false;
			}

			for (i = 0, AI = AddrInfo; AI != NULL; AI = AI->ai_next) {
				if (i == FD_SETSIZE) {
					printf("getaddrinfo returned more addresses than we could use.\n");
					break;
				}

				if ((AI->ai_family != PF_INET) && (AI->ai_family != PF_INET6))
					continue;

				m_servSock[i] = socket(AI->ai_family, AI->ai_socktype, AI->ai_protocol);
				if (m_servSock[i] == INVALID_SOCKET) {
					fprintf(stderr, "socket() failed with error %d\n", WSAGetLastError());
					continue;
				}

				if ((AI->ai_family == PF_INET6) && IN6_IS_ADDR_LINKLOCAL((IN6_ADDR *)INETADDR_ADDRESS(AI->ai_addr)) && (((SOCKADDR_IN6 *)(AI->ai_addr))->sin6_scope_id == 0)) {
					fprintf(stderr, "IPv6 link local addresses should specify a scope ID!\n");
				}

				if (bind(m_servSock[i], AI->ai_addr, (int)AI->ai_addrlen) == SOCKET_ERROR) {
					fprintf(stderr, "bind() failed with error %d\n", WSAGetLastError());
					closesocket(m_servSock[i]);
					continue;
				}

				printf("'Listening' on port %d, protocol %s, protocol family %s\n", iPort, "UDP", (AI->ai_family == PF_INET6) ? "PF_INET6" : "PF_INET");
				i++;
			}

			freeaddrinfo(AddrInfo);

			if (i == 0) {
				fprintf(stderr, "Fatal error: unable to serve on any address.\n");
				WSACleanup();
				return false;
			}

			m_iNumSock = i;
			FD_ZERO(&m_fdSockSet);

			SetTimeOut();
			return true;
		}

		void CNetworkUDPServer::SendI(int & i)
		{
			std::string s = std::to_string(i);
			if (sendto(m_servSock[m_socket], s.c_str(), s.size(), 0, (LPSOCKADDR)&m_from, m_iFromLen) == SOCKET_ERROR) {
				this->HandleError("CNetworkUDPServer::SendI()");
			}
		}

		void CNetworkUDPServer::SendB(bool & b)
		{
			std::string s = "false";
			if (b) {
				s = "true";
			}

			if (sendto(m_servSock[m_socket], s.c_str(), s.size(), 0, (LPSOCKADDR)&m_from, m_iFromLen) == SOCKET_ERROR) {
				this->HandleError("CNetworkBase::SendB()");
			}
		}

		void CNetworkUDPServer::SendS(char * ac)
		{
			if (sendto(m_servSock[m_socket], ac, strlen(ac), 0, (LPSOCKADDR)&m_from, m_iFromLen) == SOCKET_ERROR) {
				this->HandleError("CNetworkBase::SendS()");
			}
		}

		int CNetworkUDPServer::RecvS(char * ac)
		{
			Select();
			if (m_ilastError == NetworkError::FAIL_SELECT) {
				this->HandleError("CNetworkUDPServer::RecvI() -> Select()");
				return m_ilastError;
			}
			else if (m_ilastError == NetworkError::TIME_OUT) {
				return m_ilastError;
			}

			ZeroMemory(m_ac, UDP_BUFFER_SIZE);
			if (recvfrom(m_servSock[m_socket], ac, UDP_BUFFER_SIZE, 0, (LPSOCKADDR)&m_from, &m_iFromLen) == SOCKET_ERROR) {
				this->HandleError("CNetworkBase::RecvS()");
				return NetworkError::FAIL_RECEIVE;
			}
			return NetworkError::NONE;
		}

		bool CNetworkUDPServer::RecvB()
		{
			Select();
			if (m_ilastError == NetworkError::FAIL_SELECT) {
				this->HandleError("CNetworkUDPServer::RecvB() -> Select()");
				return false;
			}
			else if (m_ilastError == NetworkError::TIME_OUT) {
				return false;
			}

			ZeroMemory(m_ac, UDP_BUFFER_SIZE);
			if (recvfrom(m_servSock[m_socket], m_ac, UDP_BUFFER_SIZE, 0, (LPSOCKADDR)&m_from, &m_iFromLen) == SOCKET_ERROR) {
				this->HandleError("CNetworkBase::RecvB()");
				m_ilastError = NetworkError::FAIL_RECEIVE;
				return false;
			}
			if (strcmp("true", m_ac)) {
				return false;
			}
			return true;
		}

		int CNetworkUDPServer::RecvI()
		{
			Select();
			if (m_ilastError == NetworkError::FAIL_SELECT) {
				this->HandleError("CNetworkUDPServer::RecvI() -> Select()");
				return 0;
			}
			else if (m_ilastError == NetworkError::TIME_OUT) {
				return 0;
			}

			ZeroMemory(m_ac, UDP_BUFFER_SIZE);
			if (recvfrom(m_servSock[m_socket], m_ac, UDP_BUFFER_SIZE, 0, (LPSOCKADDR)&m_from, &m_iFromLen) == SOCKET_ERROR) {
				this->HandleError("CNetworkBase::RecvB()");
				m_ilastError = NetworkError::FAIL_RECEIVE;
				return 0;
			}

			try {
				return std::stoi(m_ac);
			}
			catch (std::invalid_argument) {
				return 0;
			}
			catch (std::out_of_range) {
				return 0;
			}
		}

		int CNetworkUDPServer::Select() {
			//
			// For connection orientated protocols, we will handle the
			// packets received from a connection collectively.  For datagram
			// protocols, we have to handle each datagram individually.
			//

			//
			// Check to see if we have any sockets remaining to be served
			// from previous time through this loop. If not, call select()
			// to wait for a connection request or a datagram to arrive.
			//

			int i = 0, ret = SOCKET_ERROR;
			for (i = 0; i < m_iNumSock; i++) {
				if (FD_ISSET(m_servSock[i], &m_fdSockSet))
					break;
			}
			if (i == m_iNumSock) {
				for (i = 0; i < m_iNumSock; i++) {
					FD_SET(m_servSock[i], &m_fdSockSet);
				}
				if (select(m_iNumSock, &m_fdSockSet, 0, 0, &m_stTimeOut) == SOCKET_ERROR) {
					fprintf(stderr, "select() failed with error %d\n", WSAGetLastError());
					WSACleanup();
					m_ilastError = NetworkError::FAIL_SELECT;
					return m_ilastError;
				}
			}

			for (i = 0; i < m_iNumSock; i++) {
				if (FD_ISSET(m_servSock[i], &m_fdSockSet)) {
					FD_CLR(m_servSock[i], &m_fdSockSet);
					break;
				}
			}

			m_socket = i;

			if (ret == 0) {
				m_ilastError = NetworkError::TIME_OUT;
			}
			else {
				m_ilastError = NetworkError::NONE;
			}

			return m_ilastError;
		}

		void CNetworkUDPServer::SendCollection()
		{
			// write number of collection items at beginning
			CopyMemory(m_acCollect, &m_iCollectionCount, ITEM_COUNT_FIELD);

			if (sendto(m_servSock[m_socket], m_acCollect, m_iCollectionPosition, 0, (LPSOCKADDR)&m_from, m_iFromLen) == SOCKET_ERROR) {
				this->HandleError("CNetworkBase::SendCollection()");
			}
			ClearBuffers();
		}

		int CNetworkUDPServer::RecvCollection() {
			ClearBuffers();

			Select();
			if (m_ilastError == NetworkError::FAIL_SELECT) {
				this->HandleError("CNetworkUDPServer::RecvCollection() -> Select()");
				return m_ilastError;
			}
			else if (m_ilastError == NetworkError::TIME_OUT) {
				return m_ilastError;
			}

			if (m_ilastError == NetworkError::NONE) {
				if (recvfrom(m_servSock[m_socket], m_acCollect, MAX_UDP_BUFFER_SIZE, 0, (LPSOCKADDR)&m_from, &m_iFromLen) == SOCKET_ERROR) {
					this->HandleError("CNetworkUDPServer::RecvCollection() -> recvfrom()");
					return NetworkError::FAIL_RECEIVE;
				}
				else {
					ZeroMemory(m_clientHost, sizeof(m_clientHost));
					getnameinfo((LPSOCKADDR)&m_from, m_iFromLen, m_clientHost, sizeof(m_clientHost), m_clientService, sizeof(m_clientService), NI_NUMERICHOST | NI_NUMERICSERV);

					unsigned short itemCount = 0;
					unsigned int fieldSize = 0;
					unsigned int position = ITEM_COUNT_FIELD;

					CopyMemory(&itemCount, m_acCollect, ITEM_COUNT_FIELD);

					for (unsigned short i = 0; i < itemCount; ++i) {
						fieldSize = 0;
						CopyMemory(&fieldSize, &m_acCollect[position], ITEM_SIZE_FIELD);
						position += ITEM_SIZE_FIELD; // moved pointer to data field

						if (fieldSize) {
							m_collection.push_back(std::pair<unsigned int, char*>(fieldSize, &m_acCollect[position]));
							position += fieldSize; 
						}
					}

					return NetworkError::NONE;
				}
			}
			return m_ilastError;
		}
	}
}