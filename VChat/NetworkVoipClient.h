#pragma once
#include "NetworkUDPClient.h"
#include "NetworkUDPServer.h"
#include "SoundRecorder.h"
#include "SoundProcessing.h"
#include "NetworkVoipChannel.h"
#include "NetworkVoipUser.h"
#include "Helpers.h"

#include "VALawCodec.h"
#include <map>
#include <string>

namespace Voip {
	namespace Network {
		using namespace std;
		class CNetworkVoipClient
		{
		private:
			CNetworkUDPServer				m_localVoipServer;	// handles inbound voip data
			CNetworkUDPClient				m_remoteVoipServer;	// used for exchanging signals/data between voip server and client
			Voip::Sound::CSoundRecorder		m_soundRecorder;
			std::vector<HANDLE>				m_captureEvents;
			std::vector<HANDLE>				m_playbackEvents;
			std::queue<pair<int, short*>>	m_bufferPlayback;

			std::deque<map<string, pair<string, string>>> m_messages_server;  // <server, <username, message>>
			std::deque<map<string, pair<string, string>>> m_messages_channel; // <channelName, <username, message>>
			std::deque<map<string, pair<string, string>>> m_messages_user; // <remoteUser, <username, message>> 

			std::vector<CNetworkVoipUser*> m_users;

			HWND m_hwnd;

			string m_strOwnIp;
			string m_strOwnID;
			string m_strOwnPort;
			string m_strUserName;
			string m_strRemoteIP;
			string m_strChannel;

			bool m_bOnChannel;
			bool m_bOnServer;
			bool m_bBufferPlaying;
			bool m_bBufferSending;

			map<string, string> m_clients;		// list of clients on the server
			map<string, string> m_members;		// list of members in the group you are in
			vector<string>		m_operators;	// list of operators on a joined channel
			vector<string>		m_usersOnChannel; // list of users on a joined channel
			vector<string>		m_channels;		// list of groups on the server

			void HandleVoipRequests();
			void SendVoipData();
			void PlayBuffer(string strIdentifier);
			void ClearPlaybackBuffer(string strIdentifier);

			short* m_noiseData;
			std::function<void(int, void*)> m_callback;
		public:
			CNetworkVoipClient();
			~CNetworkVoipClient();

			void Init(HWND hwnd);


			VoipError JoinServer(string strUsername, string strServerAddress, int iPort = DEFAULT_PORT, string strPassword = "");
			bool JoinChannel(string strChannelName);
			bool CreateChannel(string strChannelName, int iMaxMembers = 10);
			void LeaveChannel();
			void LeaveServer();
			void AddUserToChannel(string strChannelName, string strpcUsername);
			void RemoveUserFromChannel(string strChannelName, string strUsername);
			bool SetVolumeMin();
			bool SetVolumeMax();
			bool MuteMic(bool state);
			bool IsMicMuted();
			bool IsSpeakerMuted();
			bool IsOnServer();
			bool IsOnChannel();
			void SetCallBack(const std::function<void(int dataType, void* data)> &callback);

			vector<string> GetChannelList();
			vector<string> GetUsers();
			string GetChannelName();
			void SendTextMessage(string strMessage, int iMessageLength, string strRemoteUser = "", string strChannel = "");
		};
	}
}


