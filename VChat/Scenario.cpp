#include "Scenario.h"


namespace Voip {
	namespace Gui {

		CScenario::CScenario()
			: m_bShouldRender(false), m_bIsActive(false)
		{
			m_offset = { 0, 0 };
			m_bounds = { 0,0,0,0 };
			m_center = { 0,0 };
			m_bZoomedIn = false;
			m_bIsTransitioning = false;
			m_zoomFactor = 0;
			m_returnScenario = nullptr;
			m_textLayoutTitle = NULL;
		}


		CScenario::~CScenario()
		{
			Fini();
		}

		void CScenario::Init(HWND hwnd, int id, bool bShowBackground)
		{
			m_gui = hwnd;
			m_id = id;
			m_bShowBackground = bShowBackground;
		}

		void CScenario::Fini()
		{
			Helpers::SafeRelease(&m_textLayoutTitle);
			for (auto &element : m_elements) {
				Helpers::SafeDelete(&element);
			}
			m_elements.clear();
		}

		void CScenario::Update(float &time, float &timeDelta)
		{
			if (m_bShouldRender) {
				if (!m_bIsTransitioning) {
					for (auto &element : m_elements) {
						element->Update(time, timeDelta);
					}
				} else {
					if (m_bZoomedIn) {
						m_zoomFactor += 5* timeDelta;
						//m_opacity += timeDelta;
						if (m_zoomFactor > 1) {
							m_zoomFactor = 1;
							m_opacity = .7f;
							m_bIsTransitioning = false;
							Notify(m_gui, Notifications::GUIN_ENTER_SCENARIO);
						}

					}
					else {
						m_zoomFactor -= 5*timeDelta;
						//m_opacity -= timeDelta;
						if (m_zoomFactor < 0) {
							m_zoomFactor = 0;
							m_opacity = 0;
							m_bIsTransitioning = false;
							m_bShouldRender = false;
							Notify(m_gui, Notifications::GUIN_SCENARIO_RETURN);
						}
					}
				}
			}
		}

		void CScenario::Render()
		{
			if (m_bShouldRender) {
				// draw background with 100% zoom
				if (m_bShowBackground) {
					CGuiHelpers::D2DBrush->SetColor(&Colors::BLACK);
					CGuiHelpers::D2DBrush->SetOpacity(m_opacity);
					CGuiHelpers::D2DRenderTarget->FillRectangle({ 0,0, Dimensions::WINDOW_WIDTH, Dimensions::WINDOW_HEIGHT }, CGuiHelpers::D2DBrush);				
				}

				// set zooming for scenario transition
				
				CGuiHelpers::D2DRenderTarget->GetTransform(&m_transformation);
				CGuiHelpers::D2DRenderTarget->SetTransform(D2D1::Matrix3x2F::Scale(D2D1::SizeF(m_zoomFactor, m_zoomFactor), m_center));

				//if (m_bShowBackground) {
				//	CGuiHelpers::D2DBrush->SetOpacity(m_opacity);
				//	CGuiHelpers::D2DBrush->SetColor(&Colors::GUIC_SERVER_BACKGROUND);
				//	CGuiHelpers::D2DRenderTarget->FillRectangle({ 0, Dimensions::BAR_HEIGHT, Dimensions::WINDOW_WIDTH, Dimensions::WINDOW_HEIGHT - Dimensions::BAR_HEIGHT }, CGuiHelpers::D2DBrush);
				//}
				
				for (auto &element : m_elements) {
					element->Render();
				}

				// render title
				if (m_textLayoutTitle) {
					CGuiHelpers::D2DBrush->SetColor(&Colors::GUIC_TEXT_HIGHLIGHT);
					CGuiHelpers::D2DRenderTarget->DrawTextLayout({ 0,0 }, m_textLayoutTitle, CGuiHelpers::D2DBrush);
				}

				// render border
				//CGuiHelpers::D2DBrush->SetColor(&Colors::GUIC_TEXT_BOX_BACKGROUND);
				//CGuiHelpers::D2DRenderTarget->DrawRectangle({ 0,0, Dimensions::WINDOW_WIDTH, Dimensions::WINDOW_HEIGHT }, CGuiHelpers::D2DBrush);
				CGuiHelpers::D2DRenderTarget->SetTransform(m_transformation);
			}
		}

		int CScenario::GetId()
		{
			return m_id;
		}

		bool CScenario::OnMouseMove(D2D1_POINT_2F & point)
		{
			if (m_bIsActive) {
				ResetControlState();
				for (auto &element : m_elements) {
					if (element->OnMouseMove(point))
						return true;
				}
			}

			return false;
		}

		CGuiElement * CScenario::HitTest(D2D1_POINT_2F &point)
		{
			if (m_bIsActive) {
				ResetControlState();
				for (auto &element : m_elements) {
					if (element->HitTest(point)) return element;
				}
			}
			
			return nullptr;
		}

		CGuiElement * CScenario::GetElement(int id)
		{
			if (id) {
				for (auto &element : m_elements) {
					if (element->GetId() == id) return element;
				}
			}
			
			return nullptr;
		}

		CScenario * CScenario::Return()
		{
			if (m_returnScenario) {
				ResetControlState();
				m_bIsActive = false;
				Hide();
				if (!m_returnScenario->IsActive()) {
					m_returnScenario->SetActiveState(true);
					if (!m_returnScenario->ShouldRender())
						m_returnScenario->Show();
				}
			}
			return m_returnScenario;
		}

		D2D1_POINT_2F CScenario::GetSize()
		{
			return{ m_bounds.right - m_bounds.left, m_bounds.bottom - m_bounds.top };
		}

		bool CScenario::IsActive()
		{
			return m_bIsActive;
		}

		bool CScenario::ShouldRender()
		{
			return m_bShouldRender;
		}

		bool CScenario::ShouldReturn()
		{
			return (m_returnScenario != nullptr);
		}

		void CScenario::SetActiveState(bool state)
		{
			m_bIsActive = state;
		}

		void CScenario::SetOffset(float x, float y)
		{
			m_offset = { x,y };
			m_center = { m_offset.x + (m_bounds.right - m_bounds.left) / 2, m_offset.y + (m_bounds.bottom - m_bounds.top) / 2 };

			for (auto &element : m_elements) {
				element->SetOffset(x, y);
			}
		}

		void CScenario::AddElement(CGuiElement * element)
		{
			if (element) {
				element->SetOffset(m_offset.x, m_offset.y);

				// recalculate boundaries
				auto pos = element->GetPosition(), dim = element->GetDimensions();
				if (pos.x < m_bounds.left)
					m_bounds.left = pos.x;
				if (pos.y < m_bounds.top)
					m_bounds.top = pos.y;
				if (pos.x + dim.x > m_bounds.right)
					m_bounds.right = pos.x + dim.x;
				if (pos.y + dim.y > m_bounds.bottom)
					m_bounds.bottom = pos.y + dim.y;

				// recalculate center
				m_center = { m_offset.x + (m_bounds.right - m_bounds.left) / 2, m_offset.y + (m_bounds.bottom - m_bounds.top) / 2 };

				// add new element
				m_elements.push_back(element);
			}
		}

		void CScenario::AddElementLeftTo(CGuiElement *anchor, CGuiElement * element, float padding)
		{
			if (anchor && element) {
				auto pos = anchor->GetPosition(), dim = element->GetDimensions();
				element->SetPosition(pos.x - (dim.x + padding), pos.y);
				AddElement(element);
			}
		}

		void CScenario::AddElementRightTo(CGuiElement *anchor, CGuiElement * element, float padding)
		{
			if (anchor && element) {
				auto pos = anchor->GetPosition(), dim = anchor->GetDimensions();
				element->SetPosition(pos.x + dim.x + padding, pos.y);
				AddElement(element);
			}
		}

		void CScenario::AddElementBelow(CGuiElement *anchor, CGuiElement * element, float padding)
		{
			if (anchor && element) {
				auto pos = anchor->GetPosition(), dim = anchor->GetDimensions();
				element->SetPosition(pos.x, pos.y + dim.y + padding);
				AddElement(element);
			}
		}

		void CScenario::AddElementAbove(CGuiElement *anchor, CGuiElement * element, float padding)
		{
			if (anchor && element) {
				auto pos = anchor->GetPosition(), dim = element->GetDimensions();
				element->SetPosition(pos.x, pos.y - (dim.y + padding));
				AddElement(element);
			}
		}

		void CScenario::Show()
		{
			m_bShouldRender = true;
			m_bIsTransitioning = true;
			m_bZoomedIn = true;
			m_zoomFactor = 0;
		}

		void CScenario::Hide()
		{
			//m_bShouldRender = false;
			m_bIsTransitioning = true;
			m_bZoomedIn = false;
			m_zoomFactor = 1;
		}

		void CScenario::SetReturnScenario(CScenario* scenario) {
			m_returnScenario = scenario;
		}

		void CScenario::SetCenter(D2D1_POINT_2F point)
		{
			m_center = point;
		}

		void CScenario::CenterWindow()
		{
			SetOffset((Dimensions::WINDOW_WIDTH - m_bounds.right) / 2, (Dimensions::WINDOW_HEIGHT - m_bounds.bottom) / 2);
		}

		void CScenario::ResetControlState()
		{
			for (auto &element : m_elements) {
				element->Reset();
			}
		}

		void CScenario::SetTitle(std::wstring title)
		{
			m_title = title;
			Helpers::SafeRelease(&m_textLayoutTitle);

			DWRITE_TEXT_ALIGNMENT ta = CGuiHelpers::DWTextFormatCenter->GetTextAlignment();
			CGuiHelpers::DWTextFormatCenter->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
			CGuiHelpers::CreateTextLayout(m_title, Dimensions::WINDOW_WIDTH, Dimensions::BAR_HEIGHT, &m_textLayoutTitle, &CGuiHelpers::DWTextFormatCenter);
			CGuiHelpers::DWTextFormatCenter->SetTextAlignment(ta);

		}

		void CScenario::Notify(HWND hwnd, Notifications code)
		{
			NMHDR hdr;

			hdr.hwndFrom = hwnd;
			hdr.idFrom = LONG_PTR(this);
			hdr.code = code;
			SendMessage(hwnd, WM_NOTIFY, hdr.idFrom, (LPARAM)&hdr);
		}
	}
}
