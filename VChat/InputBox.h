#pragma once
#include "Inputfield.h"
#include "Scenario.h"
#include "ButtonRect.h"
#include "Label.h"

namespace Voip {
	namespace Gui {
		class CInputBox : public CScenario
		{
			CInputfield* m_inputField;
			CButtonRect* m_btnCancel;
			CButtonRect* m_btnConfirm;
			CLabel*		 m_labelTitle;

		public:
			CInputBox();
			~CInputBox();

			void Init(HWND hwnd, int id, bool bShowBackground = false, std::wstring cancelBtn = L"Cancel", std::wstring confirmBtn = L"Ok");
			void Show();

			void SetInfo(std::wstring title, std::wstring hint);
			std::string GetText();
		};

	}
}
