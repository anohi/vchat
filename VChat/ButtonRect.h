#pragma once
#include "GuiHelpers.h"
#include "GuiElement.h"

namespace Voip {
	namespace Gui {
		class CButtonRect : public CGuiElement
		{
		protected:
			std::wstring m_text;
			IDWriteTextLayout* m_textLayout;
			ID2D1Bitmap* m_pBitmap;

		public:
			CButtonRect();
			~CButtonRect();

			CButtonRect* Init(HWND hwnd, int id, std::wstring text = L"",
				D2D1_POINT_2F position = { 0, 0 }, 
				D2D1_POINT_2F dimensions = { Dimensions::BUTTON_WIDTH, Dimensions::BUTTON_HEIGHT });


			virtual void SetText(std::wstring text);
			virtual void SetSize(float x, float y);
			virtual void Update(float &time, float &timeDelta);
			virtual void Render();
			virtual bool OnMouseMove(D2D1_POINT_2F &point);
			virtual bool HitTest(D2D1_POINT_2F &point);
			virtual void Move(float xDelta, float yDelta);
			virtual void Fini() override;
			virtual void Reset() override;

			virtual void SetBitmap(const std::wstring file);
			
		};
	}
}