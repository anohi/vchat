#pragma once

namespace Voip {
	namespace Network {
		enum VoipRequest {
			START_VOIP = 1000,
			END_VOIP,
			VOIP_DATA,
			TEXT_MESSAGE_USER,
			TEXT_MESSAGE_CHANNEL,
			TEXT_MESSAGE_SERVER,

			JOIN_SERVER,
			LEAVE_SERVER,
			CREATE_CHANNEL,
			JOIN_CHANNEL,
			LEAVE_CHANNEL,
			REMOVE_USER_FROM_CHANNEL,
			GET_CHANNELS_LIST,
			GET_CHANNEL_MEMBERS,
			GET_USER,
			GET_USERS_LIST,
		};

		enum VoipError {
			ACCESS_DENIED = 2000,
			CHANNEL_FULL,
			CHANNEL_ALREADY_EXISTS,
			CHANNEL_NOT_EXISTS,
			ALREADY_ON_CHANNEL,
			SERVER_FULL,
			CANT_JOIN_SERVER,
			USER_NOT_EXISTS,
			USER_NOT_ON_CHANNEL,
			USER_NOT_OPERATOR,
			CANT_GET_CHANNEL_MEMBERS,
			CANT_GET_CHANNEL_LIST,
			VE_NONE,
			OK,
			FAIL,
			ACCEPT,
		};

		enum NetworkError {
			TIME_OUT = 3000,
			CANT_CONNECT_TO_SERVER,
			FAIL_SELECT,
			FAIL_RECEIVE,
			NONE
		};

// used for network collection
#define ITEM_COUNT_FIELD 2 // 2 bytes (max 65535 items can be stored in the collection buffer)
#define ITEM_SIZE_FIELD 4 // 4 bytes (max 4294967296 integer values can be stored)
	}
}
