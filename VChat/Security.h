#pragma once
#include <math.h>

/*
CSecurity is used to encrypt and decrypt data using the RSA-algorithm
https://de.wikipedia.org/wiki/RSA-Kryptosystem (14.10.2016)

1. generate 2 very large primes p, q
2. create the RSA_Modul N = p * q;
3. determine phi(N) = phi(p*q) = phi(p) * phi(q) = (p-1)*(q-1)
4. choose a number e which satisfies: 1 < e < phi(N), gcd(e, phi(n)) = 1, e is prime
   (e, N) is public key
5. determine d which satisfies e*d=1 mod phi(N)
	<=> phi(N) | (e*d -1)
	<=> exists q:Z for	phi(N) * q		= e*d - 1
						e*d - q*phi(N)	= 1
						e*d + k*phi(N)	= 1
	solves e*d + k*phi(N) = 1 with extended euclidean algorithm
	(d, N) is private key

	private key should be stored in a rather safe environment
*/
namespace Voip {
	namespace Security {
		class CSecurity
		{
		public:
			CSecurity();
			~CSecurity();

			void Init(); // generate prime numbers
			void Encrypt(unsigned char* message);
			void Decrypt(unsigned char* message, long int publicKey);

		private:
			long int m_decryptinoKey;
			long int m_encryptionKey;

			long int m_prime1, m_prime2;

			bool isPrime(unsigned long long int num);
		};
	}
}