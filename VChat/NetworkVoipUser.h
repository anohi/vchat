#pragma once
#include "NetworkUDPClient.h"
#include "Helpers.h"

#include <deque>
#include <queue>

namespace Voip {
	namespace Network {

		class CNetworkVoipUser
		{
		public:
			CNetworkVoipUser();
			~CNetworkVoipUser();

			CNetworkVoipUser(std::string strUserName, std::string strUserID, std::string strIP, std::string strPort);

			std::string GetUserID();
			std::string GetUsername();
			std::string GetID();
			std::string GetIP();
			std::string GetPort();

			CNetworkUDPClient *GetUDPClient();

			friend std::ostream& operator<< (std::ostream& stream, const CNetworkVoipUser& user) {
				stream << user.m_strName << "#" << user.m_strID << ", " << user.m_strIP << ":" << user.m_strPort;
				return stream;
			}

			void PushData(char * data, int iSize);
			bool NextData(char ** data, int *iSize);
			void ClearVoipData();

		private:
			std::string	m_strName;
			std::string m_strID;
			std::string	m_strIP;
			std::string	m_strPort;
			CNetworkUDPClient m_udpClient;

			std::queue<std::pair<char*, int>> m_data;
		};

	}
}