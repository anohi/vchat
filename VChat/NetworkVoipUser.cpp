#include "NetworkVoipUser.h"

namespace Voip {
	namespace Network {

		CNetworkVoipUser::CNetworkVoipUser()
		{
		}


		CNetworkVoipUser::~CNetworkVoipUser()
		{
		}

		CNetworkVoipUser::CNetworkVoipUser(std::string strUserName, std::string strUserID, std::string strIP, std::string strPort)
		{
			m_strName = strUserName;
			m_strID = strUserID;
			m_strIP = strIP;
			m_strPort = strPort;
			m_udpClient.InitIPv6((char*)m_strIP.c_str(), stoi(m_strPort));
		}

		std::string CNetworkVoipUser::GetUsername()
		{
			return m_strName;
		}

		std::string CNetworkVoipUser::GetUserID()
		{
			return m_strName + "#" + m_strID;
		}

		std::string CNetworkVoipUser::GetID()
		{
			return m_strID;
		}

		std::string CNetworkVoipUser::GetIP()
		{
			return m_strIP;
		}

		std::string CNetworkVoipUser::GetPort()
		{
			return m_strPort;
		}

		CNetworkUDPClient * CNetworkVoipUser::GetUDPClient()
		{
			return &m_udpClient;
		}

		void CNetworkVoipUser::PushData(char * data, int iSize)
		{
			m_data.push({ data, iSize });
		}

		bool CNetworkVoipUser::NextData(char ** data, int *iSize)
		{
			if (m_data.size()) {
				*data = m_data.front().first;
				*iSize = m_data.front().second;
				m_data.pop();
				return true;
			}
			else {
				data = nullptr;
				*iSize = 0;
				return false;
			}
		}

		void CNetworkVoipUser::ClearVoipData()
		{
			while (m_data.size()) {
				Helpers::SafeDelete(&m_data.front().first);
				m_data.pop();
			}
		}

	}
}