#pragma once
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#include <windowsx.h>
#include <d2d1_1.h> // Defines C and C++ versions of the primary Direct2D API for Windows 8 and later.
#include <dwrite.h> // Defines DirectWrite API.
#include <Wincodec.h>

#include <string>
#include <wchar.h>

#include "Helpers.h"
#include "GuiIDs.h"

//#include "ButtonRect.h"

#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "dwrite.lib")
#pragma comment(lib, "Windowscodecs.lib")

namespace Voip {
	namespace Gui {

		class CGuiHelpers
		{
		public:
			static float scaleX;
			static float scaleY;

			static ID2D1Factory*			D2DFactory;
			static ID2D1HwndRenderTarget*	D2DRenderTarget;
			static ID2D1DrawingStateBlock*  D2DDrawingState;
			static ID2D1SolidColorBrush*	D2DBrush;
			static IDWriteFactory*			DWFactory;
			static IDWriteTextFormat*		DWTextFormat;
			static IDWriteTextFormat*		DWTextFormatCenter;
			static IWICImagingFactory*		IWICFactory;
			static HWND windowHandle;

			static bool Init(HWND hWnd);
			static bool Fini(bool state = false);

			static D2D1_POINT_2F PixelsToDips(float x, float y);
			static D2D1_POINT_2F PixelsToDips(D2D1_POINT_2F &point);
			static D2D1_RECT_F RectToDips(float x, float y, float width, float height);
			static D2D1_RECT_F PointsToDips(D2D1_POINT_2F &point1, D2D1_POINT_2F &point2);
			static D2D1_COLOR_F RgbToColorF(float r, float g, float b, float a = 100.0f);

			static void CreateTextLayout(std::wstring text, float maxWidth, float maxHeight, IDWriteTextLayout** layout, IDWriteTextFormat** format);

			static ID2D1Bitmap* LoadImage(std::wstring file);
		};

		namespace Images {
			const std::wstring CHECKBOX_CHECKED = std::wstring(L"images\\checkbox.png");
			const std::wstring CHECKBOX_UNCHECKED = std::wstring(L"images\\checkbox_unchecked.png");
			const std::wstring DISCONNECTED = std::wstring(L"images\\disconnected.png");
			const std::wstring CONNECTED = std::wstring(L"images\\connected.png");
		}

		namespace Colors {
			const D2D1_COLOR_F GUIC_SERVER_BACKGROUND		= CGuiHelpers::RgbToColorF(30, 33, 36);
			const D2D1_COLOR_F GUIC_SERVER_BACKGROUND_HOVER	= CGuiHelpers::RgbToColorF(40, 43, 48);
			const D2D1_COLOR_F GUIC_BUTTON_SERVER_DEFAULT	= CGuiHelpers::RgbToColorF(46, 49, 54);
			const D2D1_COLOR_F GUIC_BUTTON_SERVER_HOVER		= CGuiHelpers::RgbToColorF(114, 137, 218);

			const D2D1_COLOR_F GUIC_TEXT_WHITE			= CGuiHelpers::RgbToColorF(255, 255, 255);
			const D2D1_COLOR_F GUIC_TEXT_BLUE			= CGuiHelpers::RgbToColorF(114, 137, 218);
			const D2D1_COLOR_F GUIC_TEXT_HIGHLIGHT		= CGuiHelpers::RgbToColorF(25, 150, 188);
			const D2D1_COLOR_F GUIC_TEXT_BOX_BACKGROUND = CGuiHelpers::RgbToColorF(66, 69, 73);
			const D2D1_COLOR_F GUIC_TEXT_BOX_TEXT		= CGuiHelpers::RgbToColorF(104, 106, 110);

			const D2D1_COLOR_F GUIC_ICON_YELLOW	= CGuiHelpers::RgbToColorF(250, 166, 26);
			const D2D1_COLOR_F GUIC_STATUS_RED	= CGuiHelpers::RgbToColorF(240, 71, 71);

			const D2D1_COLOR_F GUIC_BUTTON_BLUE			= CGuiHelpers::RgbToColorF(88, 115, 211);
			const D2D1_COLOR_F GUIC_BUTTON_GREEN		= CGuiHelpers::RgbToColorF(54, 147, 104);
			const D2D1_COLOR_F GUIC_BUTTON_BLUE_HOVER	= CGuiHelpers::RgbToColorF(114, 137, 218);
			const D2D1_COLOR_F GUIC_BUTTON_GREEN_HOVER	= CGuiHelpers::RgbToColorF(60, 163, 116);

			const D2D1_COLOR_F BLACK = CGuiHelpers::RgbToColorF(0, 0, 0);
			const D2D1_COLOR_F WHITE = CGuiHelpers::RgbToColorF(255, 255, 255);
		}

		namespace Dimensions {
			const float BUTTON_WIDTH		= 100;
			const float BUTTON_HEIGHT		= 100;
			const float BUTTON_SMALL_WIDTH	= 49;
			const float BUTTON_SMALL_HEIGHT = 49;
			const float CHECKBOX			= 16;
			const float INPUTBOX_WIDTH		= 300;
			const float INPUTBOX_HEIGHT		= 200;
			const float INPUTFIELD_HEIGHT	= 24;
			const float TEXT_SIZE_NORMAL	= 12;
			const float TEXT_SIZE_BIG		= 14;
			const float PADDING				= 2;
			const float PADDING_BIG			= 10;
			
			const float WINDOW_WIDTH		= 400;
			const float WINDOW_HEIGHT		= 400;
			const float BAR_HEIGHT			= 40;
			const float CHANNEL_WIDTH = 160;
			const float CHANNEL_HEIGHT = 60;
		}

		// range from 0x1 to 0x7fffffff to avoid collision with system notifications which are negative
		enum Notifications {
			GUIN_LBCLICKED = 0x1,
			GUIN_RBCLICKED,
			GUIN_DRAG_AND_DROP,
			GUIN_DROPPED_ON,
			GUIN_DRAGGED_OUT,

			GUIN_ENTER_INPUT_MODE,
			GUIN_EXIT_INPUT_MODE,
			GUIN_INPUT_TEXT_RETURN,
			GUIN_BUTTON_CLICKED,
			GUIN_CHANNEL_ITEM_CLICKED,
			GUIN_ENTER_SCENARIO,
			GUIN_SCENARIO_RETURN,
			GUIN_USER_ITEM_CLICKED,
			GUIN_SETCURSOR_EDIT,
			GUIN_SETCURSOR_HAND,
			GUIN_SETCURSOR_ARROW
		};

		namespace Cursors {
			const HCURSOR HAND = LoadCursor(NULL, IDC_HAND);
			const HCURSOR ARROW = LoadCursor(NULL, IDC_ARROW);
			const HCURSOR EDIT = LoadCursor(NULL, IDC_IBEAM);
			const HCURSOR MOVING = LoadCursor(NULL, IDC_SIZEALL);
		}
	}
}

//#include "GuiElement.h"