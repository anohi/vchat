#include "NetworkBase.h"


namespace Voip {
	namespace Network {

		void CNetworkBase::Cleanup()
		{
			for (int i = 0; i < m_iNumSock; ++i) {
				if (FD_ISSET(m_servSock[i], &m_fdSockSet)) {
					FD_CLR(m_servSock[i], &m_fdSockSet);
				}
				shutdown(m_servSock[i], SD_BOTH);
				closesocket(m_servSock[i]);
				m_servSock[i] = 0;
			}
			FD_ZERO(&m_fdSockSet);
			if (m_socket) {
				shutdown(m_socket, SD_BOTH);
				closesocket(m_socket);
			}
			WSACleanup();
		}

		void CNetworkBase::GetOwnIP(char * acIPAddress)
		{

		}

		void CNetworkBase::GetOwnName(char * acHost)
		{
		}

		void CNetworkBase::HandleError(char * acMethod)
		{
			std::cout << acMethod << ": " << WSAGetLastError() << std::endl;
		}

		CNetworkBase* CNetworkBase::Collect(bool & b)
		{
			if (b) {
				Collect("true", 4);
			}
			else {
				Collect("false", 5);
			}
			return this;
		}

		CNetworkBase* CNetworkBase::Collect(int & i)
		{
			if (m_iCollectionPosition < ITEM_COUNT_FIELD) {
				ClearBuffers();
				m_iCollectionPosition = ITEM_COUNT_FIELD;
			}

			if ((m_iCollectionPosition + 2 * ITEM_SIZE_FIELD) < MAX_UDP_BUFFER_SIZE) {
				// write size
				int size = ITEM_SIZE_FIELD;
				CopyMemory(&m_acCollect[m_iCollectionPosition], &size, ITEM_SIZE_FIELD);
				m_iCollectionPosition += ITEM_SIZE_FIELD;
				// write data
				CopyMemory(&m_acCollect[m_iCollectionPosition], &i, ITEM_SIZE_FIELD);
				m_iCollectionPosition += ITEM_SIZE_FIELD;

				m_iCollectionCount++;
			}
			return this;
		}

		CNetworkBase* CNetworkBase::Collect(char *ac, int iLength) {
			if (m_iCollectionPosition < ITEM_COUNT_FIELD) {
				ClearBuffers();
				m_iCollectionPosition = ITEM_COUNT_FIELD;
			}

			if ((iLength + m_iCollectionPosition + ITEM_SIZE_FIELD) < MAX_UDP_BUFFER_SIZE) {
				// write size
				CopyMemory(&m_acCollect[m_iCollectionPosition], &iLength, ITEM_SIZE_FIELD);
				m_iCollectionPosition += ITEM_SIZE_FIELD;
				// write data
				CopyMemory(&m_acCollect[m_iCollectionPosition], ac, iLength);
				m_iCollectionPosition += iLength;

				m_iCollectionCount++;
			}
			else {
				this->HandleError("CNetworkBase::Collect()");
			}

			return this;
		}

		CNetworkBase* CNetworkBase::Collect(std::string strBuffer) {
			Collect((char*)strBuffer.c_str(), strBuffer.length());
			return this;
		}

		DWORD CNetworkBase::NextCollectionItemSize()
		{
			if (!IsCollectionEmpty()) {
				return m_collection.front().first;
			}
			return 0;
		}

		CNetworkBase* CNetworkBase::ExtractS(char * acBuffer, rsize_t bufferSize)
		{
			if (!IsCollectionEmpty()) {
				memcpy_s(acBuffer, bufferSize, m_collection.front().second, m_collection.front().first);
				m_collection.pop_front();
			}
			return this;
		}

		CNetworkBase * CNetworkBase::ExtractB(bool & param)
		{
			if (!IsCollectionEmpty()) {
				char value[8];
				ZeroMemory(value, 8);
				CopyMemory(value, m_collection.front().second, m_collection.front().first);
				m_collection.pop_front();

				param = (strcmp("true", value) == 0);
			}
			else {
				param = false;
			}
			return this;
		}

		CNetworkBase * CNetworkBase::ExtractI(int & param)
		{
			param = 0;
			if (!IsCollectionEmpty()) {
				CopyMemory(&param, m_collection.front().second, m_collection.front().first);
				m_collection.pop_front();
			}
			else {
				param = 0;
			}
			return this;
		}

		bool CNetworkBase::ExtractB()
		{
			if (!IsCollectionEmpty()) {
				char value[8];
				ZeroMemory(value, 8);
				CopyMemory(value, m_collection.front().second, m_collection.front().first);
				m_collection.pop_front();

				return (strcmp("true", value) == 0);
			}
			return false;
		}

		int CNetworkBase::ExtractI()
		{
			int retValue = 0;
			if (!IsCollectionEmpty()) {
				CopyMemory(&retValue, m_collection.front().second, m_collection.front().first);
				m_collection.pop_front();
			}

			return retValue;
		}

		bool CNetworkBase::IsCollectionEmpty()
		{
			if (m_collection.size()) {
				return false;
			}
			return true;
		}

		void CNetworkBase::SendHandshake()
		{
			if (sendto(m_socket, "HS", 2, 0, (sockaddr*)&m_si_info, m_iFromLen) == SOCKET_ERROR) {
				this->HandleError("CNetworkBase::SendHandshake()");
			}
		}

		void CNetworkBase::RecvHandshake()
		{
			ZeroMemory(m_ac, UDP_BUFFER_SIZE);
			if (recvfrom(m_socket, m_ac, UDP_BUFFER_SIZE, 0, (sockaddr*)&m_si_info, &m_iFromLen) == SOCKET_ERROR) {
				this->HandleError("CNetworkBase::RecvHandshake()");
			}
			if (strcmp("HS", m_ac)) {
				// wrong data
				this->HandleError("CNetowrkBase::RecvHandshake() -> wrong data received");
			}
		}

		void CNetworkBase::RecvHandshakes(int i)
		{
			int iNumHS = 0;
			while (true) {
				ZeroMemory(m_ac, UDP_BUFFER_SIZE);
				if (recvfrom(m_socket, m_ac, UDP_BUFFER_SIZE, 0, (LPSOCKADDR) &m_from, &m_iFromLen) == SOCKET_ERROR) {
					this->HandleError("CNetworkBase::recvHandshakes()");
					break;
				}
				if (strcmp("HS", m_ac) == 0) {
					iNumHS++;
				}
				if (iNumHS == i) {
					break;
				}
			}
		}

		CNetworkBase* CNetworkBase::ClearBuffers() {
			m_iCollectionPosition = 0;
			m_iCollectionCount = 0;
			m_collection.clear();

			ZeroMemory(m_ac, UDP_BUFFER_SIZE);
			ZeroMemory(m_acCollect, MAX_UDP_BUFFER_SIZE);

			return this;
		}

		void CNetworkBase::SetTimeOut(long lSecond, long lmSecond)
		{
			m_stTimeOut.tv_sec = lSecond;
			m_stTimeOut.tv_usec = lmSecond;
		}
	}
}