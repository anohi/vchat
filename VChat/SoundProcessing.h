#pragma once
#include <array>
#include <inttypes.h>
#include <iostream>
#include <map>
#include <vector>
#include <complex>
#include <valarray>
#include <Windows.h>

#include <limits>
#include <algorithm>

namespace Voip {
	namespace Sound {
		using namespace std;

		//double PI = 3.141592653589793238460;
		typedef complex<double> Complex;
		typedef valarray<Complex> CArray;

		class CSoundProcessing
		{
		public:
			CSoundProcessing();
			~CSoundProcessing();

			enum CODEC {
				ALAW,
				MLAW
			};

			enum Compression {

			};

			uint8_t *Encode(CODEC codec, int16_t *source, uint8_t *dest, uint32_t length);
			int16_t *Decode(CODEC codec, uint8_t *sourse, int16_t *dest, uint32_t length);

			/*
			analyse the (encoded) data to find out possible patterns for further compression
			*/
			void Analyse(uint8_t * samples, uint32_t numBytes);
			void printBits(int8_t value);

			long double Energy(int16_t *source, uint32_t length);
			int8_t* Compress(int16_t * source, uint32_t sourceLen, uint32_t &destLen);
			int16_t* Decompress(int8_t * source, uint32_t &destLen);

			void fft(CArray& x);
			void ifft(CArray& x);

			int16_t *Mix(int16_t *sample1, int16_t *sample2, uint16_t sampleCount);
			int8_t *Mix(vector<char*> *channels, uint16_t sampleCount);

		};
	}
}
