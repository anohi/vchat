#pragma once
#include "GuiHelpers.h"
#include "Scenario.h"
#include "User.h"

namespace Voip {
	namespace Gui {
		class CGroup : public CScenario
		{
		protected:
			int m_iMaxUsers;
			int m_iCurrentUsers;

			D2D1_POINT_2F m_slotPositions[20];
			//std::vector<CUser*> m_users;

			void SwapPosition(int iSrc, int iDest);

		public:
			CGroup();
			~CGroup();

			void AddUser(CUser &user);
			void RemoveUser(std::string id);
			void UpdateUserList(std::vector<std::string> &users);
			void UpdateTalkersList(std::vector<std::string> *users);
			int MaxUsers();
			int UserCount();
			
			CUser* HitTest(D2D1_POINT_2F &point);

			void Fini();

			void Update(float &time, float &timeDelta);
			void Render();
			void SetOffset(int x, int y);
		};
	}
}