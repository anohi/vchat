#pragma once
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <Commctrl.h>
#include <WinUser.h>
#include <stdlib.h>
#include <wchar.h>
#include <functional>
#include <string>
#include <thread>

#include "../VektoriaApp/resource.h"
#pragma comment(lib, "comctl32.lib")

namespace Voip {
	namespace Gui {
		class CBaseGuiDialog
		{
		protected:
			HINSTANCE	m_hInstance;
			HWND		m_parent;
			HWND		m_gui;
			bool		m_bGuiVisible;
			bool		m_bIsRunning;
			RECT		m_rect;
			
			void ToggleZoom(bool state, int iSteps, int iDuration); // zooms in[state=true] / out[state=false] for a given duration in equal steps

			static LRESULT CALLBACK helperDlgProcess(HWND, UINT, WPARAM, LPARAM);
		public:
			CBaseGuiDialog();
			~CBaseGuiDialog();
			
			virtual void Init() = 0;
			virtual void Fini() = 0;

			void Show();
			void Hide();
			
			virtual LRESULT CALLBACK DlgProcess(HWND, UINT, WPARAM, LPARAM) = 0;
		};
	}
}
