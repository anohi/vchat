#pragma once
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <windowsx.h>
#include <tchar.h>
#include <string>
#include <fstream>

#include "GuiManager.h"
#include "ButtonRect.h"
#include "Group.h"
#include "Checkbox.h"
#include "Inputfield.h"
#include "Label.h"
#include "ChannelList.h"
#include "InfoBox.h"
#include "Settings.h"
#include "InputBox.h"

#include "NetworkVoipClient.h"

#define WM_USER_SHELLICON WM_USER+1

namespace Voip {
	namespace Gui {
		class CMainWindow
		{
		protected:
			HINSTANCE	m_hInstance;
			HWND		m_parent;
			HWND		m_gui;
			RECT		m_rect;
			bool		m_bGuiVisible;
			bool		m_bIsRunning;
			bool		m_bLBDown;
			bool		m_bRBDown;
			
			D2D1_POINT_2F m_mousePos;
			CGuiManager	*m_guiManager;
			Voip::Network::CNetworkVoipClient *m_voipClient;

			static LRESULT CALLBACK helperWindowProc(HWND, UINT, WPARAM, LPARAM);
			LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
			LRESULT OnNotify(WPARAM wParam, LPARAM lParam);
			LRESULT OnKeyUp(WPARAM wParam, LPARAM lParam);

			CScenario* CreateMainScenario(int id);
			//CScenario* CreateSettingsScenario(int id);
			//CScenario* CreateChannelScenario(int id);
			CScenario* CreateExitConfirmationScenario(int id);

			void Connect();
			void Disconnect();
			void Join(std::string channel, bool autoJoin=false);
			void Leave();
			void CreateChannel(std::string channelName);
			void ShowChannelList();
			void VoipCallback(int code, void* data);
			//// enumerations of available scenarios and elements
			//enum Element {
			//	// scenarios
			//	SCN_MAIN = 1,
			//	SCN_EXIT,
			//	SCN_CURRENT_CHANNEL,
			//	SCN_CREATE_CHANNEL,
			//	SCN_JOIN_CHANNEL,
			//	SCN_HELP,
			//	SCN_SETTTINGS,
			//	SCN_CHANNEL_LIST,

			//	// buttons
			//	BTN_MAIN_JOIN_CHANNEL,
			//	BTN_MAIN_CREATE_CHANNEL,
			//	BTN_MAIN_SETTINGS,
			//	BTN_MAIN_QUIT,
			//	BTN_MAIN_CONNECT,
			//	BTN_MAIN_DISCONNECT,
			//	BTN_MAIN_MUTE_MIC,
			//	BTN_MAIN_MUTE_SPEAKER,
			//	BTN_MAIN_LEAVE_CHANNEL,
			//	BTN_MAIN_INFO,
			//	BTN_CANCEL,
			//	BTN_EXIT_CONFIRM,
			//	BTN_CREATE_CHANNEL_DONE,
			//	BTN_SETTINGS_DONE,

			//	// checkboxes
			//	CB_SETTINGS_AUTOCONNECT,
			//	CB_SETTINGS_AUTOJOIN,

			//	// text inputs
			//	IN_CREATE_CHANNEL_NAME,
			//	IN_CREATE_CHANNEL_PASSWORD,
			//	IN_SETTINGS_CHANNEL,
			//	IN_SETTINGS_USERNAME,
			//	IN_SETTINGS_SERVER_ADDR,

			//	// labels
			//	L_EXIT_LABEL,
			//	L_CREATE_CHANNEL_NAME_LABEL,
			//	L_CREATE_CHANNEL_PASSWORD_LABEL,
			//	L_SETTINGS_CHANNEL_LABEL,
			//	L_SETTINGS_USERNAME_LABEL,
			//	L_SETTINGS_SERVER_ADDR_LABEL,
			//};

		public:
			CMainWindow();
			~CMainWindow();
			
			bool Init(HINSTANCE hInstance, HWND hWnd);
			void Tick(float time, float timeDelta);
			void Fini();
			void Show();
			void Hide();
			void Minimize();
			void SysTray();
			void ShowInfo(std::wstring text);
			bool IsVisible();
		};
	}
}