#include "Label.h"


namespace Voip {
	namespace Gui {
		CLabel::CLabel()
		{
			m_position = { 0 , 0 };
			m_offset = { 0,0 };
			m_rect = { 0,0,0,0 };
			m_opacity = 1;
			m_zoom = 1;
			m_textLayout = NULL;
			m_brush = NULL;
			m_text.clear();
			m_bShouldRender = true;
			m_bIsInteractive = false;
			m_fontSize = Dimensions::TEXT_SIZE_BIG;
		}


		CLabel::~CLabel()
		{
			Fini();
		}

		bool CLabel::OnMouseMove(D2D1_POINT_2F & point)
		{
			return false;
		}

		bool CLabel::HitTest(D2D1_POINT_2F & point)
		{
			return false;
		}

		void CLabel::Update(float & time, float & timeDelta)
		{
		}

		void CLabel::Render()
		{
			if (m_textLayout && m_brush) {
				m_brush->SetColor(m_bgColor);
				CGuiHelpers::D2DRenderTarget->DrawTextLayout({ m_rect.left, m_rect.top }, m_textLayout, m_brush);
			}
		}

		void CLabel::Move(float xDelta, float yDelta)
		{
		}

		void CLabel::Fini()
		{
			Helpers::SafeRelease(&m_brush);
			Helpers::SafeRelease(&m_textLayout);
		}

		void CLabel::SetSize(float x, float y)
		{
			m_dimensions = { x,y };
			SetPosition(m_position);
		}

		void CLabel::Reset()
		{
		}

		void CLabel::Init(int id, std::wstring text, DWRITE_TEXT_ALIGNMENT alignment, std::wstring description)
		{
			m_id = id;
			m_text = text;
			m_description = description;
			m_textAlignment = alignment;
			m_bgColor = &Colors::GUIC_TEXT_BLUE;
			m_hoverColor = &Colors::GUIC_BUTTON_GREEN_HOVER;
			CGuiHelpers::D2DRenderTarget->CreateSolidColorBrush(*m_bgColor, &m_brush);
			CreateTextLayout();
		}

		void CLabel::SetTextAlignment(DWRITE_TEXT_ALIGNMENT alignment)
		{
			m_textAlignment = alignment;
			CreateTextLayout();
		}

		void CLabel::SetText(std::wstring text)
		{
			m_text = text;
			CreateTextLayout();
		}

		void CLabel::SetFontSize(float size)
		{
			m_fontSize = size;
			CreateTextLayout();
		}

		void CLabel::CreateTextLayout()
		{
			Helpers::SafeRelease(&m_textLayout);

			DWRITE_TEXT_ALIGNMENT ta = CGuiHelpers::DWTextFormatCenter->GetTextAlignment();
			CGuiHelpers::DWTextFormatCenter->SetTextAlignment(m_textAlignment);
			CGuiHelpers::CreateTextLayout(m_text, Dimensions::WINDOW_WIDTH, Dimensions::TEXT_SIZE_BIG, &m_textLayout, &CGuiHelpers::DWTextFormatCenter);
			CGuiHelpers::DWTextFormatCenter->SetTextAlignment(ta);

			// set bounding box (m_rect)
			DWRITE_TEXT_METRICS textMetrics;
			m_textLayout->GetMetrics(&textMetrics);
			SetSize(textMetrics.widthIncludingTrailingWhitespace, textMetrics.layoutHeight);
		}

	}
}
