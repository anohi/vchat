#pragma once
#include "NetworkConstants.h"
#include "NetworkVoipUser.h"
#include "SoundProcessing.h"
#include "Helpers.h"

#include <memory>
#include <algorithm>
#include <time.h>
#include <thread>
#include <map>
#include <vector>
#include <deque>
#include <queue>
#include <chrono>

namespace Voip {
	namespace Network {
		using namespace std;
		using namespace std::chrono;

		class CNetworkVoipChannel
		{
		public:
			CNetworkVoipChannel();
			~CNetworkVoipChannel();

			VoipError AddUser(CNetworkVoipUser *user);
			VoipError RemoveUser(CNetworkVoipUser *user);
			VoipError SetOperator(CNetworkVoipUser *user);
			VoipError RemoveOperator(CNetworkVoipUser *user);

			vector<CNetworkVoipUser*>* GetOperators();
			vector<CNetworkVoipUser*>* GetUsers();

			bool HasUser(CNetworkVoipUser *user);
			bool IsOperator(CNetworkVoipUser *user);

			int GetCurrentMembers();
			int GetMaxMembers();
			string GetChannelName();

			void SetMaxMembers(int iMaxMembers);
			void SetChannelName(string strChannelName);

			void FillBuffer(CNetworkVoipUser *user, char *data, int iSize);
			
			void StartBroadCast();
			void StopBroadCast();
			void Close(); // kick all users and close channel

			void Notify(int iType, CNetworkVoipUser *user);

			bool Init(int iVoicePacketCount);
			void ResetVoicePacketBuffer();

			friend ostream& operator<< (ostream& stream, const CNetworkVoipChannel& channel) {
				stream << channel.m_strChannelName << " (" << channel.m_users.size() << "/" << channel.m_iMaxMembers << ")";
				return stream;
			}
			
		private:
			struct Message {
				string message;
				time_point<system_clock> timestamp;
				shared_ptr<CNetworkVoipUser> from;
				shared_ptr<CNetworkVoipUser> to;
			};

			struct VoicePacket {
				int iPosition;
				int iPacketNumber;
				int iPacketSize;
				char *data;
			};

			int m_iMaxMembers;
			string m_strChannelName;
			vector<CNetworkVoipUser*> m_operators;
			vector<CNetworkVoipUser*> m_users;
			queue<unique_ptr<Message>> m_messageHistory;
			
			map<CNetworkVoipUser*, queue<char*>> m_voiceStreams;
			queue<char*> m_mixedStreams;

			bool m_bBroadcastRunning;
			int m_iPacketNumber;
		};
	}
}