#include "NetworkVoipClient.h"

namespace Voip {
	namespace Network {
		CNetworkVoipClient::CNetworkVoipClient()
			: m_hwnd(0), m_bOnChannel(false), m_bOnServer(false), m_bBufferPlaying(false), m_strChannel(""), m_strOwnIp(""), m_strRemoteIP(""), m_strUserName("")
		{
		}


		CNetworkVoipClient::~CNetworkVoipClient()
		{
		}

		void CNetworkVoipClient::Init(HWND hwnd) {
			m_hwnd = hwnd;

			// Initialize Audio Capture
			if (!m_soundRecorder.Init(m_hwnd) || !m_soundRecorder.InitMicrophone(m_hwnd)) {
				perror("Couldn't initialize direct sound\n");
			}

			// Set # notification points on the capture buffer
			m_soundRecorder.SetNotifications(16);
		}


		bool CNetworkVoipClient::SetVolumeMin()
		{
			return m_soundRecorder.SetVolumeMin();
		}

		bool CNetworkVoipClient::SetVolumeMax()
		{
			return m_soundRecorder.SetVolumeMax();
		}

		bool CNetworkVoipClient::MuteMic(bool state)
		{
			return m_soundRecorder.MuteMic(state);
		}

		bool CNetworkVoipClient::IsMicMuted()
		{
			return m_soundRecorder.IsMicMuted();
		}

		bool CNetworkVoipClient::IsSpeakerMuted()
		{
			return m_soundRecorder.IsSpeakerMuted();
		}

		bool Voip::Network::CNetworkVoipClient::IsOnServer()
		{
			return m_bOnServer;
		}

		bool Voip::Network::CNetworkVoipClient::IsOnChannel()
		{
			return m_bOnChannel;
		}

		VoipError CNetworkVoipClient::JoinServer(string strUsername, string strServerAddress, int iPort, string strPassword)
		{
			if (!m_bOnServer && !m_bOnChannel) {
				if (!strServerAddress.empty()) {
					//m_remoteVoipServer.Init((char*)strServerAddress.c_str(), iPort);
					m_remoteVoipServer.InitIPv6((char*)strServerAddress.c_str(), iPort);

					m_remoteVoipServer.SetTimeOut(2);

					m_strUserName = strUsername;

					int method = VoipRequest::JOIN_SERVER;
					m_remoteVoipServer.ClearBuffers()
						->Collect(method, strUsername, strPassword)
						->SendCollection();

					int error = m_remoteVoipServer.RecvCollection();
					switch (error)
					{
					case NetworkError::NONE:
						if (!m_remoteVoipServer.IsCollectionEmpty()) {
							int response = m_remoteVoipServer.ExtractI();
							if (response == VoipError::OK) {
								char myIp[50], myPort[50], myID[10];
								ZeroMemory(myID, sizeof(myID));
								ZeroMemory(myIp, sizeof(myIp));
								ZeroMemory(myPort, sizeof(myPort));

								m_remoteVoipServer.ExtractS(myID)->ExtractS(myIp)->ExtractS(myPort);
								//m_remoteVoipServer.ExtractS(myIp);
								//m_remoteVoipServer.ExtractS(myPort);

								m_strOwnID = myID;
								m_strOwnIp = myIp;
								m_strOwnPort = myPort;
								
								m_bOnServer = true;

								// start local server to receive voip data
								m_localVoipServer.InitIPv6("", stoi(myPort));
								thread thrLocalVoipServer([=]() {
									HandleVoipRequests();
								});
								thrLocalVoipServer.detach();
								return VoipError::OK;
							}
							else {
								m_bOnServer = false;
							}
						}
						break;
					default:
						// can't connect to server
						m_remoteVoipServer.Cleanup();
						break;
					}
					
				}
			}

			return VoipError::CANT_JOIN_SERVER;
		}

		void CNetworkVoipClient::LeaveServer()
		{
			if (m_bOnServer) {
				int method = VoipRequest::LEAVE_SERVER;
				m_remoteVoipServer.ClearBuffers()
					->Collect(method, m_strUserName, m_strOwnID)
					->SendCollection();

				m_bOnChannel = false;
				m_bOnServer = false;
				m_bBufferPlaying = false;
				m_bBufferSending = false;

				ClearPlaybackBuffer(m_strChannel);
				m_soundRecorder.DeleteSoundBuffer(m_strChannel);

				m_remoteVoipServer.Cleanup();
				m_localVoipServer.Cleanup();
			}
		}

		void CNetworkVoipClient::HandleVoipRequests()
		{
			while (m_bOnServer) {
				if (m_localVoipServer.RecvCollection() == NetworkError::NONE) {
					if (!m_localVoipServer.IsCollectionEmpty()) {
						if (!m_bOnChannel || !m_bOnServer) {
							m_localVoipServer.ClearBuffers();
							continue;
						}

						int request = m_localVoipServer.ExtractI();
						switch (request) {
						case VoipRequest::VOIP_DATA:
						{
							char caChannel[50];
							ZeroMemory(caChannel, sizeof(caChannel));
							m_localVoipServer.ExtractS(caChannel);

							int iPacketNumber = m_localVoipServer.ExtractI();
							int iSize = m_localVoipServer.NextCollectionItemSize();

							if (m_bOnChannel && m_bOnServer && m_soundRecorder.BufferExists(caChannel)) {
								auto soundData = new char[iSize];
								m_localVoipServer.ExtractS(soundData, iSize);
								// apply some filter to improve quality

								// push filtered data to buffer
								m_bufferPlayback.push({ iSize, (short*)(soundData) });

								// get who is talking
								if (auto usersTalking = m_localVoipServer.ExtractI()) {
									vector<string> talkers;
									char caUserID[50];
									for (int i = 0; i < usersTalking; i++) {
										ZeroMemory(caUserID, sizeof(caUserID));
										m_localVoipServer.ExtractS(caUserID);
										talkers.push_back(caUserID);
									}

									if (m_callback) {
										m_callback(VOIP_DATA, (void*)(&talkers));
									}
								}
							}

							break;
						}
						case VoipRequest::JOIN_CHANNEL:
						{
							char user[50];
							ZeroMemory(user, sizeof(user));
							m_localVoipServer.ExtractS(user);
							int userCount = m_localVoipServer.ExtractI();
							if (userCount > 1) {
								if (!m_bBufferPlaying) {
									m_bBufferPlaying = true;
									PlayBuffer(m_strChannel); // start thread to fill buffer with sound data and play it
								}
								if (!m_bBufferSending) {
									m_bBufferSending = true;
									SendVoipData(); // start thread that captures sound and send it to server
								}
							}
							
							if (m_callback) {
								m_callback(JOIN_CHANNEL, (void*)(user));
							}
							break;
						}
						case VoipRequest::LEAVE_CHANNEL:
						{
							char user[50];
							ZeroMemory(user, sizeof(user));
							m_localVoipServer.ExtractS(user);

							// how many are left on channel? should keep sending voice data?
							int userCount = m_localVoipServer.ExtractI();
							if (userCount < 2) {
								m_bBufferPlaying = false;
								m_bBufferSending = false;
								ClearPlaybackBuffer(m_strChannel);
							}
							
							if (m_callback) {
								m_callback(LEAVE_CHANNEL, (void*)(user));
							}
							break;
						}
						default:
							break;
						}
					}
				}
			}
		}

		void CNetworkVoipClient::SendVoipData()
		{
			m_soundRecorder.StartCapture();
			
			thread thrSendVoipData([=]() {
				const auto captureEvents	= m_soundRecorder.GetNotifications();
				DWORD	segmentSize			= m_soundRecorder.GetSegmentLength();
				DWORD   segmentsCount		= captureEvents->size();
				short  *capturedSoundData	= new short[segmentSize/2];
				int		method				= VoipRequest::VOIP_DATA;
				unsigned char *encoded		= new unsigned char[segmentSize/2];
				bool shouldSend;
				Voip::Sound::CSoundProcessing dsp;

				int silentCount = 0;
				long double silenceThreshold = 0;
				while (true) {
					DWORD segment = WaitForMultipleObjects(segmentsCount, &(*captureEvents)[0], false, 0);
					if (segment >= WAIT_OBJECT_0 && segment < segmentsCount) {
						int bytesRead = m_soundRecorder.ReadCapturedData(segment, capturedSoundData);
						silenceThreshold += dsp.Energy(capturedSoundData, bytesRead / 2);

						if (++silentCount == 16) {
							silenceThreshold /= 16;
							silentCount = 0;
							break;
						}

						uint32_t destLen = 0;
						char *dest = 0;
						//CSoundProcessing::Compress(capturedSoundData, dest, bytesRead / 2, destLen);
					}
				}

				while (m_bOnChannel) {
					if (m_bBufferSending) {
						DWORD segment = WaitForMultipleObjects(segmentsCount, &(*captureEvents)[0], false, 0);
						if (segment >= WAIT_OBJECT_0 && segment < segmentsCount) {
							short  *capturedVoiceData = new short[segmentSize / 2];
							int bytesRead = m_soundRecorder.ReadCapturedData(segment, capturedVoiceData);

							// analyse sound
							long double energy = dsp.Energy(capturedVoiceData, bytesRead / 2);
							shouldSend = ((silentCount = (energy > 2 * silenceThreshold) ? 0 : silentCount + 1) < 8) ? true : false;

							if (shouldSend) {
								m_remoteVoipServer.ClearBuffers()
									->Collect(method, m_strUserName, m_strOwnID, m_strChannel)
									->Collect((char*)capturedVoiceData, bytesRead)
									->SendCollection();

							}
						}
					}

					Sleep(1);
				}

				m_soundRecorder.StopCapture();
				// free memory
				Helpers::SafeDelete(&capturedSoundData);
			});
			thrSendVoipData.detach();
		}

		void CNetworkVoipClient::PlayBuffer(string strIdentifier)
		{
			m_soundRecorder.PlayBuffer(strIdentifier);
			
			// thread that is filling the playback buffer
			thread thrPlayback([=]() {
				auto soundBuffer = m_soundRecorder.GetBuffer(strIdentifier);
				auto playbackEvents = soundBuffer->GetNotifications();
				DWORD segmentSize = soundBuffer->GetSegmentLength();
				DWORD segmentsCount = playbackEvents.size();

				while (m_bOnChannel) {
					if (m_bBufferPlaying) {
						DWORD segment = WaitForMultipleObjects(segmentsCount, &playbackEvents[0], false, 0);
						if (segment >= WAIT_OBJECT_0 && segment < segmentsCount) {
							soundBuffer->FillSilence(segment, segmentSize);
							if (m_bufferPlayback.size()) {
								soundBuffer->WriteData(m_bufferPlayback.front().second, m_bufferPlayback.front().first);
								Helpers::SafeDelete(&m_bufferPlayback.front().second);
								m_bufferPlayback.pop();
							}
						}
					}

					Sleep(1);
				}
			});
			thrPlayback.detach();
		}

		void CNetworkVoipClient::ClearPlaybackBuffer(string strIdentifier)
		{
			while (m_bufferPlayback.size()) {
				Helpers::SafeDelete(&m_bufferPlayback.front().second);
				m_bufferPlayback.pop();
			}
			
			if (auto soundBuffer = m_soundRecorder.GetBuffer(strIdentifier)) {
				auto playbackEvents = soundBuffer->GetNotifications();
				DWORD segmentSize = soundBuffer->GetSegmentLength();
				DWORD segmentsCount = playbackEvents.size();

				for (int i = 0; i < segmentsCount; i++) {
					soundBuffer->FillSilence(i, segmentSize);
				}
			}
		}

		bool CNetworkVoipClient::CreateChannel(string strChannelName, int iMaxMembers)
		{
			if (m_bOnServer) {
				int method = VoipRequest::CREATE_CHANNEL;
				m_remoteVoipServer.ClearBuffers()
					->Collect(method, m_strUserName, m_strOwnID, strChannelName, iMaxMembers)
					->SendCollection();

				if (m_remoteVoipServer.RecvB()) {
					if (!m_soundRecorder.BufferExists(strChannelName)) {
						m_soundRecorder.CreateSoundBuffer(strChannelName, Voip::Sound::PCM_8000_MONO);
					}
					m_strChannel = strChannelName;
					m_bOnChannel = true;
					return true;
				}
			}

			return false;
		}

		bool CNetworkVoipClient::JoinChannel(string strChannelName)
		{
			if (m_bOnServer) {
				int method = VoipRequest::JOIN_CHANNEL;
				m_remoteVoipServer.ClearBuffers()
					->Collect(method, m_strUserName, m_strOwnID, strChannelName)
					->SendCollection();

				if (m_remoteVoipServer.RecvCollection() == NetworkError::NONE) {
					if (!m_remoteVoipServer.IsCollectionEmpty()) {
						switch (m_remoteVoipServer.ExtractI())
						{
						case VoipError::FAIL:
							break;
						case VoipError::CHANNEL_NOT_EXISTS:
							break;
						case VoipError::CHANNEL_FULL:
							break;
						case VoipError::ACCESS_DENIED:
							break;
						case VoipError::OK:
						{
							m_strChannel = strChannelName;
							m_bOnChannel = true;

							// create soundbuffer for joined channel
							if (!m_soundRecorder.BufferExists(strChannelName)) {
								m_soundRecorder.CreateSoundBuffer(strChannelName, Voip::Sound::PCM_8000_MONO);
								int userCount = m_remoteVoipServer.ExtractI();
								if (userCount > 1) {
									m_bBufferPlaying = true;
									m_bBufferSending = true;
									PlayBuffer(strChannelName); // start thread to fill buffer with sound data and play it
									SendVoipData(); // start thread that captures sound and send it to server
								}
							}
							return true;
						}
						default:
							break;
						}

					}
				}
			}

			return false;
		}

		void CNetworkVoipClient::LeaveChannel()
		{
			if (m_bOnChannel) {
				int method = VoipRequest::LEAVE_CHANNEL;
				m_remoteVoipServer.ClearBuffers()
					->Collect(method, m_strUserName, m_strOwnID)
					->SendCollection();

				m_bOnChannel = false;
				m_bBufferPlaying = false;
				m_bBufferSending = false;

				ClearPlaybackBuffer(m_strChannel);
				m_soundRecorder.DeleteSoundBuffer(m_strChannel);
			}
		}

		void CNetworkVoipClient::AddUserToChannel(string strChannelName, string strUsername)
		{
			if (m_bOnServer) {
				int method = VoipRequest::JOIN_CHANNEL;
				m_remoteVoipServer.ClearBuffers()
					->Collect(method, strUsername, strChannelName)
					->SendCollection();

				if (m_remoteVoipServer.RecvB()) {
					//std::cout << "you added {" << strUsername << "} to {" << strChannelName << "}\n";
				}
			}
		}

		void CNetworkVoipClient::RemoveUserFromChannel(string strChannelName, string strUsername)
		{
			if (m_bOnServer) {
				int method = VoipRequest::REMOVE_USER_FROM_CHANNEL;
				m_remoteVoipServer.ClearBuffers()
					->Collect(method, strUsername, strChannelName)
					->SendCollection();

				if (m_remoteVoipServer.RecvB()) {
					//std::cout << "you removed {" << strUsername << "} from {" << strChannelName << "}\n";
				}
			}
		}

		vector<string> CNetworkVoipClient::GetChannelList()
		{
			if (!m_bOnServer) {
				return{};
			}

			int method = VoipRequest::GET_CHANNELS_LIST;
			m_remoteVoipServer.ClearBuffers()->Collect(method, m_strUserName)->SendCollection();

			// update channels
			m_channels.clear();
			if (m_remoteVoipServer.RecvCollection() == NetworkError::NONE) {
				if (!m_remoteVoipServer.IsCollectionEmpty()) {
					int channels = m_remoteVoipServer.ExtractI();
					int users = 0;
					char buffer[50];
					for (int i = 0; i < channels; i++) {
						ZeroMemory(buffer, sizeof(buffer));
						m_remoteVoipServer.ExtractS(buffer); // channel name
						m_channels.push_back(buffer);
						users = m_remoteVoipServer.ExtractI(); // operator count
						for (int op = 0; op < users; op++) {
							ZeroMemory(buffer, sizeof(buffer));
							m_remoteVoipServer.ExtractS(buffer); // operator name
						}

						users = m_remoteVoipServer.ExtractI(); // user count
						for (int user = 0; user < users; user++) {
							ZeroMemory(buffer, sizeof(buffer));
							m_remoteVoipServer.ExtractS(buffer); // user name
						}
					}
				}
				return m_channels;
			}

			return{};
		}

		vector<string> CNetworkVoipClient::GetUsers()
		{
			if (!m_bOnServer || !m_bOnChannel) {
				return{};
			}

			int method = VoipRequest::GET_USERS_LIST;
			m_remoteVoipServer.ClearBuffers()
				->Collect(method, m_strUserName, m_strOwnID, m_strChannel)
				->SendCollection();

			vector<string> users;
			if (m_remoteVoipServer.RecvCollection() == NetworkError::NONE) {
				if (!m_remoteVoipServer.IsCollectionEmpty()) {
					if (m_remoteVoipServer.ExtractI() != VoipError::FAIL) {
						int memberCount = m_remoteVoipServer.ExtractI();
						char buffer[50];
						for (int i = 0; i < memberCount; i++) {
							ZeroMemory(buffer, sizeof(buffer));
							m_remoteVoipServer.ExtractS(buffer); // user#id
							users.push_back(buffer);
						}
						return users;
					}
				}
			}
			
			return{};
		}

		string CNetworkVoipClient::GetChannelName()
		{
			return m_strChannel;
		}

		void CNetworkVoipClient::SendTextMessage(string strMessage, int iMessageLength, string strRemoteUser, string strChannel)
		{
			int method = VoipRequest::TEXT_MESSAGE_SERVER;
			if (!strChannel.empty()) {
				method = VoipRequest::TEXT_MESSAGE_CHANNEL;
			}
			if (!strRemoteUser.empty()) {
				method = VoipRequest::TEXT_MESSAGE_USER;
			}

			m_remoteVoipServer.ClearBuffers()
				->Collect(method, m_strOwnIp, m_strUserName, strMessage);

			if (method == VoipRequest::TEXT_MESSAGE_USER) {
				m_remoteVoipServer.Collect(strRemoteUser);
			}
			else if (method == VoipRequest::TEXT_MESSAGE_CHANNEL) {
				m_remoteVoipServer.Collect(strChannel);
			}

			m_remoteVoipServer.SendCollection();

			// add sent text to message list
		}

		void Voip::Network::CNetworkVoipClient::SetCallBack(const std::function<void(int dataType, void*data)>& callback)
		{
			m_callback = callback;
		}

	}
}