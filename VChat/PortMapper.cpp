#include "PortMapper.h"

namespace Voip {
	namespace Network {
		CPortMapper::CPortMapper()
		{
		}


		CPortMapper::~CPortMapper()
		{
		}


		HRESULT CPortMapper::AddPortMapping(LONG InternalPort, LONG ExternalPort)
		{
			IUPnPNAT *pUN;

			CoInitialize(NULL);
			HRESULT hr = CoCreateInstance(__uuidof(UPnPNAT),
				NULL,
				CLSCTX_INPROC_SERVER,
				__uuidof(IUPnPNAT),
				(void**)&pUN);
			if (SUCCEEDED(hr)) {
				IStaticPortMappingCollection * pSPMC = NULL;
				hr = pUN->get_StaticPortMappingCollection(&pSPMC);
				if (SUCCEEDED(hr) && pSPMC) { // see comment in "else"   

					//if (bstrProtocol && bstrInternalClient && bstrDescription) {
					//	IStaticPortMapping * pSPM = NULL;
					//	hr = pSPMC->Add(PortNumber, bstrProtocol, PortNumber, bstrInternalClient,
					//		VARIANT_TRUE, bstrDescription, &pSPM);
					//}
					//else {
					//	hr = E_OUTOFMEMORY;
					//}
				}
				else {
					hr = E_FAIL;    // work around a known bug here:  in some error    
									// conditions, get_SPMC NULLs out the pointer, but incorrectly returns a success code.   
				}
			}
			return hr;
			WSACleanup();
		}

	}
}