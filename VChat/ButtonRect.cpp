#include "ButtonRect.h"

namespace Voip {
	namespace Gui {

		CButtonRect::CButtonRect()
		{
			// default position and dimensions
			m_offset = { 0,0 };
			m_position = { 0, 0 };
			m_dimensions = { 100, 100 };
			m_bMouseOver = false;
			m_bIsActive = false;
			m_bIsInteractive = true;
			m_textLayout = NULL;
			m_brush = NULL;
			m_pBitmap = NULL;
			m_opacity = 0;
			m_zoom = 0;
			m_minMaxOpacity = { 0.5f, 1.0f };
			m_text.clear();
		}


		CButtonRect::~CButtonRect()
		{
			Fini();
		}

		CButtonRect* CButtonRect::Init(HWND hwnd, int id, std::wstring text, D2D1_POINT_2F position, D2D1_POINT_2F dimensions)
		{
			m_gui = hwnd;
			m_id = id;
			SetBgColor(&Colors::GUIC_BUTTON_BLUE);
			SetHoverColor(&Colors::GUIC_BUTTON_BLUE);
			CGuiHelpers::D2DRenderTarget->CreateSolidColorBrush(*m_bgColor, &m_brush);
			m_position = position;
			m_dimensions = dimensions;
			m_text = text;
			SetSize(m_dimensions.x, m_dimensions.y);	

			return this;
		}

		void CButtonRect::Fini()
		{
			Helpers::SafeRelease(&m_textLayout);
			Helpers::SafeRelease(&m_brush);
			Helpers::SafeRelease(&m_pBitmap);
		}

		void CButtonRect::Update(float &time, float &timeDelta)
		{
			// fade in / fade out effect
			if (m_bMouseOver) {
				if (m_bIsActive) {
					m_opacity = m_minMaxOpacity.y;
				}
				else {
					m_opacity += 2 * timeDelta;
					if (m_opacity > m_minMaxOpacity.y)
						m_opacity = m_minMaxOpacity.y;
				}
			}
			else {
				if (m_bIsActive) {
					m_opacity = m_minMaxOpacity.y;
				}
				else {
					m_opacity -= timeDelta;
					if (m_opacity < m_minMaxOpacity.x)
						m_opacity = m_minMaxOpacity.x;
				}
			}
			m_brush->SetOpacity(m_opacity);
		}
		
		void CButtonRect::Render()
		{
			if (m_bShouldRender) {
				if (m_bIsInteractive) {
					if (m_bIsActive)
						m_brush->SetColor(m_hoverColor);
					else
						m_brush->SetColor(m_bgColor);
				}
				else {
					m_brush->SetColor(m_bgColor);
				}

				CGuiHelpers::D2DRenderTarget->FillRoundedRectangle(D2D1::RoundedRect(m_rect, 3, 3), m_brush);

				if (m_pBitmap) {
					CGuiHelpers::D2DRenderTarget->DrawBitmap(m_pBitmap, m_rect);
				}
				else if (m_textLayout) {
					m_brush->SetColor(Colors::GUIC_TEXT_WHITE);
					CGuiHelpers::D2DRenderTarget->DrawTextLayout(D2D1::Point2F(m_rect.left, m_rect.top), m_textLayout, m_brush);
				}

			}
		}

		bool CButtonRect::OnMouseMove(D2D1_POINT_2F & point)
		{

			if (m_bShouldRender && m_bIsInteractive && (m_bMouseOver = (
				point.x > (m_position.x + m_offset.x) &&
				point.y > (m_position.y + m_offset.y) &&
				point.x < (m_position.x + m_dimensions.x + m_offset.x) &&
				point.y < (m_position.y + m_dimensions.y + m_offset.y))))
			{
				Notify(m_gui, Notifications::GUIN_SETCURSOR_HAND);
				return true;
			}
			return false;
		}

		bool CButtonRect::HitTest(D2D1_POINT_2F &point)
		{
			if (OnMouseMove(point)) {
				Notify(m_gui, Notifications::GUIN_BUTTON_CLICKED);
				return true;
			}
			return false;
		}

		void CButtonRect::Move(float xDelta, float yDelta)
		{
			SetPosition(m_position.x + xDelta, m_position.y + yDelta);
		}

		void CButtonRect::SetText(std::wstring text)
		{
			Helpers::SafeRelease(&m_textLayout);

			// create new text layout
			m_text = text;
			CGuiHelpers::CreateTextLayout(text, m_dimensions.x, m_dimensions.y, &m_textLayout, &CGuiHelpers::DWTextFormatCenter);
			
		}

		void CButtonRect::SetSize(float x, float y)
		{
			m_dimensions = { x, y };
			SetPosition(m_position.x, m_position.y);
			SetText(m_text);
		}

		void CButtonRect::SetBitmap(const std::wstring file)
		{
			Helpers::SafeRelease(&m_pBitmap);
			m_pBitmap = CGuiHelpers::LoadImage(file);
		}

		void CButtonRect::Reset()
		{
			m_bMouseOver = false;
		}
	}
}