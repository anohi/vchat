#pragma once

namespace Voip {
	namespace Helpers {
		// helper function to safely release com-objects
		template <class T> inline void SafeRelease(T **ppT)
		{
			if (*ppT)
			{
				(*ppT)->Release();
				*ppT = NULL;
			}
		}

		// helper function to safely free allocated objects
		template <class T> inline void SafeDelete(T **ppT) {
			if (*ppT)
			{
				delete (*ppT);
				*ppT = NULL;
			}
		}
	}
}