#pragma once
#include <natupnp.h>

namespace Voip {
	namespace Network {
		class CPortMapper
		{
		public:
			CPortMapper();
			~CPortMapper();

			HRESULT AddPortMapping(LONG InternalPort, LONG ExternalPort);
		};

	}
}