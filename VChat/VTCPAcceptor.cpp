#include "VTCPAcceptor.h"


VTCPAcceptor::VTCPAcceptor(int port, const char * hostname)
	: m_address(hostname), m_port(port), m_listening(false), m_acceptorStream(nullptr)
{
	this->m_acceptorStream = new VTCPStream(this->m_address.c_str(), this->m_port);
}


VTCPAcceptor::~VTCPAcceptor()
{
	if (this->m_acceptorStream != nullptr) {
		this->m_acceptorStream->Fini();
		free(this->m_acceptorStream);
	}

	// close all accepted sockets

}


bool VTCPAcceptor::Start()
{
	// already listening to incoming connections
	if (this->m_listening == true) {
		return true;
	}
	
	// initializing the server to llisting to incoming connections
	if (!this->m_acceptorStream->Init(false) || !this->m_acceptorStream->BindAndListen()) {
		return false;
	}

	return (this->m_listening = true);
}


VTCPStream * VTCPAcceptor::Accept()
{
	if (!this->m_listening) {
		return nullptr;
	}

	// accept new connections and returns an address to a new VTCPStream
	SOCKET clientSocket = INVALID_SOCKET;
	VTCPStream* stream = nullptr;
	if ((clientSocket = this->m_acceptorStream->Accept()) == INVALID_SOCKET) {
		return  nullptr;
	}

	return  new VTCPStream(clientSocket);
}
