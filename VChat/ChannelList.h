#pragma once
#include "GuiHelpers.h"
#include "Scenario.h"
#include "Channel.h"
#include "Inputfield.h"
#include "Label.h"
#include "ButtonRect.h"

namespace Voip {
	namespace Gui {
		class CChannelList : public CScenario
		{
		protected:
			std::vector<CGuiElement*> m_controls;
			int m_pages;
			int m_currentPage;
			int m_channelsPerPage;

			CInputfield* m_searchBox;
			CLabel* m_pageInfo;

			void ShowPage(int page);
		public:
			void UpdateChannelList(std::vector<std::string> channels);
			void Render();
			void Update(float &time, float &timeDelta);
			void Init(HWND hwnd, int id, bool bShowBackground = false);
			void Fini();
			void Reset();

			virtual bool OnMouseMove(D2D1_POINT_2F &point);
			virtual CGuiElement* HitTest(D2D1_POINT_2F &point);

			CChannelList();
			~CChannelList();
		};
	}
}