#pragma once
#include "NetworkBase.h"
#include <string>

namespace Voip {
	namespace Network {
		class CNetworkUDPServer : public Voip::Network::CNetworkBase
		{
		public:
			CNetworkUDPServer();
			~CNetworkUDPServer();

			// Geerbt �ber CNetworkBase

			virtual void Init(char * acAddress = "", int iPort = DEFAULT_PORT) override;
			bool InitIPv6(char *acAddress = "", int iPort = DEFAULT_PORT);

			void SendI(int &i);
			void SendB(bool &b);
			void SendS(char *ac);

			int RecvS(char *ac);
			bool RecvB();
			int RecvI();

			int Select();

			void SendCollection();
			int RecvCollection();
		};
	}
}


