#include "GuiBaseControl.h"

namespace Voip {
	namespace Gui {
		CGuiBaseControl::CGuiBaseControl()
		{
		}


		CGuiBaseControl::~CGuiBaseControl()
		{
		}

		void CGuiBaseControl::Init(std::string className)
		{
			m_className			= std::wstring(className.cbegin(), className.cend());

			m_wc.cbSize			= sizeof(m_wc);
			m_wc.lpszClassName = m_className.c_str(); //_T("CustomControl");
			m_wc.hInstance		= GetModuleHandle(0);
			m_wc.lpfnWndProc	= CGuiBaseControl::WndProc;
			m_wc.hCursor		= LoadCursor(NULL, IDC_ARROW);
			m_wc.hIcon			= 0;
			m_wc.lpszMenuName	= 0;
			m_wc.hbrBackground = 0; // (HBRUSH)CreateSolidBrush(Colors::GUIC_SERVER_BACKGROUND); // TODO: manage created brush object to delete later
			m_wc.style			= CS_GLOBALCLASS | CS_HREDRAW | CS_VREDRAW;
			m_wc.cbClsExtra		= 0;
			m_wc.cbWndExtra		= 0;
			m_wc.hIconSm		= 0;
			m_wc.cbWndExtra		= sizeof(ControlData*); //sizeof(CGuiBaseControl*); // defines space for the control-pointer

			RegisterControl();				// register the control
			CreateControl(m_hwndParent);	// create the control
		}

		void CGuiBaseControl::RegisterControl()
		{
			RegisterClassEx(&m_wc);
		}

		void CGuiBaseControl::UnRegisterControl()
		{
			UnregisterClass(m_className.c_str(), NULL);
		}

		HWND CGuiBaseControl::CreateControl(HWND hwndParent)
		{
			m_hwnd = CreateWindowEx(0, m_className.c_str(), m_className.c_str(), WS_VISIBLE | WS_CHILD, m_rect.left, m_rect.top, m_rect.right, m_rect.bottom, hwndParent, NULL, GetModuleHandle(0), NULL);
			//DWORD error = GetLastError();
			if (m_hwnd) {
				SetControlData(m_hwnd, this);
			}

			return m_hwnd;
		}

		LRESULT CGuiBaseControl::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			ControlData *controlData = GetControlData(hWnd);
			if (controlData == NULL) {
				return DefWindowProc(hWnd, msg, wParam, lParam);
			}
			return controlData->gui->ControlProc(hWnd, msg, wParam, lParam);
		}

		/*
		retrieves the pointer to this control-object's extra data
		*/
		CGuiBaseControl::ControlData* CGuiBaseControl::GetControlData(HWND hWnd)
		{
			return reinterpret_cast<CGuiBaseControl::ControlData*>(GetWindowLongPtr(hWnd, 0));
		}

		/*
		sets the pointer to control-object's extra data
		*/
		void CGuiBaseControl::SetControlData(HWND hWnd, CGuiBaseControl * control)
		{
			ControlData *controlData = new ControlData;
			if (controlData) {
				controlData->hwnd = hWnd;
				controlData->gui = control;
				SetWindowLongPtr(hWnd, 0, (LONG_PTR)(controlData));
			}
		}

		/*
		Notifies the parent of an action performed on the control or its changed state
		*/
		void CGuiBaseControl::Notify(HWND hwnd, Constants::GUIN_NOTIFICATIONCODE code)
		{
			NMHDR hdr;

			hdr.hwndFrom = hwnd;
			hdr.idFrom = GetWindowLong(hwnd, GWL_ID);
			hdr.code = code;
			SendMessage(GetParent(hwnd), WM_NOTIFY, hdr.idFrom, (LPARAM)&hdr);
		}
	}
}