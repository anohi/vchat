#include "InfoBox.h"

namespace Voip {
	namespace Gui {

		CInfoBox::CInfoBox()
		{
			m_timeout = 36000;
		}


		CInfoBox::~CInfoBox()
		{
			Fini();
		}

		void CInfoBox::Init(HWND hwnd, int id, bool bShowBackground)
		{
			CScenario::Init(hwnd, id, bShowBackground);
			CLabel* text = new CLabel();
			CButtonRect* textBar = new CButtonRect();
			CButtonRect* buttonBar = new CButtonRect();
			CButtonRect* ok = new CButtonRect();

			text->Init(IDS::Element::INFO_BOX_TEXT, L"some info text");
			text->SetBgColor(&Colors::WHITE);

			ok->Init(m_gui, IDS::Element::BTN_CANCEL, L"OK");
			ok->SetOpacityMinMax(1.f, .6f);
			ok->SetSize(Dimensions::BUTTON_WIDTH, Dimensions::BAR_HEIGHT);

			textBar->Init(m_gui, -1);
			textBar->SetBgColor(&Colors::GUIC_BUTTON_BLUE);
			textBar->SetOpacityMinMax(0.8f, 0.8f);
			textBar->SetInteractivity(false);
			textBar->SetSize(Dimensions::WINDOW_WIDTH + Dimensions::PADDING_BIG, 1.5*Dimensions::BUTTON_SMALL_HEIGHT);

			buttonBar->Init(m_gui, -2);
			buttonBar->SetBgColor(&Colors::GUIC_BUTTON_BLUE_HOVER);
			buttonBar->SetOpacityMinMax(0.3f, 0.3f);
			buttonBar->SetSize(Dimensions::WINDOW_WIDTH + Dimensions::PADDING_BIG, Dimensions::BUTTON_SMALL_HEIGHT + 10);
			buttonBar->SetInteractivity(false);
			
			AddElement(textBar);
			AddElementBelow(textBar, buttonBar, 0);
			AddElement(text);
			AddElement(ok);

			text->AlignCenter(textBar, true, true);
			ok->AlignCenter(buttonBar, true, true);
			CenterWindow();
		}

		void CInfoBox::SetText(std::wstring text)
		{
			m_text = text;
			if (auto element = GetElement(IDS::Element::INFO_BOX_TEXT)) {
				auto label = dynamic_cast<CLabel*>(element);
				label->SetText(text);
				label->AlignCenter(GetElement(-1), true, true);
			}
		}

		void CInfoBox::Show()
		{
			CScenario::Show();
			m_timer.Reset();
		}

		void CInfoBox::Update(float &time, float &timeDelta)
		{
			if (m_bShouldRender) {
				m_timer.Tick();
				if (m_timer.GetTimeTotal() > m_timeout) {
					m_timer.Reset();
					for (const auto btn : m_elements) {
						if (btn->GetId() == IDS::Element::BTN_CANCEL) {
							btn->Notify(m_gui, Notifications::GUIN_BUTTON_CLICKED);
						}
					}
				}
			}

			CScenario::Update(time, timeDelta);
		}

		void CInfoBox::Render()
		{
			CScenario::Render();

			// draw time out bar
			if (m_bShouldRender && !m_bIsTransitioning) {
				const auto percent = (1.0f - m_timer.GetTimeTotal() / m_timeout);
				const auto dim = m_elements[0]->GetDimensions();
				CGuiHelpers::D2DBrush->SetColor(&Colors::GUIC_ICON_YELLOW);
				CGuiHelpers::D2DRenderTarget->FillRectangle({ 0, m_offset.y + dim.y, Dimensions::WINDOW_WIDTH * percent, dim.y + m_offset.y + 2 }, CGuiHelpers::D2DBrush);
			}
		}

		void CInfoBox::SetTimeout(float time)
		{
			m_timeout = time;
		}

	}
}