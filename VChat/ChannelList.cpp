#include "ChannelList.h"


namespace Voip {
	namespace Gui {
		CChannelList::CChannelList()
		{
			m_pages = 0;
			m_currentPage = 1;
			m_channelsPerPage = 6;
			m_center = { Dimensions::WINDOW_WIDTH / 2, Dimensions::WINDOW_WIDTH / 2 };
			m_bIsActive = false;
		}


		CChannelList::~CChannelList()
		{
			Fini();
		}


		void CChannelList::ShowPage(int page)
		{
			int min = (page-1) * m_channelsPerPage;
			int index = 0;

			if (min < 0)
				min = 0;

			int start = min;

			if (m_elements.size() >= min) {
				for (auto &channel : m_elements) {
					channel->SetVisibility(false);
					channel->SetInteractivity(false);
				}

				int col = 0;
				int row = 0;
				while (index < m_channelsPerPage && (start + index) < m_elements.size()) {
					m_elements.at(start+index)->SetPosition(col * (Dimensions::CHANNEL_WIDTH + Dimensions::PADDING_BIG), row * (Dimensions::CHANNEL_HEIGHT + Dimensions::PADDING_BIG));
					m_elements.at(start+index)->SetVisibility(true);
					m_elements.at(start+index)->SetInteractivity(true);
					index++;
					col++;
					if (col > 1) {
						row++;
						col = 0;
					}
					if (row > 2)
						break;
				} 
			}

			// update label
			for (auto label : m_controls) {
				if (label->GetId() == IDS::Element::L_JOIN_PAGE_INFO) {
					dynamic_cast<CLabel*>(label)->SetText(std::to_wstring(m_currentPage) + L"/" + std::to_wstring(m_pages));
					for (auto control : m_controls) {
						if (control->GetId() == IDS::Element::BTN_PREVIOUS_PAGE) {
							label->SetPositionLeftTo(control)->SetOffset(0, (Dimensions::INPUTFIELD_HEIGHT - Dimensions::TEXT_SIZE_BIG) / 2);
						}
					}
				}
			}
		}

		void CChannelList::UpdateChannelList(std::vector<std::string> channels)
		{
			// remove old channels
			for (auto element : m_elements) {
				Helpers::SafeDelete(&element);
			}
			m_elements.clear();

			// add channels to list
			for (auto &channelName : channels) {
				auto channel = new CChannel();
				channel->Init(m_gui, IDS::Element::CHANNEL_ITEM, std::wstring(channelName.begin(), channelName.end()));
				channel->SetSize(Dimensions::CHANNEL_WIDTH, Dimensions::CHANNEL_HEIGHT);
				channel->SetOffset((Dimensions::WINDOW_WIDTH - 2*Dimensions::CHANNEL_WIDTH - Dimensions::PADDING_BIG)/2, 2*Dimensions::BAR_HEIGHT + 10);
				m_elements.push_back(channel);
			}

			m_currentPage = 1;
			m_pages = ceilf(float(channels.size()) / float(m_channelsPerPage));

			if (m_pages)
				ShowPage(1);
		}


		bool CChannelList::OnMouseMove(D2D1_POINT_2F & point)
		{
			if (m_bIsActive && m_bShouldRender) {
				for (auto control : m_controls) {
					if (control->OnMouseMove(point)) {
						return true;
					}
				}
				return CScenario::OnMouseMove(point);
			}

			return false;
		}


		CGuiElement * CChannelList::HitTest(D2D1_POINT_2F & point)
		{
			if (m_bIsActive && m_bShouldRender) {
				for (auto control : m_controls) {
					if (control->HitTest(point)) {
						auto id = control->GetId();
						if (id == IDS::Element::BTN_PREVIOUS_PAGE) {
							m_currentPage--;
							if (m_currentPage < 1)
								m_currentPage = 1;
							else
								ShowPage(m_currentPage);

						}
						else if (id == IDS::Element::BTN_NEXT_PAGE) {
							m_currentPage++;
							if (m_currentPage > m_pages)
								m_currentPage = m_pages;
							else
								ShowPage(m_currentPage);
						}
						return control;
					}
				}
			}

			return CScenario::HitTest(point);
		}

		void CChannelList::Render()
		{
			if (m_bShouldRender) {
				CScenario::Render();

				// render controls
				for (auto controls : m_controls) {
					controls->Render();
				}

				if (!m_pages) {
					std::wstring text = L"No channels available\nCreate one!";
					CGuiHelpers::D2DBrush->SetColor(&Colors::GUIC_TEXT_BOX_TEXT);
					D2D1_RECT_F rect = { 0, Dimensions::WINDOW_HEIGHT/2, Dimensions::WINDOW_WIDTH, Dimensions::INPUTFIELD_HEIGHT };
					CGuiHelpers::D2DRenderTarget->DrawText(text.c_str(), text.length(), CGuiHelpers::DWTextFormatCenter, rect, CGuiHelpers::D2DBrush);
				}
			}
		}


		void CChannelList::Update(float &time, float &timeDelta)
		{
			CScenario::Update(time, timeDelta);

			for (auto control : m_controls) {
				control->Update(time, timeDelta);
			}
		}


		void CChannelList::Init(HWND hwnd, int id, bool bShowBackground)
		{
			CScenario::Init(hwnd, id, bShowBackground);

			// create searchbox
			m_searchBox = new CInputfield();
			m_searchBox->Init(hwnd, IDS::Element::IN_SEARCH_BOX, L"search channel name");
			m_searchBox->SetOffset((Dimensions::WINDOW_WIDTH - (Dimensions::WINDOW_WIDTH / 2 - 10)) / 2, Dimensions::BAR_HEIGHT + Dimensions::PADDING_BIG);
			//UpdateChannelList({ "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen" }); // test input
			m_controls.push_back(m_searchBox);
			
			// create page navigation buttons
			m_pageInfo = new CLabel();
			m_pageInfo->Init(IDS::Element::L_JOIN_PAGE_INFO, L"0/0");
			
			CButtonRect* previous = new CButtonRect();
			CButtonRect* next = new CButtonRect();
			
			float x = (Dimensions::WINDOW_WIDTH - 2 * Dimensions::CHANNEL_WIDTH - Dimensions::PADDING_BIG) / 2;
			previous->Init(m_gui, IDS::Element::BTN_PREVIOUS_PAGE, L"<", { 0,0 }, { Dimensions::INPUTFIELD_HEIGHT, Dimensions::INPUTFIELD_HEIGHT });
			next->Init(m_gui, IDS::Element::BTN_NEXT_PAGE, L">", { 0,0 }, { Dimensions::INPUTFIELD_HEIGHT, Dimensions::INPUTFIELD_HEIGHT });

			next->SetPosition(Dimensions::WINDOW_WIDTH - x - Dimensions::INPUTFIELD_HEIGHT, Dimensions::WINDOW_HEIGHT - 2*Dimensions::BAR_HEIGHT);
			previous->SetPositionLeftTo(next, Dimensions::PADDING);
			m_pageInfo->SetPositionLeftTo(previous)->SetOffset(0, (Dimensions::INPUTFIELD_HEIGHT - Dimensions::TEXT_SIZE_BIG)/2);

			m_controls.push_back(m_pageInfo);
			m_controls.push_back(previous);
			m_controls.push_back(next);

			CenterWindow();
		}


		void CChannelList::Fini()
		{
			CScenario::Fini();
			for (auto control : m_controls) {
				Helpers::SafeDelete(&control);
			}

		}

		void CChannelList::Reset()
		{
			m_pages = 0;
			m_currentPage = 0;
			m_channelsPerPage = 6;
			m_pageInfo->SetText(L"0/0");
			m_searchBox->SetText("");
			ShowPage(1);
		}

	}
}