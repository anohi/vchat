#include "GuiHelpers.h"

namespace Voip {
	namespace Gui {
		// initialize static variables
		float CGuiHelpers::scaleX = 1.0f;
		float CGuiHelpers::scaleY = 1.0f;
		HWND CGuiHelpers::windowHandle = 0;
		ID2D1Factory*			CGuiHelpers::D2DFactory		 = NULL;
		ID2D1SolidColorBrush*	CGuiHelpers::D2DBrush		 = NULL;
		ID2D1HwndRenderTarget*	CGuiHelpers::D2DRenderTarget = NULL;
		IDWriteFactory*			CGuiHelpers::DWFactory		 = NULL;
		IDWriteTextFormat*		CGuiHelpers::DWTextFormat	 = NULL;
		IDWriteTextFormat*		CGuiHelpers::DWTextFormatCenter = NULL;
		IWICImagingFactory*		CGuiHelpers::IWICFactory	 = NULL;
		


		// https://msdn.microsoft.com/en-us/library/windows/desktop/ff684173(v=vs.85).aspx
		bool CGuiHelpers::Init(HWND hWnd)
		{
			// create IWICImagingFactory
			CoInitializeEx(NULL, COINIT_MULTITHREADED);
			HRESULT res = CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, (LPVOID*)&IWICFactory);
			if (FAILED(res)) return Fini(false);

			// create a Direct2D factory
			res = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &D2DFactory);
			if (FAILED(res)) return Fini(false);

			// create a Direct2D render target
			// 
			RECT rect;
			GetClientRect(hWnd, &rect);
			//D2D1_PIXEL_FORMAT pixelFormat = D2D1::PixelFormat(DXGI_FORMAT_B8G8R8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED);
			res = D2DFactory->CreateHwndRenderTarget(D2D1::RenderTargetProperties(), D2D1::HwndRenderTargetProperties(hWnd, D2D1::SizeU(rect.right, rect.bottom)), &D2DRenderTarget);
			if (FAILED(res)) return Fini(false);

			// create a DirectWrite factory
			res = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown**>(&DWFactory));
			if (FAILED(res)) return Fini(false);

			// create a DirectWrite text format object
			res = DWFactory->CreateTextFormat(L"Lucida Console", NULL, DWRITE_FONT_WEIGHT_DEMI_BOLD, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, Dimensions::TEXT_SIZE_BIG, L"", &DWTextFormatCenter);
			DWTextFormatCenter->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
			DWTextFormatCenter->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
			if (FAILED(res)) return Fini(false);

			res = DWFactory->CreateTextFormat(L"Lucida Console", NULL, DWRITE_FONT_WEIGHT_NORMAL, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, Dimensions::TEXT_SIZE_NORMAL, L"", &DWTextFormat);
			//DWTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
			DWTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
			if (FAILED(res)) return Fini(false);

			// create brushes
			res = D2DRenderTarget->CreateSolidColorBrush(D2D1::ColorF(0, 0, 0, 1), &D2DBrush);
			if (FAILED(res)) return Fini(false);

			// calculate scale factors
			FLOAT dpiX, dpiY;
			D2DFactory->GetDesktopDpi(&dpiX, &dpiY);
			scaleX = dpiX / 96.0f; //96 DPI is the standard 100 %
			scaleY = dpiY / 96.0f;

			return true;
		}

		bool CGuiHelpers::Fini(bool state)
		{
			Helpers::SafeRelease(&D2DFactory);
			Helpers::SafeRelease(&D2DRenderTarget);
			Helpers::SafeRelease(&D2DBrush);
			Helpers::SafeRelease(&DWFactory);
			Helpers::SafeRelease(&DWTextFormat);
			Helpers::SafeRelease(&IWICFactory);
			CoUninitialize();
			return state;
		}

		D2D1_POINT_2F CGuiHelpers::PixelsToDips(float x, float y)
		{
			return D2D1::Point2F(x / scaleX, y / scaleY);
		}

		D2D1_POINT_2F CGuiHelpers::PixelsToDips(D2D1_POINT_2F & point)
		{
			return D2D1::Point2F(point.x / scaleX, point.y / scaleY);
		}

		D2D1_RECT_F CGuiHelpers::RectToDips(float x, float y, float width, float height)
		{
			return D2D1::RectF(x / scaleX, y / scaleY, width / scaleX, height / scaleY);
		}

		D2D1_RECT_F CGuiHelpers::PointsToDips(D2D1_POINT_2F & point1, D2D1_POINT_2F & point2)
		{
			return D2D1::RectF(point1.x / scaleX, point1.y / scaleY, point2.x / scaleX, point2.y / scaleY);
		}

		D2D1_COLOR_F CGuiHelpers::RgbToColorF(float r, float g, float b, float a)
		{
			return D2D1::ColorF(r / 255, g / 255, b / 255, a / 100);
		}

		void CGuiHelpers::CreateTextLayout(std::wstring text, float maxWidth, float maxHeight, IDWriteTextLayout** layout, IDWriteTextFormat** format)
		{
			DWFactory->CreateTextLayout(text.c_str(), text.length(), *format, maxWidth, maxHeight, layout);
		}

		// https://msdn.microsoft.com/en-us/library/windows/desktop/ee719658(v=vs.85).aspx (7.12.2016)
		ID2D1Bitmap* CGuiHelpers::LoadImage(std::wstring file)
		{
			IWICBitmapDecoder *pDecoder = NULL;
			IWICBitmapFrameDecode *pFrame = NULL;
			IWICFormatConverter *pFormatConverter = NULL;
			ID2D1Bitmap *pBitmap = NULL;
			
			if (!IWICFactory || !D2DRenderTarget)
				return pBitmap;

			// create a decoder https://msdn.microsoft.com/en-us/library/windows/desktop/ee690307(v=vs.85).aspx (7.12.2016)
			if (SUCCEEDED(IWICFactory->CreateDecoderFromFilename(file.c_str(), NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pDecoder))) {
				// retrieve the first frame of the image from the decoder https://msdn.microsoft.com/en-us/library/windows/desktop/ee690086(v=vs.85).aspx (7.12.2016)
				if (SUCCEEDED(pDecoder->GetFrame(0, &pFrame))) {
					// create a FormatConverter https://msdn.microsoft.com/en-us/library/windows/desktop/ee690317(v=vs.85).aspx (7.12.2016)
					if (SUCCEEDED(IWICFactory->CreateFormatConverter(&pFormatConverter))) {
						// convert the frame to 32bppPBGRA https://msdn.microsoft.com/en-us/library/windows/desktop/ee690279(v=vs.85).aspx (7.12.2016)
						if (SUCCEEDED(pFormatConverter->Initialize(
							pFrame,
							GUID_WICPixelFormat32bppPBGRA,
							WICBitmapDitherTypeNone,
							NULL,
							0.f,
							WICBitmapPaletteTypeCustom)))
						{
							// create D2DBitmap object https://msdn.microsoft.com/en-us/library/windows/desktop/dd371797(v=vs.85).aspx (7.12.2016)
							D2DRenderTarget->CreateBitmapFromWicBitmap(pFormatConverter, NULL, &pBitmap);
						}
					}
				}
			}

			// release com-objects
			Helpers::SafeRelease(&pDecoder);
			Helpers::SafeRelease(&pFrame);
			Helpers::SafeRelease(&pFormatConverter);

			return pBitmap;
		}
	}
}