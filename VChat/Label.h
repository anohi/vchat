#pragma once
#include "GuiHelpers.h"
#include "GuiElement.h"

namespace Voip {
	namespace Gui {
		class CLabel : public CGuiElement
		{
		protected:
			IDWriteTextLayout* m_textLayout;
			DWRITE_TEXT_ALIGNMENT m_textAlignment;
			std::wstring m_text;
			float m_fontSize;
			virtual void CreateTextLayout();
		public:
			CLabel();
			~CLabel();

			// Geerbt �ber CGuiElement
			virtual bool OnMouseMove(D2D1_POINT_2F & point) override;
			virtual bool HitTest(D2D1_POINT_2F & point) override;
			virtual void Update(float & time, float & timeDelta) override;
			virtual void Render() override;
			virtual void Move(float xDelta, float yDelta) override;
			virtual void Fini() override;
			virtual void SetSize(float x, float y) override;
			virtual void Reset() override;
			virtual void Init(int id, std::wstring text = L"", DWRITE_TEXT_ALIGNMENT alignment = DWRITE_TEXT_ALIGNMENT_LEADING, std::wstring description = L"");
			virtual void SetTextAlignment(DWRITE_TEXT_ALIGNMENT alignment);
			virtual void SetText(std::wstring text);
			virtual void SetFontSize(float size);
		};
	}
}