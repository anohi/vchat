#include "GuiElement.h"


namespace Voip {
	namespace Gui {

		CGuiElement* CGuiElement::SetVisibility(bool visible)
		{
			m_bShouldRender = visible;
			return this;
		}

		CGuiElement* CGuiElement::SetBgColor(const D2D1_COLOR_F *color)
		{
			m_bgColor = color;
			return this;
		}

		CGuiElement* CGuiElement::SetHoverColor(const D2D1_COLOR_F *color)
		{
			m_hoverColor = color;
			return this;
		}

		CGuiElement* CGuiElement::SetId(int id)
		{
			m_id = id;
			return this;
		}

		CGuiElement* CGuiElement::SetInteractivity(bool isInteractive)
		{
			m_bIsInteractive = isInteractive;
			return this;
		}

		CGuiElement * CGuiElement::SetActiveState(bool isActive)
		{
			m_bIsActive = isActive;
			return this;
		}

		CGuiElement * CGuiElement::SetOffset(float x, float y)
		{
			m_offset = { x, y };
			SetPosition(m_position.x, m_position.y);
			return this;
		}

		void CGuiElement::SetPosition(float x, float y)
		{
			m_position = { x,y };
			m_rect = CGuiHelpers::PointsToDips(D2D1::Point2F(m_position.x + m_offset.x, m_position.y + m_offset.y), D2D1::Point2F(m_position.x + m_dimensions.x + m_offset.x, m_position.y + m_dimensions.y + m_offset.y));
		}

		void CGuiElement::SetPosition(D2D1_POINT_2F & point)
		{
			SetPosition(point.x, point.y);
		}

		CGuiElement * CGuiElement::AlignLeft(CGuiElement * anchor)
		{
			if (anchor) {
				auto pos = anchor->GetPosition();
				this->SetPosition(pos.x, m_position.y);
			}
			return this;
		}

		CGuiElement * CGuiElement::AlignRight(CGuiElement * anchor)
		{
			if (anchor) {
				auto pos = anchor->GetPosition(), dim = anchor->GetDimensions();
				this->SetPosition(pos.x + dim.x - m_dimensions.x, m_position.y);
			}
			return this;
		}

		CGuiElement * CGuiElement::AlignBottom(CGuiElement * anchor)
		{
			if (anchor) {
				auto pos = anchor->GetPosition(), dim = anchor->GetDimensions();
				this->SetPosition(m_position.x, pos.y + dim.y);
			}
			return this;
		}

		CGuiElement * CGuiElement::AlignTop(CGuiElement * anchor)
		{
			if (anchor) {
				auto pos = anchor->GetPosition();
				this->SetPosition(m_position.x, pos.y);
			}
			return this;
		}

		CGuiElement * CGuiElement::AlignCenter(CGuiElement * anchor, bool horizontal, bool vertical)
		{
			if (anchor) {
				auto pos = anchor->GetPosition(), dim = anchor->GetDimensions();
				//int x = horizontal ? (pos.x + ((pos.x + dim.x) - m_dimensions.x) / 2) : m_position.x;
				int x = horizontal ? pos.x + (dim.x - m_dimensions.x) / 2 : m_position.x;
				int y = vertical ? pos.y + (dim.y - m_dimensions.y) / 2 : m_position.y;
				this->SetPosition(x, y);
			}
			return this;
		}

		CGuiElement * CGuiElement::SetPositionLeftTo(CGuiElement * anchor, float padding)
		{
			if (anchor) {
				auto pos = anchor->GetPosition();
				SetPosition(pos.x - (m_dimensions.x + padding), pos.y);
			}
			return this;
		}

		CGuiElement * CGuiElement::SetPositionRightTo(CGuiElement * anchor, float padding)
		{
			if (anchor) {
				auto pos = anchor->GetPosition(), dim = anchor->GetDimensions();
				SetPosition(pos.x + dim.x + padding, pos.y);
			}
			return this;
		}

		CGuiElement * CGuiElement::SetPositionAbove(CGuiElement * anchor, float padding)
		{
			if (anchor) {
				auto pos = anchor->GetPosition();
				SetPosition(pos.x, pos.y - (m_dimensions.y + padding));
			}
			return this;
		}

		CGuiElement * CGuiElement::SetPositionUnder(CGuiElement * anchor, float padding)
		{
			if (anchor) {
				auto pos = anchor->GetPosition(), dim = anchor->GetDimensions();
				SetPosition(pos.x, pos.y + dim.y + padding);
			}
			return this;
		}

		void CGuiElement::SetOpacityMinMax(float min, float max)
		{
			m_minMaxOpacity = { min, max };
		}

		void CGuiElement::SetOpacity(float opacity)
		{
			m_opacity = opacity;
		}

		int CGuiElement::GetId()
		{
			return m_id;
		}

		bool CGuiElement::IsInteractive()
		{
			return m_bIsInteractive;
		}

		bool CGuiElement::ShouldRender()
		{
			return m_bShouldRender;
		}

		bool CGuiElement::IsActive()
		{
			return m_bIsActive;
		}

		D2D_POINT_2F CGuiElement::GetPosition()
		{
			return m_position;
		}

		D2D_POINT_2F CGuiElement::GetDimensions()
		{
			return m_dimensions;
		}

		const D2D1_COLOR_F * CGuiElement::GetBgColor()
		{
			return m_bgColor;
		}

		const D2D1_COLOR_F * CGuiElement::GetHoverColor()
		{
			return m_hoverColor;
		}

		/*
		Notifies the parent of an action performed on the control or its changed state
		https://msdn.microsoft.com/en-us/library/windows/desktop/bb775583(v=vs.85).aspx
		*/
		void CGuiElement::Notify(HWND hwnd, Notifications code)
		{
			NMHDR hdr;

			hdr.hwndFrom = hwnd;
			hdr.idFrom = LONG_PTR(this);
			hdr.code = code;
			SendMessage(hwnd, WM_NOTIFY, hdr.idFrom, (LPARAM)&hdr);
		}
}
}