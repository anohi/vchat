#include "Group.h"

namespace Voip {
	namespace Gui {

		CGroup::CGroup()
		{
			m_offset = { Dimensions::WINDOW_WIDTH / 2 - 152, Dimensions::WINDOW_HEIGHT / 2 - 152 };
			m_center = { Dimensions::WINDOW_WIDTH / 2,Dimensions::WINDOW_HEIGHT / 2 };
			m_iMaxUsers = 20;
			m_iCurrentUsers = 0;
			m_id = 0;

			for (int i = 0; i < 6; i++) {
				m_slotPositions[i] = { i * 51, 0 };
				m_slotPositions[i + 14] = { i * 51, 5 * 51 };
			}
			for (int i = 6, b = 1; i < 14; i++) {
				m_slotPositions[i] = { i % 2 ? 5 * 51 : 0, b * 51 };
				if (i % 2)
					b++;
			}

			for (int i = 0; i < m_iMaxUsers; i++) {
				CUser* user = new CUser();
				user->Init(m_gui, IDS::Element::USER_ITEM);
				user->SetSize(Dimensions::BUTTON_WIDTH / 2 - 1, Dimensions::BUTTON_HEIGHT / 2 - 1);
				user->SetPosition(m_slotPositions[i]);
				user->SetOffset(m_offset.x, m_offset.y);
				m_elements.push_back(user);
			}
		}


		CGroup::~CGroup()
		{
		}

		void CGroup::AddUser(CUser &user)
		{
			if (m_iCurrentUsers < m_iMaxUsers) {
				user.SetSize(Dimensions::BUTTON_WIDTH / 2 - 1, Dimensions::BUTTON_HEIGHT / 2 - 1);
				m_iCurrentUsers++;
			}

			// update positions
			for (int i = 0; i < m_elements.size(); i++) {
				m_elements.at(i)->SetPosition(m_slotPositions[i]);
				m_elements.at(i)->SetOffset(m_offset.x, m_offset.y);
			}


		}

		void CGroup::RemoveUser(std::string id)
		{
			// update positions
		}

		void CGroup::UpdateUserList(std::vector<std::string> &users)
		{
			for (int i = 0; i < m_elements.size(); i++) {
				const auto user = dynamic_cast<CUser*>(m_elements.at(i));
				user->SetEmpty();
				user->SetPosition(m_slotPositions[i]);
				user->SetOffset(m_offset.x, m_offset.y);

				if (i < users.size()) {
					user->SetUserId(users.at(i));
					user->SetText(std::wstring(users.at(i).begin(), users.at(i).begin() + 1));
				}
			}
		}

		void CGroup::UpdateTalkersList(std::vector<std::string>* users)
		{
			// set all to not talking
			for (auto &user : m_elements) {
				dynamic_cast<CUser*>(user)->SetState(CUser::NORMAL);
			}

			// set talkers
			for (auto &userId : *users) {
				for (auto &element : m_elements) {
					if (dynamic_cast<CUser*>(element)->GetUserId() == userId) {
						dynamic_cast<CUser*>(element)->SetState(CUser::TALKING);
					}
				}
			}
		}

		int CGroup::MaxUsers()
		{
			return 0;
		}

		int CGroup::UserCount()
		{
			return 0;
		}

		CUser * CGroup::HitTest(D2D1_POINT_2F & point)
		{
			if (m_bIsActive && !m_bIsTransitioning) {
				for (auto &user : m_elements) {
					user->Reset();
				}
				for (auto &user : m_elements) {
					if (user->HitTest(point)) {
						// open options menu for this user
						return dynamic_cast<CUser*>(user);
					}
				}
			}
			
			return nullptr;
		}

		void CGroup::Fini()
		{
			for (auto &user : m_elements) {
				user->Fini();
			}
			m_elements.clear();
		}

		void CGroup::Update(float &time, float &timeDelta)
		{

			for (auto &user : m_elements) {
				user->Update(time, timeDelta);
			}

			if (m_bZoomedIn) {
				m_zoomFactor += 5 * timeDelta;
				if (m_zoomFactor > 1) {
					m_zoomFactor = 1;
					m_bIsTransitioning = false;
				}
			}
			else {
				m_zoomFactor -= 5 * timeDelta;
				if (m_zoomFactor < 0) {
					m_zoomFactor = 0;
					m_bIsTransitioning = false;
					m_bShouldRender = false;
				}
			}

		}

		void CGroup::Render()
		{
			if (m_bShouldRender) {
				// rendering 20 slots for users
				CGuiHelpers::D2DRenderTarget->GetTransform(&m_transformation);
				CGuiHelpers::D2DRenderTarget->SetTransform(D2D1::Matrix3x2F::Scale(D2D1::SizeF(m_zoomFactor, m_zoomFactor), m_center));
				// render elements
				for (auto &user : m_elements) {
					user->Render();
				}

				CGuiHelpers::D2DRenderTarget->SetTransform(m_transformation);
			}
		}

		void CGroup::SetOffset(int x, int y)
		{
			m_offset = { x,y };
			for (auto &user : m_elements) {
				user->SetOffset(x, y);
			}

			m_center = { m_offset.x + (m_bounds.right - m_bounds.left) / 2, m_offset.y + (m_bounds.bottom - m_bounds.top) / 2 };
		}

		void CGroup::SwapPosition(int iSrc, int iDest)
		{

		}

	}
}