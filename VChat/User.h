#pragma once
#include "GuiHelpers.h"
#include "ButtonRect.h"
#include "Timer.h"

namespace Voip {
	namespace Gui {
		class CUser : public CButtonRect
		{
		public:
			CUser();
			~CUser();

			enum STATE {
				TALKING,
				AFK,
				DND,
				NORMAL,
				INVISIBLE
			};

			virtual void Update(float &time, float &timeDelta);
			virtual void Render();
			virtual void SetEmpty();
			virtual void SetState(STATE state);
			virtual void SetUserId(std::string userId);

			std::string GetUserId();
			STATE GetState();

		protected:
			Voip::Helpers::CTimer m_timer;

			std::string m_strUserId;
			bool m_bIsEmpty;
			STATE m_state;
		};
	}
}