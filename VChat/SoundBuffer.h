#pragma once
#pragma once
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <mmsystem.h>
#include <dsound.h>
#include <math.h>
#include <vector>
#include "Helpers.h"

namespace Voip {
	namespace Sound {
		const WAVEFORMATEX PCM_8000_MONO = { WAVE_FORMAT_PCM, 1, 8000, 16000, 2, 16, 0 };
		const WAVEFORMATEX PCM_11025_MONO = { WAVE_FORMAT_PCM, 1, 11025, 22050, 2, 16, 0 };
		const WAVEFORMATEX PCM_16000_MONO = { WAVE_FORMAT_PCM, 1, 16000, 32000, 2, 16, 0 };

		class CSoundBuffer
		{
		protected:
			LPDIRECTSOUNDBUFFER8 m_lpDSBuffer;

		private:
			std::vector<HANDLE>	m_hPlaybackEvents;
			size_t				m_nPlaybackSegments;
			WAVEFORMATEX		m_wfxFormat;
			bool				m_bMuted;

		public:
			CSoundBuffer();
			~CSoundBuffer();

			bool SetVolumeMin();
			bool SetVolumeMax();

			BOOL PlayBuffer(DWORD dwFlags = DSBPLAY_LOOPING);
			BOOL StopBuffer();
			BOOL CreateSoundBuffer(LPDIRECTSOUND lpDS, WAVEFORMATEX wfxFormat);
			BOOL WriteData(short *data, DWORD dwSize);
			BOOL FillSilence(DWORD segment, DWORD dwSize);
			BOOL SetNotifications(size_t count);
			BOOL IsInit();
			DWORD GetSegmentLength();
			const std::vector<HANDLE> GetNotifications();
			void Cleanup();

			LPDIRECTSOUNDBUFFER8 GetBuffer();
		};
	}
}