#pragma once
#include "GuiBaseControl.h"
#include <vector>

//using namespace Vektoria::Plugin;

namespace Voip {
	namespace Gui {
		class CGuiListServer : public Voip::Gui::CGuiBaseControl
		{
		public:
			CGuiListServer(HWND hWndParent);
			~CGuiListServer();
			
			LRESULT CALLBACK ControlProc(HWND, UINT, WPARAM, LPARAM) override;
			LRESULT OnPaint(WPARAM wParam, LPARAM lParam);
			LRESULT OnLButtonClick(WPARAM wParam, LPARAM lParam);
			LRESULT OnSetFont(WPARAM wParam, LPARAM lParam);
			void Fini() override;

		private:
			std::vector<std::string> m_entries;
			
			COLORREF	m_crUnselected;
			COLORREF	m_crSelected;
			COLORREF	m_crMouseOver;

			LRESULT OnLButtonDown(WPARAM wParam, LPARAM lParam);
			LRESULT OnLButtonUp(WPARAM wParam, LPARAM lParam);
			LRESULT OnRButtonDown(WPARAM wParam, LPARAM lParam);
			LRESULT OnRButtonUp(WPARAM wParam, LPARAM lParam);
		};
	}
}


