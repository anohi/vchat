#pragma once
#include <fstream>

#include "GuiHelpers.h"
#include "Scenario.h"
#include "ButtonRect.h"
#include "Inputfield.h"
#include "Label.h"
#include "Checkbox.h"

using namespace std;
namespace Voip {
	namespace Gui {
		class CSettings : public CScenario
		{
		protected:
			std::string m_file;

			bool m_autoConnect;
			bool m_autoJoin;
			int m_port;
			std::string m_serverAddr;
			std::string m_userName;
			std::string m_channel;

		public:
			CSettings();
			~CSettings();

			void Init(HWND hwnd, int id, bool bShowBackground = false);
			bool LoadSettings(std::string file = "");
			void SaveSettings(std::string file = "");

			void SetAutoConnect(bool state);
			void SetAutoJoin(bool state);
			void SetUsername(std::string name);
			void SetChannel(std::string channel);
			void SetServer(std::string addr);
			void SetPort(int port);

			std::string GetChannel();
			std::string GetServer();
			std::string GetUsername();
			int GetPort();
			bool IsAutoConnect();
			bool IsAutoJoin();

			CGuiElement* HitTest(D2D1_POINT_2F &point);
		};
	}
}
