#include "SoundRecorder.h"
#include "VALawCodec.h"
#include "SoundProcessing.h"

#include <vector>
#include <deque>
#include <thread>
#include <fstream>


using namespace std;
using namespace Voip;


Sound::CSoundProcessing dsp;
Sound::CSoundRecorder soundRecorder;
VALawCodec codec;
deque<pair<int, short*>> mixed;

HWND hwnd = GetConsoleWindow();

int main() {
	if (!soundRecorder.Init(hwnd) || !soundRecorder.InitMicrophone(hwnd)) {
		printf("SoundRecorder::Init() failed\n");
		return 0;
		std::system("pause");
	}
	soundRecorder.SetNotifications(16);
	soundRecorder.CreateSoundBuffer("sb1", Sound::PCM_8000_MONO);

	thread thrSendVoipData([=]() {
		soundRecorder.StartCapture();
		auto	captureEvents = soundRecorder.GetNotifications();
		DWORD	segmentSize = soundRecorder.GetSegmentLength();
		DWORD   segmentsCount = captureEvents.size();
		short  *data = new short[segmentSize / 2];
		short  *test;
		DWORD bytes = 0;

		while (true) {
			DWORD segment = WaitForMultipleObjects(segmentsCount, &captureEvents[0], false, 0);
			if (segment >= WAIT_OBJECT_0 && segment < segmentsCount) {
				int bytesRead = soundRecorder.ReadCapturedData(segment, data);

				vector<int16_t*> channels;
				channels.push_back(data);
				channels.push_back(data);
				channels.push_back(data);
				channels.push_back(data);
				channels.push_back(data);
				channels.push_back(data);
				channels.push_back(data);

				//test = dsp.Mix(&channels, bytesRead);

				mixed.push_back({ bytesRead, test});
				cout << segment << endl;
			}

			Sleep(1);
		}

		soundRecorder.StopCapture();
		// free memory
		if (data) {
			free(data);
		}

		cout << "Stopped recording...\n";
	});
	thrSendVoipData.detach();


	thread thrPlayback([=]() {
		auto soundBuffer = soundRecorder.GetBuffer("sb1");
		soundBuffer->PlayBuffer();

		while (true) {
			auto playbackEvents = soundBuffer->GetNotifications();
			DWORD segmentSize = soundBuffer->GetSegmentLength();
			DWORD segmentsCount = playbackEvents.size();

			DWORD segment = WaitForMultipleObjects(segmentsCount, &playbackEvents[0], false, 0);
			if (segment >= WAIT_OBJECT_0 && segment < segmentsCount) {
				soundBuffer->FillSilence(segment, segmentSize);
				if (mixed.size()) {
					soundBuffer->WriteData(mixed.front().second, mixed.front().first);
					free(mixed.front().second);
					mixed.pop_front();
				}
			}

			Sleep(1);
		}
		cout << "Stopped playing { sb1 }\n";
	});
	thrPlayback.detach();

	std::system("pause");
	return 0;
}