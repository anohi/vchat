//{{NO_DEPENDENCIES}}
// Von Microsoft Visual C++ generierte Includedatei.
// Verwendet durch VektoriaApp.rc
//
#define VOIP_IMAGE_W                    100
#define VOIP_IMAGE_H                    100
#define IDI_ICON1                       101
#define IDD_VOIP                        102
#define IDI_ICON_CHATROOM               104
#define IDI_ICON_SERVER1                105
#define IDI_ICON_SERVER2                106
#define IDD_INPUT                       107
#define IDC_BUTTON_CONNECTSERVER        1001
#define IDC_BUTTON_ADDSERVER            1002
#define IDC_BUTTON_REMOVESERVER         1003
#define IDC_LIST_MESSAGES               1004
#define IDC_EDIT_CHATBOX                1005
#define IDC_TREE_SERVERLIST             1006
#define IDC_EDIT_INPUTBOX               1007
#define IDC_LABEL_INFO                  1008
#define IDC_CUSTOM2                     1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
